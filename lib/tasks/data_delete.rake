namespace :data_delete do
	task create: :environment do
		goat = Goat.where(token_no: ['Kid Female Thalachery', 'Kid Male Thalachery'])
		goat.each do |goat| 
			calving = goat.goat_calving_periods
			if calving.present?
				calving.each do |cal|
					cal.delete
				end
			end
		end

	end
end