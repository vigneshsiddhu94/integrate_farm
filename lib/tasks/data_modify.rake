 require 'csv'
 namespace :data_modify do
	task create: :environment do
		file = "/home/vigneshsiddhu/Desktop/Lakshmi Goat Farm/Dump Data/goat_calving_periods.csv"
		CSV.foreach(file, :headers => true) do |row|
			g = GoatCalvingPeriod.new
			g.id =  row['id']
			g.goat_id = row['goat_id']
			g.goat_status = row['goat_status'].to_i
			g.live = row['live'].to_i
			g.message = row ['message']
			g.created_at = row['created_at']
			g.updated_at = row['updated_at']
			g.save
		end
 		
 	end
 end