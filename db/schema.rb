# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2025_02_07_141132) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_stat_statements"
  enable_extension "plpgsql"

  create_table "batch_goats", force: :cascade do |t|
    t.bigint "batch_id"
    t.bigint "goat_id"
    t.integer "status", default: 1
    t.date "date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["batch_id"], name: "index_batch_goats_on_batch_id"
    t.index ["goat_id"], name: "index_batch_goats_on_goat_id"
  end

  create_table "batch_weights", force: :cascade do |t|
    t.bigint "business_id", null: false
    t.bigint "user_id", null: false
    t.integer "kid_stage"
    t.bigint "rack_detail_id", null: false
    t.integer "male_count"
    t.integer "female_count"
    t.integer "weight"
    t.bigint "month_on_weight_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_batch_weights_on_business_id"
    t.index ["month_on_weight_id"], name: "index_batch_weights_on_month_on_weight_id"
    t.index ["rack_detail_id"], name: "index_batch_weights_on_rack_detail_id"
    t.index ["user_id"], name: "index_batch_weights_on_user_id"
  end

  create_table "batches", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id"
    t.bigint "rack_detail_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["rack_detail_id"], name: "index_batches_on_rack_detail_id"
    t.index ["user_id"], name: "index_batches_on_user_id"
  end

  create_table "businesses", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_businesses_on_user_id"
  end

  create_table "calves", force: :cascade do |t|
    t.bigint "cow_id"
    t.integer "token_no"
    t.date "date_of_birth"
    t.string "breed_variety"
    t.string "gender"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["cow_id"], name: "index_calves_on_cow_id"
  end

  create_table "calving_periods", force: :cascade do |t|
    t.bigint "cow_id"
    t.string "cow_status"
    t.date "calve_date"
    t.string "message"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "active", default: false
    t.index ["cow_id"], name: "index_calving_periods_on_cow_id"
  end

  create_table "coconut_expenses", force: :cascade do |t|
    t.integer "reason"
    t.string "name"
    t.datetime "date"
    t.integer "total_amount"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_coconut_expenses_on_user_id"
  end

  create_table "coconut_invoices", force: :cascade do |t|
    t.string "lorry"
    t.integer "total_amount"
    t.datetime "date"
    t.bigint "user_id", null: false
    t.bigint "customer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_coconut_invoices_on_customer_id"
    t.index ["user_id"], name: "index_coconut_invoices_on_user_id"
  end

  create_table "cows", force: :cascade do |t|
    t.string "token_no"
    t.string "milk_assurance"
    t.float "cow_rate"
    t.date "date_of_purchase"
    t.string "breed_variety"
    t.string "cow_location"
    t.string "age"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id", null: false
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_cows_on_business_id"
    t.index ["user_id"], name: "index_cows_on_user_id"
  end

  create_table "crop_activities", force: :cascade do |t|
    t.bigint "land_crop_id"
    t.integer "activity"
    t.date "date"
    t.string "comment"
    t.date "end_date"
    t.integer "status", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["land_crop_id"], name: "index_crop_activities_on_land_crop_id"
  end

  create_table "crop_yields", force: :cascade do |t|
    t.bigint "land_crop_id", null: false
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.decimal "yield_per_kg", precision: 10, scale: 2, null: false
    t.decimal "production_cost", precision: 10, scale: 2, null: false
    t.decimal "per_kg_rate", precision: 10, scale: 2, null: false
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["land_crop_id"], name: "index_crop_yields_on_land_crop_id"
  end

  create_table "crops", force: :cascade do |t|
    t.string "name"
    t.integer "yield"
    t.string "details"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "variety"
    t.string "nutrition"
    t.integer "period"
    t.integer "seed_acre"
    t.string "intercrop"
    t.integer "water_requirement"
    t.integer "sowing_distance"
    t.integer "season"
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_crops_on_business_id"
  end

  create_table "customer_accounts", force: :cascade do |t|
    t.decimal "amount", precision: 15, scale: 2
    t.string "reason"
    t.date "date"
    t.bigint "customer_id", null: false
    t.string "balance_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_customer_accounts_on_customer_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.integer "is_activate"
    t.integer "mobile_number"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "account_number", precision: 30
    t.string "bank"
    t.string "ifsc"
    t.string "branch"
    t.bigint "place_id"
    t.index ["place_id"], name: "index_customers_on_place_id"
    t.index ["user_id"], name: "index_customers_on_user_id"
  end

  create_table "daily_works", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.bigint "main_work_id", null: false
    t.string "comment"
    t.integer "status", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["goat_id"], name: "index_daily_works_on_goat_id"
    t.index ["main_work_id"], name: "index_daily_works_on_main_work_id"
  end

  create_table "death_goats", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.bigint "disease_id", null: false
    t.date "died_date"
    t.text "comment"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.float "weight"
    t.index ["disease_id"], name: "index_death_goats_on_disease_id"
    t.index ["goat_id"], name: "index_death_goats_on_goat_id"
  end

  create_table "disease_goats", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.bigint "disease_id"
    t.string "comment"
    t.integer "status", default: 1
    t.date "disease_date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["disease_id"], name: "index_disease_goats_on_disease_id"
    t.index ["goat_id"], name: "index_disease_goats_on_goat_id"
  end

  create_table "diseases", force: :cascade do |t|
    t.string "name"
    t.string "symptoms"
    t.string "impact_goat"
    t.string "treatment"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_diseases_on_user_id"
  end

  create_table "dry_fodders", force: :cascade do |t|
    t.date "purchase_date"
    t.date "close_date"
    t.integer "amount"
    t.integer "qty"
    t.integer "land_qty"
    t.string "comment"
    t.integer "variety", default: 1
    t.integer "status", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "farm_expenses", force: :cascade do |t|
    t.datetime "date", precision: nil
    t.integer "category"
    t.integer "amount"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.bigint "business_id", null: false
    t.text "note"
    t.index ["business_id"], name: "index_farm_expenses_on_business_id"
    t.index ["user_id"], name: "index_farm_expenses_on_user_id"
  end

  create_table "farmer_accounts", force: :cascade do |t|
    t.decimal "amount", precision: 15, scale: 2
    t.string "reason"
    t.date "date"
    t.bigint "farmer_id", null: false
    t.string "balance_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["farmer_id"], name: "index_farmer_accounts_on_farmer_id"
  end

  create_table "farmer_bills", force: :cascade do |t|
    t.integer "asal_kai_count"
    t.float "asal_kai_rate"
    t.integer "neer_vattral__kai_count"
    t.float "neer_vattral_kai_rate"
    t.integer "total_amount"
    t.datetime "procurement_date"
    t.bigint "user_id", null: false
    t.bigint "farmer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "paid", default: false
    t.datetime "paid_date"
    t.integer "asal_kai_vasi"
    t.integer "neer_vatral_kai_vasi"
    t.index ["farmer_id"], name: "index_farmer_bills_on_farmer_id"
    t.index ["user_id"], name: "index_farmer_bills_on_user_id"
  end

  create_table "farmer_lands", force: :cascade do |t|
    t.string "acre"
    t.integer "tree_count"
    t.bigint "farmer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["farmer_id"], name: "index_farmer_lands_on_farmer_id"
  end

  create_table "farmers", force: :cascade do |t|
    t.string "name"
    t.boolean "is_activate", default: true
    t.integer "mobile_number"
    t.bigint "user_id", null: false
    t.bigint "place_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "account_number", precision: 30
    t.string "bank"
    t.string "ifsc"
    t.string "branch"
    t.index ["place_id"], name: "index_farmers_on_place_id"
    t.index ["user_id"], name: "index_farmers_on_user_id"
  end

  create_table "feed_availabilities", force: :cascade do |t|
    t.date "date"
    t.integer "fodder_variety"
    t.integer "stored_kg"
    t.integer "rate"
    t.integer "total_amount"
    t.integer "current_kg"
    t.bigint "business_id", null: false
    t.bigint "user_id", null: false
    t.boolean "status", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "wastage_kg"
    t.index ["business_id"], name: "index_feed_availabilities_on_business_id"
    t.index ["user_id"], name: "index_feed_availabilities_on_user_id"
  end

  create_table "feed_consumptions", force: :cascade do |t|
    t.date "date", null: false
    t.float "concentrate_feed", default: 0.0, null: false
    t.float "thippi", default: 0.0, null: false
    t.float "asola", default: 0.0, null: false
    t.float "dry_fodder", default: 0.0, null: false
    t.float "green_fodder", default: 0.0, null: false
    t.float "agathi_mulberry", default: 0.0, null: false
    t.float "silage", default: 0.0, null: false
    t.float "wastage_green", default: 0.0, null: false
    t.float "wastage_dry", default: 0.0, null: false
    t.string "cutting_land", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "business_id", null: false
    t.index ["business_id"], name: "index_feed_consumptions_on_business_id"
  end

  create_table "feed_rates", force: :cascade do |t|
    t.string "concentrate_feed"
    t.string "green_fodder"
    t.string "dry_fodder"
    t.string "chaval_powder"
    t.boolean "status", default: true
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "feeds", force: :cascade do |t|
    t.boolean "status", default: true
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.date "date"
    t.integer "animal"
    t.integer "concentrate"
    t.integer "green_fodder"
    t.integer "dry_fodder"
    t.integer "dry_fodder_variety"
    t.integer "green_fodder_variety"
    t.integer "feed_cost"
    t.bigint "business_id"
    t.bigint "user_id"
    t.index ["business_id"], name: "index_feeds_on_business_id"
    t.index ["user_id"], name: "index_feeds_on_user_id"
  end

  create_table "fertilizer_activities", force: :cascade do |t|
    t.bigint "crop_activity_id", null: false
    t.bigint "fertilizer_id", null: false
    t.date "date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["crop_activity_id"], name: "index_fertilizer_activities_on_crop_activity_id"
    t.index ["fertilizer_id"], name: "index_fertilizer_activities_on_fertilizer_id"
  end

  create_table "fertilizers", force: :cascade do |t|
    t.string "name"
    t.string "details"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "goat_calving_periods", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.integer "goat_status"
    t.integer "live", default: 1
    t.string "message"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.datetime "date", precision: nil
    t.index ["goat_id"], name: "index_goat_calving_periods_on_goat_id"
  end

  create_table "goat_sales", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.bigint "invoice_id", null: false
    t.integer "weight"
    t.integer "amount"
    t.integer "purpose_of_sale"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["goat_id"], name: "index_goat_sales_on_goat_id"
    t.index ["invoice_id"], name: "index_goat_sales_on_invoice_id"
  end

  create_table "goat_weights", force: :cascade do |t|
    t.bigint "goat_id"
    t.integer "weight"
    t.date "date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["goat_id"], name: "index_goat_weights_on_goat_id"
  end

  create_table "goats", force: :cascade do |t|
    t.integer "token_no"
    t.integer "breed_variety", default: 1
    t.float "weight"
    t.date "date_of_birth"
    t.integer "age"
    t.integer "live", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.integer "gender"
    t.index ["user_id"], name: "index_goats_on_user_id"
  end

  create_table "insurance_goats", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.string "policy_no"
    t.date "start_date"
    t.date "exp_date"
    t.integer "amount"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["goat_id"], name: "index_insurance_goats_on_goat_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "total_amount"
    t.date "sale_date"
    t.integer "discount"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.float "total_weight"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "kopparais", force: :cascade do |t|
    t.float "kg"
    t.float "rate"
    t.datetime "date"
    t.float "total_amount"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quality"
    t.index ["user_id"], name: "index_kopparais_on_user_id"
  end

  create_table "labour_accounts", force: :cascade do |t|
    t.decimal "amount", precision: 15, scale: 2
    t.string "reason"
    t.date "date"
    t.bigint "customer_id", null: false
    t.string "balance_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_labour_accounts_on_customer_id"
  end

  create_table "labours", force: :cascade do |t|
    t.string "name"
    t.boolean "is_activate", default: true
    t.integer "mobile_number"
    t.bigint "user_id", null: false
    t.bigint "place_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_id"], name: "index_labours_on_place_id"
    t.index ["user_id"], name: "index_labours_on_user_id"
  end

  create_table "land_crops", force: :cascade do |t|
    t.bigint "land_id", null: false
    t.bigint "crop_id", null: false
    t.date "date"
    t.integer "status", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "fodder"
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_land_crops_on_business_id"
    t.index ["crop_id"], name: "index_land_crops_on_crop_id"
    t.index ["land_id"], name: "index_land_crops_on_land_id"
  end

  create_table "land_waters", force: :cascade do |t|
    t.bigint "water_id", null: false
    t.bigint "land_id", null: false
    t.date "date"
    t.integer "status", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_land_waters_on_business_id"
    t.index ["land_id"], name: "index_land_waters_on_land_id"
    t.index ["water_id"], name: "index_land_waters_on_water_id"
  end

  create_table "lands", force: :cascade do |t|
    t.string "name"
    t.integer "cent"
    t.integer "soil", default: 1
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_lands_on_business_id"
  end

  create_table "loads", force: :cascade do |t|
    t.integer "paruthi"
    t.integer "sakku"
    t.integer "rate"
    t.integer "coconut_count"
    t.integer "total_amount"
    t.bigint "coconut_invoice_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quality"
    t.index ["coconut_invoice_id"], name: "index_loads_on_coconut_invoice_id"
  end

  create_table "main_works", force: :cascade do |t|
    t.integer "breed_variety", null: false
    t.string "day"
    t.integer "action"
    t.string "action_detail"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_main_works_on_user_id"
  end

  create_table "mate_goats", force: :cascade do |t|
    t.bigint "male_id"
    t.bigint "female_id"
    t.date "date_of_mat"
    t.string "comment"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["female_id"], name: "index_mate_goats_on_female_id"
    t.index ["male_id"], name: "index_mate_goats_on_male_id"
  end

  create_table "mattais", force: :cascade do |t|
    t.integer "load"
    t.integer "rate"
    t.datetime "date"
    t.integer "total_amount"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_mattais_on_user_id"
  end

  create_table "milk_productions", force: :cascade do |t|
    t.bigint "cow_id"
    t.date "milk_date"
    t.string "day_time"
    t.float "fat"
    t.float "snf"
    t.float "rate"
    t.float "litre"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "feed_rate"
    t.bigint "business_id", null: false
    t.bigint "user_id", null: false
    t.index ["business_id"], name: "index_milk_productions_on_business_id"
    t.index ["cow_id"], name: "index_milk_productions_on_cow_id"
    t.index ["user_id"], name: "index_milk_productions_on_user_id"
  end

  create_table "month_on_weights", force: :cascade do |t|
    t.integer "total_male_count"
    t.integer "total_female_count"
    t.integer "total_weight"
    t.date "date"
    t.bigint "business_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_month_on_weights_on_business_id"
    t.index ["user_id"], name: "index_month_on_weights_on_user_id"
  end

  create_table "mytable", id: :integer, default: nil, force: :cascade do |t|
    t.integer "token_no", null: false
    t.integer "breed_variety", null: false
    t.decimal "weight", precision: 4, scale: 2, null: false
    t.date "date_of_birth", null: false
    t.integer "age", null: false
    t.integer "live", null: false
    t.string "created_at", limit: 26, null: false
    t.string "updated_at", limit: 26, null: false
    t.integer "user_id", null: false
    t.integer "gender", null: false
  end

  create_table "parent_goats", force: :cascade do |t|
    t.bigint "children_id"
    t.bigint "parent_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["children_id"], name: "index_parent_goats_on_children_id"
    t.index ["parent_id"], name: "index_parent_goats_on_parent_id"
  end

  create_table "pending_works", force: :cascade do |t|
    t.string "work"
    t.integer "work_type"
    t.datetime "date"
    t.boolean "status", default: false, null: false
    t.bigint "user_id", null: false
    t.bigint "business_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_pending_works_on_business_id"
    t.index ["user_id"], name: "index_pending_works_on_user_id"
  end

  create_table "places", force: :cascade do |t|
    t.string "name"
    t.boolean "is_activate", default: true
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_places_on_user_id"
  end

  create_table "purchase_goats", force: :cascade do |t|
    t.bigint "goat_id", null: false
    t.date "date_of_purchase"
    t.integer "rate"
    t.string "place"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["goat_id"], name: "index_purchase_goats_on_goat_id"
  end

  create_table "purchaser_bills", force: :cascade do |t|
    t.integer "asal_vhurai"
    t.integer "asal_kai"
    t.integer "neer_vattral_vhurai"
    t.integer "neer_vattral_kai"
    t.integer "number_of_person"
    t.integer "asal"
    t.integer "kai"
    t.integer "total_coconut"
    t.datetime "procurement_date"
    t.bigint "user_id", null: false
    t.bigint "purchaser_id", null: false
    t.bigint "vechicle_id", null: false
    t.bigint "farmer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "total_amount"
    t.boolean "paid", default: false
    t.datetime "paid_date"
    t.index ["farmer_id"], name: "index_purchaser_bills_on_farmer_id"
    t.index ["purchaser_id"], name: "index_purchaser_bills_on_purchaser_id"
    t.index ["user_id"], name: "index_purchaser_bills_on_user_id"
    t.index ["vechicle_id"], name: "index_purchaser_bills_on_vechicle_id"
  end

  create_table "purchasers", force: :cascade do |t|
    t.string "name"
    t.integer "is_activate"
    t.integer "mobile_number"
    t.bigint "user_id", null: false
    t.bigint "place_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_id"], name: "index_purchasers_on_place_id"
    t.index ["user_id"], name: "index_purchasers_on_user_id"
  end

  create_table "rack_details", force: :cascade do |t|
    t.string "name"
    t.integer "capacity"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_rack_details_on_user_id"
  end

  create_table "salaries", force: :cascade do |t|
    t.integer "knr_count"
    t.integer "pattai_count"
    t.integer "salary"
    t.datetime "date"
    t.bigint "user_id", null: false
    t.bigint "labour_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["labour_id"], name: "index_salaries_on_labour_id"
    t.index ["user_id"], name: "index_salaries_on_user_id"
  end

  create_table "sales_and_deaths", force: :cascade do |t|
    t.integer "male_count"
    t.integer "action_type"
    t.integer "goat_type"
    t.integer "female_count"
    t.date "date"
    t.bigint "business_id", null: false
    t.bigint "user_id", null: false
    t.integer "rate"
    t.float "total_weight"
    t.integer "total_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_sales_and_deaths_on_business_id"
    t.index ["user_id"], name: "index_sales_and_deaths_on_user_id"
  end

  create_table "user_employees", force: :cascade do |t|
    t.integer "role"
    t.boolean "active", default: true
    t.bigint "user_id", null: false
    t.bigint "business_id", null: false
    t.bigint "employee_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_user_employees_on_business_id"
    t.index ["employee_id"], name: "index_user_employees_on_employee_id"
    t.index ["user_id"], name: "index_user_employees_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "is_activate", default: true
    t.integer "type_of_business", default: 1
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vechicles", force: :cascade do |t|
    t.string "name"
    t.integer "capacity"
    t.boolean "is_activate", default: true
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_vechicles_on_user_id"
  end

  create_table "waters", force: :cascade do |t|
    t.integer "source", default: 1
    t.string "name"
    t.integer "level", default: 1
    t.string "details"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "business_id", null: false
    t.index ["business_id"], name: "index_waters_on_business_id"
  end

  add_foreign_key "batch_goats", "batches"
  add_foreign_key "batch_goats", "goats"
  add_foreign_key "batch_weights", "businesses"
  add_foreign_key "batch_weights", "month_on_weights"
  add_foreign_key "batch_weights", "rack_details"
  add_foreign_key "batch_weights", "users"
  add_foreign_key "batches", "rack_details"
  add_foreign_key "batches", "users"
  add_foreign_key "businesses", "users"
  add_foreign_key "calves", "cows"
  add_foreign_key "calving_periods", "cows"
  add_foreign_key "coconut_expenses", "users"
  add_foreign_key "coconut_invoices", "customers"
  add_foreign_key "coconut_invoices", "users"
  add_foreign_key "cows", "businesses"
  add_foreign_key "cows", "users"
  add_foreign_key "crop_activities", "land_crops"
  add_foreign_key "crop_yields", "land_crops"
  add_foreign_key "crops", "businesses"
  add_foreign_key "customer_accounts", "customers"
  add_foreign_key "customers", "places"
  add_foreign_key "customers", "users"
  add_foreign_key "daily_works", "goats"
  add_foreign_key "daily_works", "main_works"
  add_foreign_key "death_goats", "diseases"
  add_foreign_key "death_goats", "goats"
  add_foreign_key "disease_goats", "diseases"
  add_foreign_key "disease_goats", "goats"
  add_foreign_key "diseases", "users"
  add_foreign_key "farm_expenses", "businesses"
  add_foreign_key "farm_expenses", "users"
  add_foreign_key "farmer_accounts", "farmers"
  add_foreign_key "farmer_bills", "farmers"
  add_foreign_key "farmer_bills", "users"
  add_foreign_key "farmer_lands", "farmers"
  add_foreign_key "farmers", "places"
  add_foreign_key "farmers", "users"
  add_foreign_key "feed_availabilities", "businesses"
  add_foreign_key "feed_availabilities", "users"
  add_foreign_key "feeds", "businesses"
  add_foreign_key "feeds", "users"
  add_foreign_key "fertilizer_activities", "crop_activities"
  add_foreign_key "fertilizer_activities", "fertilizers"
  add_foreign_key "goat_calving_periods", "goats"
  add_foreign_key "goat_sales", "goats"
  add_foreign_key "goat_sales", "invoices"
  add_foreign_key "goat_weights", "goats"
  add_foreign_key "goats", "users"
  add_foreign_key "insurance_goats", "goats"
  add_foreign_key "invoices", "users"
  add_foreign_key "kopparais", "users"
  add_foreign_key "labour_accounts", "customers"
  add_foreign_key "labours", "places"
  add_foreign_key "labours", "users"
  add_foreign_key "land_crops", "businesses"
  add_foreign_key "land_crops", "crops"
  add_foreign_key "land_crops", "lands"
  add_foreign_key "land_waters", "businesses"
  add_foreign_key "land_waters", "lands"
  add_foreign_key "land_waters", "waters"
  add_foreign_key "lands", "businesses"
  add_foreign_key "loads", "coconut_invoices"
  add_foreign_key "main_works", "users"
  add_foreign_key "mate_goats", "goats", column: "female_id"
  add_foreign_key "mate_goats", "goats", column: "male_id"
  add_foreign_key "mattais", "users"
  add_foreign_key "milk_productions", "businesses"
  add_foreign_key "milk_productions", "cows"
  add_foreign_key "milk_productions", "users"
  add_foreign_key "month_on_weights", "businesses"
  add_foreign_key "month_on_weights", "users"
  add_foreign_key "parent_goats", "goats", column: "children_id"
  add_foreign_key "parent_goats", "goats", column: "parent_id"
  add_foreign_key "pending_works", "businesses"
  add_foreign_key "pending_works", "users"
  add_foreign_key "places", "users"
  add_foreign_key "purchase_goats", "goats"
  add_foreign_key "purchaser_bills", "purchasers"
  add_foreign_key "purchaser_bills", "users"
  add_foreign_key "purchaser_bills", "users", column: "farmer_id"
  add_foreign_key "purchaser_bills", "vechicles"
  add_foreign_key "purchasers", "places"
  add_foreign_key "purchasers", "users"
  add_foreign_key "rack_details", "users"
  add_foreign_key "salaries", "labours"
  add_foreign_key "salaries", "users"
  add_foreign_key "sales_and_deaths", "businesses"
  add_foreign_key "sales_and_deaths", "users"
  add_foreign_key "user_employees", "businesses"
  add_foreign_key "user_employees", "users"
  add_foreign_key "user_employees", "users", column: "employee_id"
  add_foreign_key "vechicles", "users"
  add_foreign_key "waters", "businesses"
end
