class AddColumnsToCustomer < ActiveRecord::Migration[7.1]
  def change
    add_column :customers, :account_number, :decimal
    add_column :customers, :bank, :string
    add_column :customers, :ifsc, :string
    add_column :customers, :branch, :string
  end
end
