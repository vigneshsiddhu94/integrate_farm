class CreatePurchaseGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_goats do |t|
	  t.references :goat, foreign_key: true, null: false
      t.date       :date_of_purchase
      t.integer    :rate
      t.string     :place

      t.timestamps null: false
    end
  end
end
