class CreateBatchWeights < ActiveRecord::Migration[7.1]
  def change
    create_table :batch_weights do |t|
      t.belongs_to :business, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true
      t.integer :kid_stage
      t.belongs_to :rack_detail, null: false, foreign_key: true
      t.integer :male_count
      t.integer :female_count
      t.integer :weight
      t.belongs_to :month_on_weight, null: false, foreign_key: true

      t.timestamps
    end
  end
end
