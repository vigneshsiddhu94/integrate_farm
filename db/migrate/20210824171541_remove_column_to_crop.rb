class RemoveColumnToCrop < ActiveRecord::Migration[5.2]
  def change
  	remove_column :crops, :part_number, :string
  end
end
