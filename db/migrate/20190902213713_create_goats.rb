class CreateGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :goats do |t|
    	t.integer     :token_no
      t.integer     :breed_variety, default: 1
      t.float       :weight
      t.date        :date_of_birth
      t.integer     :age
      t.integer     :live, default: 1

      t.timestamps null: false
    end
  end
end
