class CreateMateGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :mate_goats do |t|
      t.references :male
      t.references :female
      t.date 	   :date_of_mat
      t.string 	   :comment

      t.timestamps null: false
    end
    add_foreign_key :mate_goats, :goats, column: :male_id, primary_key: :id
    add_foreign_key :mate_goats, :goats, column: :female_id, primary_key: :id
    end
end
