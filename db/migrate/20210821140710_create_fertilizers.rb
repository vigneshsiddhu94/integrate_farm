class CreateFertilizers < ActiveRecord::Migration[5.2]
  def change
    create_table :fertilizers do |t|
		t.string   :name
		t.string   :details
      	t.timestamps null: false
    end
  end
end
