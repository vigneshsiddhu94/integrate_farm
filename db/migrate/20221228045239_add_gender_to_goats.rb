class AddGenderToGoats < ActiveRecord::Migration[5.2]
  def change
    add_column :goats, :gender, :integer
  end
end
