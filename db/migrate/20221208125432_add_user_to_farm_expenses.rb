class AddUserToFarmExpenses < ActiveRecord::Migration[5.2]
  def change
    add_reference :farm_expenses, :user, foreign_key: true
  end
end
