class CreateFarmerLands < ActiveRecord::Migration[7.0]
  def change
    create_table :farmer_lands do |t|
      t.string :acre
      t.integer :tree_count
      t.references :farmer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
