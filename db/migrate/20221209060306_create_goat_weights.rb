class CreateGoatWeights < ActiveRecord::Migration[5.2]
  def change
    create_table :goat_weights do |t|
      t.references :goat, foreign_key: true
      t.integer :weight
      t.date :date

      t.timestamps
    end
  end
end
