class CreateMonthOnWeights < ActiveRecord::Migration[7.1]
  def change
    create_table :month_on_weights do |t|
      t.integer :total_male_count
      t.integer :total_female_count
      t.integer :total_weight
      t.date :date
      t.belongs_to :business, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
