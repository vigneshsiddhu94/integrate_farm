class AddPaidToPurchaserBill < ActiveRecord::Migration[7.0]
  def change
    add_column :purchaser_bills, :paid, :boolean, default: false
    add_column :purchaser_bills, :paid_date, :datetime
  end
end
