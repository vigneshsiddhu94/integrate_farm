class CreateMainWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :main_works do |t|
      t.integer :breed_variety, null: false
      t.string  :day
      t.integer :action
      t.string  :action_detail

      t.timestamps null: false
    end
  end
end
