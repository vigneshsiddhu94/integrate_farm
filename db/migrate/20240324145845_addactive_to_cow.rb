class AddactiveToCow < ActiveRecord::Migration[7.0]
  def change
    add_column :calving_periods, :active, :boolean, default: false
  end
end
