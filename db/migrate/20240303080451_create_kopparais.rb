class CreateKopparais < ActiveRecord::Migration[7.0]
  def change
    create_table :kopparais do |t|
      t.integer :kg
      t.integer :rate
      t.datetime :date
      t.integer :total_amount
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
