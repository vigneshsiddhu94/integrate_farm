class DryFodders < ActiveRecord::Migration[5.2]
  def change
  	create_table :dry_fodders do |t|
		t.date		:purchase_date
		t.date		:close_date
		t.integer	:amount
		t.integer	:qty
		t.integer	:land_qty
		t.string	:comment
		t.integer	:variety, default: 1
		t.integer	:status, default: 1
		t.timestamps null: false	
    end
  end
end