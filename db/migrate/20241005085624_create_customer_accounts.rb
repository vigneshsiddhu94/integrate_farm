class CreateCustomerAccounts < ActiveRecord::Migration[7.1]
  def change
    create_table :customer_accounts do |t|
      t.decimal :amount, precision: 15, scale: 2
      t.string :reason
      t.date :date
      t.references :customer, null: false, foreign_key: true
      t.string :balance_amount

      t.timestamps
    end
  end
end
