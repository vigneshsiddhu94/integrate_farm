class CreateFeedRates < ActiveRecord::Migration[5.2]
  def change
    create_table :feed_rates do |t|
        t.string 	  :concentrate_feed
      	t.string 	  :green_fodder
      	t.string 	  :dry_fodder
        t.string    :chaval_powder
      	t.boolean   :status, default: true
      	t.timestamps
    end
  end
end
