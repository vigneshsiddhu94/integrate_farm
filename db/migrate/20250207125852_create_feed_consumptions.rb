class CreateFeedConsumptions < ActiveRecord::Migration[7.2]
  def change
    create_table :feed_consumptions do |t|
      t.date :date, null: false
      t.float :concentrate_feed, null: false, default: 0.0
      t.float :thippi, null: false, default: 0.0
      t.float :asola, null: false, default: 0.0
      t.float :dry_fodder, null: false, default: 0.0
      t.float :green_fodder, null: false, default: 0.0
      t.float :agathi_mulberry, null: false, default: 0.0
      t.float :silage, null: false, default: 0.0
      t.float :wastage_green, null: false, default: 0.0
      t.float :wastage_dry, null: false, default: 0.0
      t.string :cutting_land, null: false, default: ""

      t.timestamps
    end
  end
end
