class CreateLandCrops < ActiveRecord::Migration[5.2]
  def change
    create_table :land_crops do |t|
    t.references :land, foreign_key: true, null: false
    t.references :crop, foreign_key: true, null: false
    t.date	     :date
	t.integer	 :status, default: 1
    t.timestamps null: false
    end
  end
end
