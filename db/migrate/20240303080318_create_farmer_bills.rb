class CreateFarmerBills < ActiveRecord::Migration[7.0]
  def change
    create_table :farmer_bills do |t|
      t.integer :asal_kai_count
      t.integer :asal_kai_rate
      t.integer :neer_vattral__kai_count
      t.integer :neer_vattral_kai_rate
      t.integer :total_amount
      t.datetime :procurement_date
      t.references :user, null: false, foreign_key: true
      t.references :farmer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
