class CreatePurchaserBills < ActiveRecord::Migration[7.0]
  def change
    create_table :purchaser_bills do |t|
      t.integer :asal_vhurai
      t.integer :asal_kai
      t.integer :neer_vattral_vhurai
      t.integer :neer_vattral_kai
      t.integer :number_of_person
      t.integer :asal
      t.integer :kai
      t.integer :total_coconut
      t.datetime :procurement_date
      t.references :user, null: false, foreign_key: true
      t.references :purchaser, null: false, foreign_key: true
      t.references :vechicle, null: false, foreign_key: true
      t.references :farmer, null: false, foreign_key: true, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
