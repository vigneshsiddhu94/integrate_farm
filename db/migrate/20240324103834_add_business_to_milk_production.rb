class AddBusinessToMilkProduction < ActiveRecord::Migration[7.0]
  def change
    add_reference :milk_productions, :business, null: false, foreign_key: true
    add_reference :milk_productions, :user, null: false, foreign_key: true
  end
end
