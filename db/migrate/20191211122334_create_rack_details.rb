class CreateRackDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :rack_details do |t|
      t.string     :name
      t.string     :no
      t.integer	   :capacity

      t.timestamps null: false
    end
  end
end

