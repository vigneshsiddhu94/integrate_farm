class CreateFeeds < ActiveRecord::Migration[5.2]
  def change
    create_table :feeds do |t|
    	t.references :cow, foreign_key: true
    	t.string 	   :concentrate
      	t.string 	 :green
      	t.string 	 :dry
      	t.string 	 :chaval
      	t.boolean  :status, default: true
      	t.timestamps
    end
  end
end
