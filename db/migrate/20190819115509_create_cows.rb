class CreateCows < ActiveRecord::Migration[5.2]
  def change
    create_table :cows do |t|
      t.integer  :token_no
  		t.string   :milk_assurance
  		t.float    :cow_rate
  		t.date     :date_of_purchase
  		t.string   :breed_variety
  		t.string   :cow_location
  		t.string   :age
      	t.timestamps null: false
    end
  end
end
