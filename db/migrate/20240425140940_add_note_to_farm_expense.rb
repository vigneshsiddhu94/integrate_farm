class AddNoteToFarmExpense < ActiveRecord::Migration[7.1]
  def change
    add_column :farm_expenses, :note, :text
  end
end
