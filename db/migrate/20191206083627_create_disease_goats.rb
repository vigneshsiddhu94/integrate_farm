class CreateDiseaseGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :disease_goats do |t|
      t.references :goat, foreign_key: true, null: false
      t.references :disease, foreign_key: true
      t.string     :comment
      t.integer    :status, default: 1
      t.date       :disease_date

      t.timestamps null: false
    end
  end
end
