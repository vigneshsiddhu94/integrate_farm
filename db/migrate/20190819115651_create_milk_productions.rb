class CreateMilkProductions < ActiveRecord::Migration[5.2]
  def change
    create_table :milk_productions do |t|
    	t.references :cow, foreign_key: true
   		t.date       :milk_date
  		t.string     :day_time
  		t.float      :fat
  		t.float	     :snf
  		t.float	     :rate
    	t.float      :litre
    	t.timestamps null: false
    end
  end
end
