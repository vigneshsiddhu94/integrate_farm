class AddBusinessToWater < ActiveRecord::Migration[7.0]
  def change
    add_reference :waters, :business, null: false, foreign_key: true
  end
end
