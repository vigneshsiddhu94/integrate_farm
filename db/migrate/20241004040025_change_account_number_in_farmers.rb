class ChangeAccountNumberInFarmers < ActiveRecord::Migration[7.1]
  def change
    change_column :farmers, :account_number, :decimal, precision: 30, scale: 0
  end
end
