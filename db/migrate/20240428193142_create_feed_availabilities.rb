class CreateFeedAvailabilities < ActiveRecord::Migration[7.1]
  def change
    create_table :feed_availabilities do |t|
      t.date :date
      t.integer :fodder_variety
      t.integer :stored_kg
      t.integer :rate
      t.integer :total_amount
      t.integer :current_kg
      t.references :business, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.boolean :status, null: false, default: true

      t.timestamps
    end
  end
end
