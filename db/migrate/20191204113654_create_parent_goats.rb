class CreateParentGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :parent_goats do |t|
      t.references :children
      t.references :parent

      t.timestamps null: false
    end
    add_foreign_key :parent_goats, :goats, column: :children_id, primary_key: :id
    add_foreign_key :parent_goats, :goats, column: :parent_id, primary_key: :id
  end
end
