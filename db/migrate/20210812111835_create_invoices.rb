class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :total_amount
      t.date    :sale_date
      t.integer :discount

      t.timestamps
    end
  end
end
