class CreateLoads < ActiveRecord::Migration[7.0]
  def change
    create_table :loads do |t|
      t.integer :paruthi
      t.integer :sakku
      t.integer :rate
      t.integer :coconut_count
      t.integer :total_amount
      t.references :user, null: false, foreign_key: true
      t.references :coconut_invoice, null: false, foreign_key: true

      t.timestamps
    end
  end
end
