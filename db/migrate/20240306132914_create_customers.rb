class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :is_activate
      t.integer :mobile_number
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
