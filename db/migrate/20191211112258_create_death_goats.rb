class CreateDeathGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :death_goats do |t|
      t.references :goat, foreign_key: true, null: false
      t.references :disease, foreign_key: true, null: false
      t.date       :died_date
      t.text       :comment
      
      t.timestamps null: false
    end
  end
end
