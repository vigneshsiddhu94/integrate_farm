class AddWeightToDeathGoats < ActiveRecord::Migration[7.1]
  def change
    add_column :death_goats, :weight, :float
  end
end
