class CreateSalesAndDeaths < ActiveRecord::Migration[7.1]
  def change
    create_table :sales_and_deaths do |t|
      t.integer :male_count
      t.integer :action_type
      t.integer :goat_type
      t.integer :female_count
      t.date :date
      t.belongs_to :business, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true
      t.integer :rate
      t.float :total_weight
      t.integer :total_amount

      t.timestamps
    end
  end
end
