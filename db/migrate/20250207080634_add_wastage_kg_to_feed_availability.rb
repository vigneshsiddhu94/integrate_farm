class AddWastageKgToFeedAvailability < ActiveRecord::Migration[7.2]
  def change
    add_column :feed_availabilities, :wastage_kg, :float, precision: 10, scale: 2
  end
end
