class ChangeColumnTypeInKopparai < ActiveRecord::Migration[7.0]
  def change
    change_column :kopparais, :rate, :float
    change_column :kopparais, :kg, :float
    change_column :kopparais, :total_amount, :float
  end
end
