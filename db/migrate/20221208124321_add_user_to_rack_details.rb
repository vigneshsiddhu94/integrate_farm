class AddUserToRackDetails < ActiveRecord::Migration[5.2]
  def change
    add_reference :rack_details, :user, foreign_key: true
  end
end
