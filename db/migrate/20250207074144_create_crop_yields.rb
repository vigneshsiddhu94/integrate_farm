class CreateCropYields < ActiveRecord::Migration[7.2]
  def change
    create_table :crop_yields do |t|
      t.references :land_crop, null: false, foreign_key: true
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.decimal :yield_per_kg, precision: 10, scale: 2, null: false
      t.decimal :production_cost, precision: 10, scale: 2, null: false
      t.decimal :per_kg_rate, precision: 10, scale: 2, null: false
      t.text :comment

      t.timestamps
    end
  end
end
