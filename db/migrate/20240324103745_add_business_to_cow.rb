class AddBusinessToCow < ActiveRecord::Migration[7.0]
  def change
    add_reference :cows, :business, null: false, foreign_key: true
  end
end
