class AddUserToMainWorks < ActiveRecord::Migration[5.2]
  def change
    add_reference :main_works, :user, foreign_key: true
  end
end
