class ChangeColumnTypeInFarmerBill < ActiveRecord::Migration[7.0]
  def change
    change_column :farmer_bills, :asal_kai_rate, :float
    change_column :farmer_bills, :neer_vattral_kai_rate, :float
  end
end
