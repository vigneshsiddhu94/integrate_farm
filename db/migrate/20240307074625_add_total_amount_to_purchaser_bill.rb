class AddTotalAmountToPurchaserBill < ActiveRecord::Migration[7.0]
  def change
    add_column :purchaser_bills, :total_amount, :integer
  end
end
