class CreateVechicles < ActiveRecord::Migration[7.0]
  def change
    create_table :vechicles do |t|
      t.string :name
      t.integer :capacity
      t.boolean :is_activate, default: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
