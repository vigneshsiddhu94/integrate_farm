class RemoveCowReferenceAndAddBusinessUserReferencesToFeeds < ActiveRecord::Migration[7.1]
  def change
    remove_reference :feeds, :cow, foreign_key: true
    remove_column :feeds, :chaval, :string
    remove_column :feeds, :green, :string
    remove_column :feeds, :dry, :string
    remove_column :feeds, :concentrate, :string

    add_column :feeds, :date, :date
    add_column :feeds, :animal, :integer
    add_column :feeds, :concentrate, :integer
    add_column :feeds, :green_fodder, :integer
    add_column :feeds, :dry_fodder, :integer
    add_column :feeds, :dry_fodder_variety, :integer
    add_column :feeds, :green_fodder_variety, :integer
    add_column :feeds, :feed_cost, :integer
    add_reference :feeds, :business, foreign_key: true
    add_reference :feeds, :user, foreign_key: true
  end
end
