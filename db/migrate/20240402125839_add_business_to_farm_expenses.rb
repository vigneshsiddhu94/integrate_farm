class AddBusinessToFarmExpenses < ActiveRecord::Migration[7.0]
  def change
    add_reference :farm_expenses, :business, null: false, foreign_key: true
  end
end
