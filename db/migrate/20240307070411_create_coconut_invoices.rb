class CreateCoconutInvoices < ActiveRecord::Migration[7.0]
  def change
    create_table :coconut_invoices do |t|
      t.string :lorry
      t.integer :total_amount
      t.datetime :date
      t.references :user, null: false, foreign_key: true
      t.references :customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
