class CreateGoatSales < ActiveRecord::Migration[5.2]
  def change
    create_table :goat_sales do |t|
      t.references :goat, foreign_key: true, null: false
      t.references :invoice, foreign_key: true, null: false
      t.integer    :weight
      t.integer    :amount
      t.integer	   :purpose_of_sale

      t.timestamps
    end
  end
end
