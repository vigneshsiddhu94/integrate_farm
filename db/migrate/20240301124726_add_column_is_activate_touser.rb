class AddColumnIsActivateTouser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :is_activate, :boolean, default: true
    add_column :users, :type_of_business, :integer, default: 1
  end
end
