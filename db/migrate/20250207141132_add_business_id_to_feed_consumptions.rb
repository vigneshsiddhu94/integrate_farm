class AddBusinessIdToFeedConsumptions < ActiveRecord::Migration[7.2]
  def change
    add_column :feed_consumptions, :business_id, :integer, null: false
    add_index :feed_consumptions, :business_id  # Add index for performance
  end
end
