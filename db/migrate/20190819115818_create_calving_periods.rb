class CreateCalvingPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :calving_periods do |t|
		t.references :cow, foreign_key: true
    	t.string     :cow_status
    	t.date       :calve_date
    	t.string 	 :message
     	t.timestamps null: false
    end
  end
end
