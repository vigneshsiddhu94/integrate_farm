class AddPaidToFarmerBill < ActiveRecord::Migration[7.0]
  def change
    add_column :farmer_bills, :paid, :boolean, default: false
    add_column :farmer_bills, :paid_date, :datetime
  end
end
