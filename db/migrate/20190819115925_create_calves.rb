class CreateCalves < ActiveRecord::Migration[5.2]
  def change
    create_table :calves do |t|
    	t.references :cow, foreign_key: true
      t.integer    :token_no
  		t.date       :date_of_birth
  		t.string     :breed_variety
  		t.string     :gender
      t.timestamps null: false
    end
  end
end
