class AddColumnToCrops < ActiveRecord::Migration[5.2]
  def change
  	add_column :crops, :part_number, :string
  	add_column :crops, :variety , :integer
	add_column :crops, :nutrition, :string
	add_column :crops, :period, :integer
	add_column :crops, :seed_acre, :integer
	add_column :crops, :intercrop, :string
	add_column :crops, :water_requirement, :integer
	add_column :crops, :sowing_distance, :integer
	add_column :crops, :season, :integer
  end
end
