class CreateLandWaters < ActiveRecord::Migration[5.2]
  def change
    create_table :land_waters do |t|
    	t.references :water, foreign_key: true, null: false
		t.references :land, foreign_key: true, null: false
		t.date	     :date
		t.integer	 :status, default: 1
		t.timestamps null: false	
    end
  end
end
