class CreateWaters < ActiveRecord::Migration[5.2]
  def change
    create_table :waters do |t|
    	t.integer	:source, default: 1
    	t.string	:name
    	t.integer	:level, default: 1
    	t.string	:details
      t.timestamps null: false
    end
  end
end
