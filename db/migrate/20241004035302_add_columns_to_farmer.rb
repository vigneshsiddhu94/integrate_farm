class AddColumnsToFarmer < ActiveRecord::Migration[7.1]
  def change
    add_column :farmers, :account_number, :decimal
    add_column :farmers, :bank, :string
    add_column :farmers, :ifsc, :string
    add_column :farmers, :branch, :string
  end
end
