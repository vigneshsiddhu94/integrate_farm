class AddDateToGoatCalvingPeriods < ActiveRecord::Migration[5.2]
  def change
    add_column :goat_calving_periods, :date, :date
  end
end
