class ChangeDateToBeDatetimeInGoatCalvingPeriods < ActiveRecord::Migration[5.2]
  def change
  	  change_column :goat_calving_periods, :date, :datetime
  end
end
