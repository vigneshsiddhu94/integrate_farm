class ChangeColumnStatusInPendingWorks < ActiveRecord::Migration[7.1]
  def change
    change_column_default :pending_works, :status, from: true, to: false
  end
end
