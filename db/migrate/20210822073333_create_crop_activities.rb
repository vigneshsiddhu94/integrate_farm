class CreateCropActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :crop_activities do |t|
    	t.references :land_crop
		t.integer	 :activity
		t.date		 :date
		t.string 	 :comment
		t.date		 :end_date
		t.integer	 :status, default: 1
      	t.timestamps null: false
    end
    add_foreign_key :crop_activities, :land_crops, column: :land_crop_id
  end
end