class AddColumnsToFarmerBill < ActiveRecord::Migration[7.1]
  def change
    add_column :farmer_bills, :asal_kai_vasi, :integer
    add_column :farmer_bills, :neer_vatral_kai_vasi, :integer
  end
end
