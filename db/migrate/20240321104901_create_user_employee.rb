class CreateUserEmployee < ActiveRecord::Migration[7.0]
  def change
    create_table :user_employees do |t|
      t.integer :role
      t.boolean :active, default: true
      t.references :user, null: false, foreign_key: true
      t.references :business, null: false, foreign_key: true
      t.references :employee, null: false, foreign_key: { to_table: :users }
      t.timestamps
    end
  end
end
