class CreateMattais < ActiveRecord::Migration[7.0]
  def change
    create_table :mattais do |t|
      t.integer :load
      t.integer :rate
      t.datetime :date
      t.integer :total_amount
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
