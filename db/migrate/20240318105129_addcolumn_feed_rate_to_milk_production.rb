class AddcolumnFeedRateToMilkProduction < ActiveRecord::Migration[7.0]
  def change
    add_column :milk_productions, :feed_rate, :integer
  end
end