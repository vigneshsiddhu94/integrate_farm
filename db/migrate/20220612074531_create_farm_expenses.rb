class CreateFarmExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :farm_expenses do |t|
      t.datetime :date
      t.integer :category
      t.integer :amount

      t.timestamps
    end
  end
end
