class RemoveColumns < ActiveRecord::Migration[5.2]
  def self.up
    remove_column :rack_details, :no
  end

  def self.down
    add_column :rack_details, :no, :string
  end
end
