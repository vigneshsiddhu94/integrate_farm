class AddPlaceToCustomers < ActiveRecord::Migration[7.1]
  def change
    add_reference :customers, :place, foreign_key: true, null: true
  end
end
