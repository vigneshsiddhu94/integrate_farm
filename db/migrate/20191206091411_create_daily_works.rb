class CreateDailyWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :daily_works do |t|
      t.references :goat, foreign_key: true, null: false
      t.references :main_work, foreign_key: true, null: false
      t.string 	   :comment
      t.integer    :status, default: 1

      t.timestamps null: false
    end
  end
end
