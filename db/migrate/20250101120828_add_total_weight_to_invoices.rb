class AddTotalWeightToInvoices < ActiveRecord::Migration[7.1]
  def change
    add_column :invoices, :total_weight, :float, precision: 10, scale: 2
  end
end
