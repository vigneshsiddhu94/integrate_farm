class CreateFertilizerActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :fertilizer_activities do |t|
    	t.references	:crop_activity, foreign_key: true, null: false
    	t.references	:fertilizer, foreign_key: true, null: false
    	t.date		    :date
    	t.timestamps null: false
    end
  end
end
