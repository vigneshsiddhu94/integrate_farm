class CreateInsuranceGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :insurance_goats do |t|
      t.references :goat, foreign_key: true, null: false
      t.string     :policy_no
      t.date       :start_date
      t.date       :exp_date
      t.integer    :amount

      t.timestamps null: false
    end
  end
end
