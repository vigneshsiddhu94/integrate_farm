class CreatePendingWorks < ActiveRecord::Migration[7.0]
  def change
    create_table :pending_works do |t|
      t.string :work
      t.integer :work_type
      t.datetime :date
      t.boolean :status, null: false, default: true
      t.references :user, null: false, foreign_key: true
      t.references :business, null: false, foreign_key: true

      t.timestamps
    end
  end
end
