class CreateGoatCalvingPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :goat_calving_periods do |t|
		t.references :goat, foreign_key: true, null: false
		t.integer    :goat_status
		t.integer    :live, default: 1
		t.string	 :message
		
		t.timestamps null: false
    end
  end
end
