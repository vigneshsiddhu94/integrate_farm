class CreateLands < ActiveRecord::Migration[5.2]
  def change
    create_table :lands do |t|
		t.string     :name
		t.integer    :cent
		t.integer	 :soil, default: 1
      	t.timestamps null: false
    end
  end
end
