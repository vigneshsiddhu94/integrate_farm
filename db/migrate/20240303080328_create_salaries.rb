class CreateSalaries < ActiveRecord::Migration[7.0]
  def change
    create_table :salaries do |t|
      t.integer :knr_count
      t.integer :pattai_count
      t.integer :salary
      t.datetime :date
      t.references :user, null: false, foreign_key: true
      t.references :labour, null: false, foreign_key: true

      t.timestamps
    end
  end
end
