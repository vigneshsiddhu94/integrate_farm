class ChangeAccountNumberInCustomers < ActiveRecord::Migration[7.1]
  def change
    change_column :customers, :account_number, :decimal, precision: 30, scale: 0
  end
end
