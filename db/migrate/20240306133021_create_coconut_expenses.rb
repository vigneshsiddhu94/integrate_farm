class CreateCoconutExpenses < ActiveRecord::Migration[7.0]
  def change
    create_table :coconut_expenses do |t|
      t.integer :reason
      t.string :name
      t.datetime :date
      t.integer :total_amount
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
