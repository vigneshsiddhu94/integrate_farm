class CreateBatchGoats < ActiveRecord::Migration[5.2]
  def change
    create_table :batch_goats do |t|
      t.references :batch, foreign_key: true
      t.references :goat, foreign_key: true
      t.integer :status, default: 1
      t.date :date

      t.timestamps
    end
  end
end
