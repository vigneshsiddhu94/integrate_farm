class AddBusinessToLand < ActiveRecord::Migration[7.0]
  def change
    add_reference :lands, :business, null: false, foreign_key: true
    add_reference :crops, :business, null: false, foreign_key: true
    add_reference :land_crops, :business, null: false, foreign_key: true
    add_reference :land_waters, :business, null: false, foreign_key: true
  end
end
