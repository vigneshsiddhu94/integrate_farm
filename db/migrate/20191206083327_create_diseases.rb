class CreateDiseases < ActiveRecord::Migration[5.2]
  def change
    create_table :diseases do |t|
      t.string :name
      t.string :symptoms
      t.string :impact_goat
      t.string :treatment

      t.timestamps null: false
    end
  end
end
