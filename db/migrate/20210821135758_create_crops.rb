class CreateCrops < ActiveRecord::Migration[5.2]
  def change
    create_table :crops do |t|
		t.string	:name
    	t.integer   :yield
		t.string    :details
    	t.timestamps null: false
    end
  end
end
