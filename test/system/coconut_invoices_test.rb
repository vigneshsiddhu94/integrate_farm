require "application_system_test_case"

class CoconutInvoicesTest < ApplicationSystemTestCase
  setup do
    @coconut_invoice = coconut_invoices(:one)
  end

  test "visiting the index" do
    visit coconut_invoices_url
    assert_selector "h1", text: "Coconut invoices"
  end

  test "should create coconut invoice" do
    visit coconut_invoices_url
    click_on "New coconut invoice"

    fill_in "Customer", with: @coconut_invoice.customer_id
    fill_in "Date", with: @coconut_invoice.date
    fill_in "Load", with: @coconut_invoice.load_id
    fill_in "Lorry", with: @coconut_invoice.lorry
    fill_in "Total amount", with: @coconut_invoice.total_amount
    fill_in "User", with: @coconut_invoice.user_id
    click_on "Create Coconut invoice"

    assert_text "Coconut invoice was successfully created"
    click_on "Back"
  end

  test "should update Coconut invoice" do
    visit coconut_invoice_url(@coconut_invoice)
    click_on "Edit this coconut invoice", match: :first

    fill_in "Customer", with: @coconut_invoice.customer_id
    fill_in "Date", with: @coconut_invoice.date
    fill_in "Load", with: @coconut_invoice.load_id
    fill_in "Lorry", with: @coconut_invoice.lorry
    fill_in "Total amount", with: @coconut_invoice.total_amount
    fill_in "User", with: @coconut_invoice.user_id
    click_on "Update Coconut invoice"

    assert_text "Coconut invoice was successfully updated"
    click_on "Back"
  end

  test "should destroy Coconut invoice" do
    visit coconut_invoice_url(@coconut_invoice)
    click_on "Destroy this coconut invoice", match: :first

    assert_text "Coconut invoice was successfully destroyed"
  end
end
