require "application_system_test_case"

class FarmerLandsTest < ApplicationSystemTestCase
  setup do
    @farmer_land = farmer_lands(:one)
  end

  test "visiting the index" do
    visit farmer_lands_url
    assert_selector "h1", text: "Farmer lands"
  end

  test "should create farmer land" do
    visit farmer_lands_url
    click_on "New farmer land"

    fill_in "Acre", with: @farmer_land.acre
    fill_in "Farmer", with: @farmer_land.farmer_id
    fill_in "Tree count", with: @farmer_land.tree_count
    click_on "Create Farmer land"

    assert_text "Farmer land was successfully created"
    click_on "Back"
  end

  test "should update Farmer land" do
    visit farmer_land_url(@farmer_land)
    click_on "Edit this farmer land", match: :first

    fill_in "Acre", with: @farmer_land.acre
    fill_in "Farmer", with: @farmer_land.farmer_id
    fill_in "Tree count", with: @farmer_land.tree_count
    click_on "Update Farmer land"

    assert_text "Farmer land was successfully updated"
    click_on "Back"
  end

  test "should destroy Farmer land" do
    visit farmer_land_url(@farmer_land)
    click_on "Destroy this farmer land", match: :first

    assert_text "Farmer land was successfully destroyed"
  end
end
