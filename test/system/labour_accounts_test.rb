require "application_system_test_case"

class LabourAccountsTest < ApplicationSystemTestCase
  setup do
    @labour_account = labour_accounts(:one)
  end

  test "visiting the index" do
    visit labour_accounts_url
    assert_selector "h1", text: "Labour accounts"
  end

  test "should create labour account" do
    visit labour_accounts_url
    click_on "New labour account"

    fill_in "Amount", with: @labour_account.amount
    fill_in "Balance amount", with: @labour_account.balance_amount
    fill_in "Customer", with: @labour_account.customer_id
    fill_in "Date", with: @labour_account.date
    fill_in "Reason", with: @labour_account.reason
    click_on "Create Labour account"

    assert_text "Labour account was successfully created"
    click_on "Back"
  end

  test "should update Labour account" do
    visit labour_account_url(@labour_account)
    click_on "Edit this labour account", match: :first

    fill_in "Amount", with: @labour_account.amount
    fill_in "Balance amount", with: @labour_account.balance_amount
    fill_in "Customer", with: @labour_account.customer_id
    fill_in "Date", with: @labour_account.date
    fill_in "Reason", with: @labour_account.reason
    click_on "Update Labour account"

    assert_text "Labour account was successfully updated"
    click_on "Back"
  end

  test "should destroy Labour account" do
    visit labour_account_url(@labour_account)
    click_on "Destroy this labour account", match: :first

    assert_text "Labour account was successfully destroyed"
  end
end
