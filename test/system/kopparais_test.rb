require "application_system_test_case"

class KopparaisTest < ApplicationSystemTestCase
  setup do
    @kopparai = kopparais(:one)
  end

  test "visiting the index" do
    visit kopparais_url
    assert_selector "h1", text: "Kopparais"
  end

  test "should create kopparai" do
    visit kopparais_url
    click_on "New kopparai"

    fill_in "Date", with: @kopparai.date
    fill_in "Kg", with: @kopparai.kg
    fill_in "Rate", with: @kopparai.rate
    fill_in "Total amount", with: @kopparai.total_amount
    fill_in "User", with: @kopparai.user_id
    click_on "Create Kopparai"

    assert_text "Kopparai was successfully created"
    click_on "Back"
  end

  test "should update Kopparai" do
    visit kopparai_url(@kopparai)
    click_on "Edit this kopparai", match: :first

    fill_in "Date", with: @kopparai.date
    fill_in "Kg", with: @kopparai.kg
    fill_in "Rate", with: @kopparai.rate
    fill_in "Total amount", with: @kopparai.total_amount
    fill_in "User", with: @kopparai.user_id
    click_on "Update Kopparai"

    assert_text "Kopparai was successfully updated"
    click_on "Back"
  end

  test "should destroy Kopparai" do
    visit kopparai_url(@kopparai)
    click_on "Destroy this kopparai", match: :first

    assert_text "Kopparai was successfully destroyed"
  end
end
