require "application_system_test_case"

class FarmExpensesTest < ApplicationSystemTestCase
  setup do
    @farm_expense = farm_expenses(:one)
  end

  test "visiting the index" do
    visit farm_expenses_url
    assert_selector "h1", text: "Farm Expenses"
  end

  test "creating a Farm expense" do
    visit farm_expenses_url
    click_on "New Farm Expense"

    fill_in "Amount", with: @farm_expense.amount
    fill_in "Category", with: @farm_expense.category
    fill_in "Date", with: @farm_expense.date
    click_on "Create Farm expense"

    assert_text "Farm expense was successfully created"
    click_on "Back"
  end

  test "updating a Farm expense" do
    visit farm_expenses_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @farm_expense.amount
    fill_in "Category", with: @farm_expense.category
    fill_in "Date", with: @farm_expense.date
    click_on "Update Farm expense"

    assert_text "Farm expense was successfully updated"
    click_on "Back"
  end

  test "destroying a Farm expense" do
    visit farm_expenses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Farm expense was successfully destroyed"
  end
end
