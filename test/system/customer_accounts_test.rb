require "application_system_test_case"

class CustomerAccountsTest < ApplicationSystemTestCase
  setup do
    @customer_account = customer_accounts(:one)
  end

  test "visiting the index" do
    visit customer_accounts_url
    assert_selector "h1", text: "Customer accounts"
  end

  test "should create customer account" do
    visit customer_accounts_url
    click_on "New customer account"

    fill_in "Amount", with: @customer_account.amount
    fill_in "Balance amount", with: @customer_account.balance_amount
    fill_in "Customer", with: @customer_account.customer_id
    fill_in "Date", with: @customer_account.date
    fill_in "Reason", with: @customer_account.reason
    click_on "Create Customer account"

    assert_text "Customer account was successfully created"
    click_on "Back"
  end

  test "should update Customer account" do
    visit customer_account_url(@customer_account)
    click_on "Edit this customer account", match: :first

    fill_in "Amount", with: @customer_account.amount
    fill_in "Balance amount", with: @customer_account.balance_amount
    fill_in "Customer", with: @customer_account.customer_id
    fill_in "Date", with: @customer_account.date
    fill_in "Reason", with: @customer_account.reason
    click_on "Update Customer account"

    assert_text "Customer account was successfully updated"
    click_on "Back"
  end

  test "should destroy Customer account" do
    visit customer_account_url(@customer_account)
    click_on "Destroy this customer account", match: :first

    assert_text "Customer account was successfully destroyed"
  end
end
