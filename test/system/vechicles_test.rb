require "application_system_test_case"

class VechiclesTest < ApplicationSystemTestCase
  setup do
    @vechicle = vechicles(:one)
  end

  test "visiting the index" do
    visit vechicles_url
    assert_selector "h1", text: "Vechicles"
  end

  test "should create vechicle" do
    visit vechicles_url
    click_on "New vechicle"

    fill_in "Capacity", with: @vechicle.capacity
    fill_in "Is activate", with: @vechicle.is_activate
    fill_in "Name", with: @vechicle.name
    fill_in "User", with: @vechicle.user_id
    click_on "Create Vechicle"

    assert_text "Vechicle was successfully created"
    click_on "Back"
  end

  test "should update Vechicle" do
    visit vechicle_url(@vechicle)
    click_on "Edit this vechicle", match: :first

    fill_in "Capacity", with: @vechicle.capacity
    fill_in "Is activate", with: @vechicle.is_activate
    fill_in "Name", with: @vechicle.name
    fill_in "User", with: @vechicle.user_id
    click_on "Update Vechicle"

    assert_text "Vechicle was successfully updated"
    click_on "Back"
  end

  test "should destroy Vechicle" do
    visit vechicle_url(@vechicle)
    click_on "Destroy this vechicle", match: :first

    assert_text "Vechicle was successfully destroyed"
  end
end
