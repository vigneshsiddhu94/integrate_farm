require "application_system_test_case"

class FarmerBillsTest < ApplicationSystemTestCase
  setup do
    @farmer_bill = farmer_bills(:one)
  end

  test "visiting the index" do
    visit farmer_bills_url
    assert_selector "h1", text: "Farmer bills"
  end

  test "should create farmer bill" do
    visit farmer_bills_url
    click_on "New farmer bill"

    fill_in "Asal kai count", with: @farmer_bill.asal_kai_count
    fill_in "Asal kai rate", with: @farmer_bill.asal_kai_rate
    fill_in "Farmer", with: @farmer_bill.farmer_id
    fill_in "Neer vattral  kai count", with: @farmer_bill.neer_vattral__kai_count
    fill_in "Neer vattral kai rate", with: @farmer_bill.neer_vattral_kai_rate
    fill_in "Procurement date", with: @farmer_bill.procurement_date
    fill_in "Total amount", with: @farmer_bill.total_amount
    fill_in "User", with: @farmer_bill.user_id
    click_on "Create Farmer bill"

    assert_text "Farmer bill was successfully created"
    click_on "Back"
  end

  test "should update Farmer bill" do
    visit farmer_bill_url(@farmer_bill)
    click_on "Edit this farmer bill", match: :first

    fill_in "Asal kai count", with: @farmer_bill.asal_kai_count
    fill_in "Asal kai rate", with: @farmer_bill.asal_kai_rate
    fill_in "Farmer", with: @farmer_bill.farmer_id
    fill_in "Neer vattral  kai count", with: @farmer_bill.neer_vattral__kai_count
    fill_in "Neer vattral kai rate", with: @farmer_bill.neer_vattral_kai_rate
    fill_in "Procurement date", with: @farmer_bill.procurement_date
    fill_in "Total amount", with: @farmer_bill.total_amount
    fill_in "User", with: @farmer_bill.user_id
    click_on "Update Farmer bill"

    assert_text "Farmer bill was successfully updated"
    click_on "Back"
  end

  test "should destroy Farmer bill" do
    visit farmer_bill_url(@farmer_bill)
    click_on "Destroy this farmer bill", match: :first

    assert_text "Farmer bill was successfully destroyed"
  end
end
