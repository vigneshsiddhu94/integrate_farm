require "application_system_test_case"

class FeedAvailabilitiesTest < ApplicationSystemTestCase
  setup do
    @feed_availability = feed_availabilities(:one)
  end

  test "visiting the index" do
    visit feed_availabilities_url
    assert_selector "h1", text: "Feed availabilities"
  end

  test "should create feed availability" do
    visit feed_availabilities_url
    click_on "New feed availability"

    fill_in "Business", with: @feed_availability.business_id
    fill_in "Current kg", with: @feed_availability.current_kg
    fill_in "Date", with: @feed_availability.date
    fill_in "Fodder variety", with: @feed_availability.fodder_variety
    fill_in "Rate", with: @feed_availability.rate
    check "Status" if @feed_availability.status
    fill_in "Stored kg", with: @feed_availability.stored_kg
    fill_in "Total amount", with: @feed_availability.total_amount
    fill_in "User", with: @feed_availability.user_id
    click_on "Create Feed availability"

    assert_text "Feed availability was successfully created"
    click_on "Back"
  end

  test "should update Feed availability" do
    visit feed_availability_url(@feed_availability)
    click_on "Edit this feed availability", match: :first

    fill_in "Business", with: @feed_availability.business_id
    fill_in "Current kg", with: @feed_availability.current_kg
    fill_in "Date", with: @feed_availability.date
    fill_in "Fodder variety", with: @feed_availability.fodder_variety
    fill_in "Rate", with: @feed_availability.rate
    check "Status" if @feed_availability.status
    fill_in "Stored kg", with: @feed_availability.stored_kg
    fill_in "Total amount", with: @feed_availability.total_amount
    fill_in "User", with: @feed_availability.user_id
    click_on "Update Feed availability"

    assert_text "Feed availability was successfully updated"
    click_on "Back"
  end

  test "should destroy Feed availability" do
    visit feed_availability_url(@feed_availability)
    click_on "Destroy this feed availability", match: :first

    assert_text "Feed availability was successfully destroyed"
  end
end
