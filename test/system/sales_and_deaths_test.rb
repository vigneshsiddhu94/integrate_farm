require "application_system_test_case"

class SalesAndDeathsTest < ApplicationSystemTestCase
  setup do
    @sales_and_death = sales_and_deaths(:one)
  end

  test "visiting the index" do
    visit sales_and_deaths_url
    assert_selector "h1", text: "Sales and deaths"
  end

  test "should create sales and death" do
    visit sales_and_deaths_url
    click_on "New sales and death"

    fill_in "Action type", with: @sales_and_death.action_type
    fill_in "Business", with: @sales_and_death.business_id
    fill_in "Date", with: @sales_and_death.date
    fill_in "Female count", with: @sales_and_death.female_count
    fill_in "Goat type", with: @sales_and_death.goat_type
    fill_in "Male count", with: @sales_and_death.male_count
    fill_in "Rate", with: @sales_and_death.rate
    fill_in "Total amount", with: @sales_and_death.total_amount
    fill_in "Total weight", with: @sales_and_death.total_weight
    fill_in "User", with: @sales_and_death.user_id
    click_on "Create Sales and death"

    assert_text "Sales and death was successfully created"
    click_on "Back"
  end

  test "should update Sales and death" do
    visit sales_and_death_url(@sales_and_death)
    click_on "Edit this sales and death", match: :first

    fill_in "Action type", with: @sales_and_death.action_type
    fill_in "Business", with: @sales_and_death.business_id
    fill_in "Date", with: @sales_and_death.date
    fill_in "Female count", with: @sales_and_death.female_count
    fill_in "Goat type", with: @sales_and_death.goat_type
    fill_in "Male count", with: @sales_and_death.male_count
    fill_in "Rate", with: @sales_and_death.rate
    fill_in "Total amount", with: @sales_and_death.total_amount
    fill_in "Total weight", with: @sales_and_death.total_weight
    fill_in "User", with: @sales_and_death.user_id
    click_on "Update Sales and death"

    assert_text "Sales and death was successfully updated"
    click_on "Back"
  end

  test "should destroy Sales and death" do
    visit sales_and_death_url(@sales_and_death)
    click_on "Destroy this sales and death", match: :first

    assert_text "Sales and death was successfully destroyed"
  end
end
