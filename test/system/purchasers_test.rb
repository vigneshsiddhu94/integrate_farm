require "application_system_test_case"

class PurchasersTest < ApplicationSystemTestCase
  setup do
    @purchaser = purchasers(:one)
  end

  test "visiting the index" do
    visit purchasers_url
    assert_selector "h1", text: "Purchasers"
  end

  test "should create purchaser" do
    visit purchasers_url
    click_on "New purchaser"

    fill_in "Is activate", with: @purchaser.is_activate
    fill_in "Mobile number", with: @purchaser.mobile_number
    fill_in "Name", with: @purchaser.name
    fill_in "Place", with: @purchaser.place_id
    fill_in "User", with: @purchaser.user_id
    click_on "Create Purchaser"

    assert_text "Purchaser was successfully created"
    click_on "Back"
  end

  test "should update Purchaser" do
    visit purchaser_url(@purchaser)
    click_on "Edit this purchaser", match: :first

    fill_in "Is activate", with: @purchaser.is_activate
    fill_in "Mobile number", with: @purchaser.mobile_number
    fill_in "Name", with: @purchaser.name
    fill_in "Place", with: @purchaser.place_id
    fill_in "User", with: @purchaser.user_id
    click_on "Update Purchaser"

    assert_text "Purchaser was successfully updated"
    click_on "Back"
  end

  test "should destroy Purchaser" do
    visit purchaser_url(@purchaser)
    click_on "Destroy this purchaser", match: :first

    assert_text "Purchaser was successfully destroyed"
  end
end
