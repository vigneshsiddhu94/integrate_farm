require "application_system_test_case"

class MonthOnWeightsTest < ApplicationSystemTestCase
  setup do
    @month_on_weight = month_on_weights(:one)
  end

  test "visiting the index" do
    visit month_on_weights_url
    assert_selector "h1", text: "Month on weights"
  end

  test "should create month on weight" do
    visit month_on_weights_url
    click_on "New month on weight"

    fill_in "Business", with: @month_on_weight.business_id
    fill_in "Date", with: @month_on_weight.date
    fill_in "Total female count", with: @month_on_weight.total_female_count
    fill_in "Total male count", with: @month_on_weight.total_male_count
    fill_in "Total weight", with: @month_on_weight.total_weight
    fill_in "User", with: @month_on_weight.user_id
    click_on "Create Month on weight"

    assert_text "Month on weight was successfully created"
    click_on "Back"
  end

  test "should update Month on weight" do
    visit month_on_weight_url(@month_on_weight)
    click_on "Edit this month on weight", match: :first

    fill_in "Business", with: @month_on_weight.business_id
    fill_in "Date", with: @month_on_weight.date
    fill_in "Total female count", with: @month_on_weight.total_female_count
    fill_in "Total male count", with: @month_on_weight.total_male_count
    fill_in "Total weight", with: @month_on_weight.total_weight
    fill_in "User", with: @month_on_weight.user_id
    click_on "Update Month on weight"

    assert_text "Month on weight was successfully updated"
    click_on "Back"
  end

  test "should destroy Month on weight" do
    visit month_on_weight_url(@month_on_weight)
    click_on "Destroy this month on weight", match: :first

    assert_text "Month on weight was successfully destroyed"
  end
end
