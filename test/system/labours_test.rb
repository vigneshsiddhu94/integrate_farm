require "application_system_test_case"

class LaboursTest < ApplicationSystemTestCase
  setup do
    @labour = labours(:one)
  end

  test "visiting the index" do
    visit labours_url
    assert_selector "h1", text: "Labours"
  end

  test "should create labour" do
    visit labours_url
    click_on "New labour"

    fill_in "Is activate", with: @labour.is_activate
    fill_in "Mobile number", with: @labour.mobile_number
    fill_in "Name", with: @labour.name
    fill_in "Place", with: @labour.place_id
    fill_in "User", with: @labour.user_id
    click_on "Create Labour"

    assert_text "Labour was successfully created"
    click_on "Back"
  end

  test "should update Labour" do
    visit labour_url(@labour)
    click_on "Edit this labour", match: :first

    fill_in "Is activate", with: @labour.is_activate
    fill_in "Mobile number", with: @labour.mobile_number
    fill_in "Name", with: @labour.name
    fill_in "Place", with: @labour.place_id
    fill_in "User", with: @labour.user_id
    click_on "Update Labour"

    assert_text "Labour was successfully updated"
    click_on "Back"
  end

  test "should destroy Labour" do
    visit labour_url(@labour)
    click_on "Destroy this labour", match: :first

    assert_text "Labour was successfully destroyed"
  end
end
