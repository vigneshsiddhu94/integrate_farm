require "application_system_test_case"

class MattaisTest < ApplicationSystemTestCase
  setup do
    @mattai = mattais(:one)
  end

  test "visiting the index" do
    visit mattais_url
    assert_selector "h1", text: "Mattais"
  end

  test "should create mattai" do
    visit mattais_url
    click_on "New mattai"

    fill_in "Date", with: @mattai.date
    fill_in "Load", with: @mattai.load
    fill_in "Rate", with: @mattai.rate
    fill_in "Total amount", with: @mattai.total_amount
    fill_in "User", with: @mattai.user_id
    click_on "Create Mattai"

    assert_text "Mattai was successfully created"
    click_on "Back"
  end

  test "should update Mattai" do
    visit mattai_url(@mattai)
    click_on "Edit this mattai", match: :first

    fill_in "Date", with: @mattai.date
    fill_in "Load", with: @mattai.load
    fill_in "Rate", with: @mattai.rate
    fill_in "Total amount", with: @mattai.total_amount
    fill_in "User", with: @mattai.user_id
    click_on "Update Mattai"

    assert_text "Mattai was successfully updated"
    click_on "Back"
  end

  test "should destroy Mattai" do
    visit mattai_url(@mattai)
    click_on "Destroy this mattai", match: :first

    assert_text "Mattai was successfully destroyed"
  end
end
