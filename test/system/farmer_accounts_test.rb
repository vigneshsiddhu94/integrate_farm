require "application_system_test_case"

class FarmerAccountsTest < ApplicationSystemTestCase
  setup do
    @farmer_account = farmer_accounts(:one)
  end

  test "visiting the index" do
    visit farmer_accounts_url
    assert_selector "h1", text: "Farmer accounts"
  end

  test "should create farmer account" do
    visit farmer_accounts_url
    click_on "New farmer account"

    fill_in "Amount", with: @farmer_account.amount
    fill_in "Balance amount", with: @farmer_account.balance_amount
    fill_in "Date", with: @farmer_account.date
    fill_in "Farmer", with: @farmer_account.farmer_id
    fill_in "Reason", with: @farmer_account.reason
    click_on "Create Farmer account"

    assert_text "Farmer account was successfully created"
    click_on "Back"
  end

  test "should update Farmer account" do
    visit farmer_account_url(@farmer_account)
    click_on "Edit this farmer account", match: :first

    fill_in "Amount", with: @farmer_account.amount
    fill_in "Balance amount", with: @farmer_account.balance_amount
    fill_in "Date", with: @farmer_account.date
    fill_in "Farmer", with: @farmer_account.farmer_id
    fill_in "Reason", with: @farmer_account.reason
    click_on "Update Farmer account"

    assert_text "Farmer account was successfully updated"
    click_on "Back"
  end

  test "should destroy Farmer account" do
    visit farmer_account_url(@farmer_account)
    click_on "Destroy this farmer account", match: :first

    assert_text "Farmer account was successfully destroyed"
  end
end
