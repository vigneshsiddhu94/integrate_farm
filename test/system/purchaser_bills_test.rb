require "application_system_test_case"

class PurchaserBillsTest < ApplicationSystemTestCase
  setup do
    @purchaser_bill = purchaser_bills(:one)
  end

  test "visiting the index" do
    visit purchaser_bills_url
    assert_selector "h1", text: "Purchaser bills"
  end

  test "should create purchaser bill" do
    visit purchaser_bills_url
    click_on "New purchaser bill"

    fill_in "Asal", with: @purchaser_bill.asal
    fill_in "Asal kai", with: @purchaser_bill.asal_kai
    fill_in "Asal vhurai", with: @purchaser_bill.asal_vhurai
    fill_in "Farmer", with: @purchaser_bill.farmer_id
    fill_in "Kai", with: @purchaser_bill.kai
    fill_in "Neer vattral kai", with: @purchaser_bill.neer_vattral_kai
    fill_in "Neer vattral vhurai", with: @purchaser_bill.neer_vattral_vhurai
    fill_in "Number of person", with: @purchaser_bill.number_of_person
    fill_in "Place", with: @purchaser_bill.place_id
    fill_in "Procurement date", with: @purchaser_bill.procurement_date
    fill_in "Procurement place", with: @purchaser_bill.procurement_place_id
    fill_in "Purchase", with: @purchaser_bill.purchase_id
    fill_in "Total coconut", with: @purchaser_bill.total_coconut
    fill_in "User", with: @purchaser_bill.user_id
    fill_in "Vechicle", with: @purchaser_bill.vechicle_id
    click_on "Create Purchaser bill"

    assert_text "Purchaser bill was successfully created"
    click_on "Back"
  end

  test "should update Purchaser bill" do
    visit purchaser_bill_url(@purchaser_bill)
    click_on "Edit this purchaser bill", match: :first

    fill_in "Asal", with: @purchaser_bill.asal
    fill_in "Asal kai", with: @purchaser_bill.asal_kai
    fill_in "Asal vhurai", with: @purchaser_bill.asal_vhurai
    fill_in "Farmer", with: @purchaser_bill.farmer_id
    fill_in "Kai", with: @purchaser_bill.kai
    fill_in "Neer vattral kai", with: @purchaser_bill.neer_vattral_kai
    fill_in "Neer vattral vhurai", with: @purchaser_bill.neer_vattral_vhurai
    fill_in "Number of person", with: @purchaser_bill.number_of_person
    fill_in "Place", with: @purchaser_bill.place_id
    fill_in "Procurement date", with: @purchaser_bill.procurement_date
    fill_in "Procurement place", with: @purchaser_bill.procurement_place_id
    fill_in "Purchase", with: @purchaser_bill.purchase_id
    fill_in "Total coconut", with: @purchaser_bill.total_coconut
    fill_in "User", with: @purchaser_bill.user_id
    fill_in "Vechicle", with: @purchaser_bill.vechicle_id
    click_on "Update Purchaser bill"

    assert_text "Purchaser bill was successfully updated"
    click_on "Back"
  end

  test "should destroy Purchaser bill" do
    visit purchaser_bill_url(@purchaser_bill)
    click_on "Destroy this purchaser bill", match: :first

    assert_text "Purchaser bill was successfully destroyed"
  end
end
