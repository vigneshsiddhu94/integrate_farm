require "application_system_test_case"

class PendingWorksTest < ApplicationSystemTestCase
  setup do
    @pending_work = pending_works(:one)
  end

  test "visiting the index" do
    visit pending_works_url
    assert_selector "h1", text: "Pending works"
  end

  test "should create pending work" do
    visit pending_works_url
    click_on "New pending work"

    fill_in "Business", with: @pending_work.business_id
    fill_in "Date", with: @pending_work.date
    check "Status" if @pending_work.status
    fill_in "Type", with: @pending_work.type
    fill_in "User", with: @pending_work.user_id
    fill_in "Work", with: @pending_work.work
    click_on "Create Pending work"

    assert_text "Pending work was successfully created"
    click_on "Back"
  end

  test "should update Pending work" do
    visit pending_work_url(@pending_work)
    click_on "Edit this pending work", match: :first

    fill_in "Business", with: @pending_work.business_id
    fill_in "Date", with: @pending_work.date
    check "Status" if @pending_work.status
    fill_in "Type", with: @pending_work.type
    fill_in "User", with: @pending_work.user_id
    fill_in "Work", with: @pending_work.work
    click_on "Update Pending work"

    assert_text "Pending work was successfully updated"
    click_on "Back"
  end

  test "should destroy Pending work" do
    visit pending_work_url(@pending_work)
    click_on "Destroy this pending work", match: :first

    assert_text "Pending work was successfully destroyed"
  end
end
