require "application_system_test_case"

class CoconutExpensesTest < ApplicationSystemTestCase
  setup do
    @coconut_expense = coconut_expenses(:one)
  end

  test "visiting the index" do
    visit coconut_expenses_url
    assert_selector "h1", text: "Coconut expenses"
  end

  test "should create coconut expense" do
    visit coconut_expenses_url
    click_on "New coconut expense"

    fill_in "Date", with: @coconut_expense.date
    fill_in "Name", with: @coconut_expense.name
    fill_in "Reason", with: @coconut_expense.reason
    fill_in "Total amount", with: @coconut_expense.total_amount
    fill_in "User", with: @coconut_expense.user_id
    click_on "Create Coconut expense"

    assert_text "Coconut expense was successfully created"
    click_on "Back"
  end

  test "should update Coconut expense" do
    visit coconut_expense_url(@coconut_expense)
    click_on "Edit this coconut expense", match: :first

    fill_in "Date", with: @coconut_expense.date
    fill_in "Name", with: @coconut_expense.name
    fill_in "Reason", with: @coconut_expense.reason
    fill_in "Total amount", with: @coconut_expense.total_amount
    fill_in "User", with: @coconut_expense.user_id
    click_on "Update Coconut expense"

    assert_text "Coconut expense was successfully updated"
    click_on "Back"
  end

  test "should destroy Coconut expense" do
    visit coconut_expense_url(@coconut_expense)
    click_on "Destroy this coconut expense", match: :first

    assert_text "Coconut expense was successfully destroyed"
  end
end
