require "test_helper"

class CoconutInvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coconut_invoice = coconut_invoices(:one)
  end

  test "should get index" do
    get coconut_invoices_url
    assert_response :success
  end

  test "should get new" do
    get new_coconut_invoice_url
    assert_response :success
  end

  test "should create coconut_invoice" do
    assert_difference("CoconutInvoice.count") do
      post coconut_invoices_url, params: { coconut_invoice: { customer_id: @coconut_invoice.customer_id, date: @coconut_invoice.date, load_id: @coconut_invoice.load_id, lorry: @coconut_invoice.lorry, total_amount: @coconut_invoice.total_amount, user_id: @coconut_invoice.user_id } }
    end

    assert_redirected_to coconut_invoice_url(CoconutInvoice.last)
  end

  test "should show coconut_invoice" do
    get coconut_invoice_url(@coconut_invoice)
    assert_response :success
  end

  test "should get edit" do
    get edit_coconut_invoice_url(@coconut_invoice)
    assert_response :success
  end

  test "should update coconut_invoice" do
    patch coconut_invoice_url(@coconut_invoice), params: { coconut_invoice: { customer_id: @coconut_invoice.customer_id, date: @coconut_invoice.date, load_id: @coconut_invoice.load_id, lorry: @coconut_invoice.lorry, total_amount: @coconut_invoice.total_amount, user_id: @coconut_invoice.user_id } }
    assert_redirected_to coconut_invoice_url(@coconut_invoice)
  end

  test "should destroy coconut_invoice" do
    assert_difference("CoconutInvoice.count", -1) do
      delete coconut_invoice_url(@coconut_invoice)
    end

    assert_redirected_to coconut_invoices_url
  end
end
