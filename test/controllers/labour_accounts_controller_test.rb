require "test_helper"

class LabourAccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @labour_account = labour_accounts(:one)
  end

  test "should get index" do
    get labour_accounts_url
    assert_response :success
  end

  test "should get new" do
    get new_labour_account_url
    assert_response :success
  end

  test "should create labour_account" do
    assert_difference("LabourAccount.count") do
      post labour_accounts_url, params: { labour_account: { amount: @labour_account.amount, balance_amount: @labour_account.balance_amount, customer_id: @labour_account.customer_id, date: @labour_account.date, reason: @labour_account.reason } }
    end

    assert_redirected_to labour_account_url(LabourAccount.last)
  end

  test "should show labour_account" do
    get labour_account_url(@labour_account)
    assert_response :success
  end

  test "should get edit" do
    get edit_labour_account_url(@labour_account)
    assert_response :success
  end

  test "should update labour_account" do
    patch labour_account_url(@labour_account), params: { labour_account: { amount: @labour_account.amount, balance_amount: @labour_account.balance_amount, customer_id: @labour_account.customer_id, date: @labour_account.date, reason: @labour_account.reason } }
    assert_redirected_to labour_account_url(@labour_account)
  end

  test "should destroy labour_account" do
    assert_difference("LabourAccount.count", -1) do
      delete labour_account_url(@labour_account)
    end

    assert_redirected_to labour_accounts_url
  end
end
