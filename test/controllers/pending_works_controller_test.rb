require "test_helper"

class PendingWorksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pending_work = pending_works(:one)
  end

  test "should get index" do
    get pending_works_url
    assert_response :success
  end

  test "should get new" do
    get new_pending_work_url
    assert_response :success
  end

  test "should create pending_work" do
    assert_difference("PendingWork.count") do
      post pending_works_url, params: { pending_work: { business_id: @pending_work.business_id, date: @pending_work.date, status: @pending_work.status, type: @pending_work.type, user_id: @pending_work.user_id, work: @pending_work.work } }
    end

    assert_redirected_to pending_work_url(PendingWork.last)
  end

  test "should show pending_work" do
    get pending_work_url(@pending_work)
    assert_response :success
  end

  test "should get edit" do
    get edit_pending_work_url(@pending_work)
    assert_response :success
  end

  test "should update pending_work" do
    patch pending_work_url(@pending_work), params: { pending_work: { business_id: @pending_work.business_id, date: @pending_work.date, status: @pending_work.status, type: @pending_work.type, user_id: @pending_work.user_id, work: @pending_work.work } }
    assert_redirected_to pending_work_url(@pending_work)
  end

  test "should destroy pending_work" do
    assert_difference("PendingWork.count", -1) do
      delete pending_work_url(@pending_work)
    end

    assert_redirected_to pending_works_url
  end
end
