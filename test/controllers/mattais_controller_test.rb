require "test_helper"

class MattaisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mattai = mattais(:one)
  end

  test "should get index" do
    get mattais_url
    assert_response :success
  end

  test "should get new" do
    get new_mattai_url
    assert_response :success
  end

  test "should create mattai" do
    assert_difference("Mattai.count") do
      post mattais_url, params: { mattai: { date: @mattai.date, load: @mattai.load, rate: @mattai.rate, total_amount: @mattai.total_amount, user_id: @mattai.user_id } }
    end

    assert_redirected_to mattai_url(Mattai.last)
  end

  test "should show mattai" do
    get mattai_url(@mattai)
    assert_response :success
  end

  test "should get edit" do
    get edit_mattai_url(@mattai)
    assert_response :success
  end

  test "should update mattai" do
    patch mattai_url(@mattai), params: { mattai: { date: @mattai.date, load: @mattai.load, rate: @mattai.rate, total_amount: @mattai.total_amount, user_id: @mattai.user_id } }
    assert_redirected_to mattai_url(@mattai)
  end

  test "should destroy mattai" do
    assert_difference("Mattai.count", -1) do
      delete mattai_url(@mattai)
    end

    assert_redirected_to mattais_url
  end
end
