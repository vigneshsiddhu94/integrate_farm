require 'test_helper'

class FarmExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @farm_expense = farm_expenses(:one)
  end

  test "should get index" do
    get farm_expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_farm_expense_url
    assert_response :success
  end

  test "should create farm_expense" do
    assert_difference('FarmExpense.count') do
      post farm_expenses_url, params: { farm_expense: { amount: @farm_expense.amount, category: @farm_expense.category, date: @farm_expense.date } }
    end

    assert_redirected_to farm_expense_url(FarmExpense.last)
  end

  test "should show farm_expense" do
    get farm_expense_url(@farm_expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_farm_expense_url(@farm_expense)
    assert_response :success
  end

  test "should update farm_expense" do
    patch farm_expense_url(@farm_expense), params: { farm_expense: { amount: @farm_expense.amount, category: @farm_expense.category, date: @farm_expense.date } }
    assert_redirected_to farm_expense_url(@farm_expense)
  end

  test "should destroy farm_expense" do
    assert_difference('FarmExpense.count', -1) do
      delete farm_expense_url(@farm_expense)
    end

    assert_redirected_to farm_expenses_url
  end
end
