require "test_helper"

class FeedAvailabilitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @feed_availability = feed_availabilities(:one)
  end

  test "should get index" do
    get feed_availabilities_url
    assert_response :success
  end

  test "should get new" do
    get new_feed_availability_url
    assert_response :success
  end

  test "should create feed_availability" do
    assert_difference("FeedAvailability.count") do
      post feed_availabilities_url, params: { feed_availability: { business_id: @feed_availability.business_id, current_kg: @feed_availability.current_kg, date: @feed_availability.date, fodder_variety: @feed_availability.fodder_variety, rate: @feed_availability.rate, status: @feed_availability.status, stored_kg: @feed_availability.stored_kg, total_amount: @feed_availability.total_amount, user_id: @feed_availability.user_id } }
    end

    assert_redirected_to feed_availability_url(FeedAvailability.last)
  end

  test "should show feed_availability" do
    get feed_availability_url(@feed_availability)
    assert_response :success
  end

  test "should get edit" do
    get edit_feed_availability_url(@feed_availability)
    assert_response :success
  end

  test "should update feed_availability" do
    patch feed_availability_url(@feed_availability), params: { feed_availability: { business_id: @feed_availability.business_id, current_kg: @feed_availability.current_kg, date: @feed_availability.date, fodder_variety: @feed_availability.fodder_variety, rate: @feed_availability.rate, status: @feed_availability.status, stored_kg: @feed_availability.stored_kg, total_amount: @feed_availability.total_amount, user_id: @feed_availability.user_id } }
    assert_redirected_to feed_availability_url(@feed_availability)
  end

  test "should destroy feed_availability" do
    assert_difference("FeedAvailability.count", -1) do
      delete feed_availability_url(@feed_availability)
    end

    assert_redirected_to feed_availabilities_url
  end
end
