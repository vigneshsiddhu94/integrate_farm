require "test_helper"

class PurchaserBillsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @purchaser_bill = purchaser_bills(:one)
  end

  test "should get index" do
    get purchaser_bills_url
    assert_response :success
  end

  test "should get new" do
    get new_purchaser_bill_url
    assert_response :success
  end

  test "should create purchaser_bill" do
    assert_difference("PurchaserBill.count") do
      post purchaser_bills_url, params: { purchaser_bill: { asal: @purchaser_bill.asal, asal_kai: @purchaser_bill.asal_kai, asal_vhurai: @purchaser_bill.asal_vhurai, farmer_id: @purchaser_bill.farmer_id, kai: @purchaser_bill.kai, neer_vattral_kai: @purchaser_bill.neer_vattral_kai, neer_vattral_vhurai: @purchaser_bill.neer_vattral_vhurai, number_of_person: @purchaser_bill.number_of_person, place_id: @purchaser_bill.place_id, procurement_date: @purchaser_bill.procurement_date, procurement_place_id: @purchaser_bill.procurement_place_id, purchase_id: @purchaser_bill.purchase_id, total_coconut: @purchaser_bill.total_coconut, user_id: @purchaser_bill.user_id, vechicle_id: @purchaser_bill.vechicle_id } }
    end

    assert_redirected_to purchaser_bill_url(PurchaserBill.last)
  end

  test "should show purchaser_bill" do
    get purchaser_bill_url(@purchaser_bill)
    assert_response :success
  end

  test "should get edit" do
    get edit_purchaser_bill_url(@purchaser_bill)
    assert_response :success
  end

  test "should update purchaser_bill" do
    patch purchaser_bill_url(@purchaser_bill), params: { purchaser_bill: { asal: @purchaser_bill.asal, asal_kai: @purchaser_bill.asal_kai, asal_vhurai: @purchaser_bill.asal_vhurai, farmer_id: @purchaser_bill.farmer_id, kai: @purchaser_bill.kai, neer_vattral_kai: @purchaser_bill.neer_vattral_kai, neer_vattral_vhurai: @purchaser_bill.neer_vattral_vhurai, number_of_person: @purchaser_bill.number_of_person, place_id: @purchaser_bill.place_id, procurement_date: @purchaser_bill.procurement_date, procurement_place_id: @purchaser_bill.procurement_place_id, purchase_id: @purchaser_bill.purchase_id, total_coconut: @purchaser_bill.total_coconut, user_id: @purchaser_bill.user_id, vechicle_id: @purchaser_bill.vechicle_id } }
    assert_redirected_to purchaser_bill_url(@purchaser_bill)
  end

  test "should destroy purchaser_bill" do
    assert_difference("PurchaserBill.count", -1) do
      delete purchaser_bill_url(@purchaser_bill)
    end

    assert_redirected_to purchaser_bills_url
  end
end
