require "test_helper"

class SalesAndDeathsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sales_and_death = sales_and_deaths(:one)
  end

  test "should get index" do
    get sales_and_deaths_url
    assert_response :success
  end

  test "should get new" do
    get new_sales_and_death_url
    assert_response :success
  end

  test "should create sales_and_death" do
    assert_difference("SalesAndDeath.count") do
      post sales_and_deaths_url, params: { sales_and_death: { action_type: @sales_and_death.action_type, business_id: @sales_and_death.business_id, date: @sales_and_death.date, female_count: @sales_and_death.female_count, goat_type: @sales_and_death.goat_type, male_count: @sales_and_death.male_count, rate: @sales_and_death.rate, total_amount: @sales_and_death.total_amount, total_weight: @sales_and_death.total_weight, user_id: @sales_and_death.user_id } }
    end

    assert_redirected_to sales_and_death_url(SalesAndDeath.last)
  end

  test "should show sales_and_death" do
    get sales_and_death_url(@sales_and_death)
    assert_response :success
  end

  test "should get edit" do
    get edit_sales_and_death_url(@sales_and_death)
    assert_response :success
  end

  test "should update sales_and_death" do
    patch sales_and_death_url(@sales_and_death), params: { sales_and_death: { action_type: @sales_and_death.action_type, business_id: @sales_and_death.business_id, date: @sales_and_death.date, female_count: @sales_and_death.female_count, goat_type: @sales_and_death.goat_type, male_count: @sales_and_death.male_count, rate: @sales_and_death.rate, total_amount: @sales_and_death.total_amount, total_weight: @sales_and_death.total_weight, user_id: @sales_and_death.user_id } }
    assert_redirected_to sales_and_death_url(@sales_and_death)
  end

  test "should destroy sales_and_death" do
    assert_difference("SalesAndDeath.count", -1) do
      delete sales_and_death_url(@sales_and_death)
    end

    assert_redirected_to sales_and_deaths_url
  end
end
