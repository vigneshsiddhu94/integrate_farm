require "test_helper"

class FarmerAccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @farmer_account = farmer_accounts(:one)
  end

  test "should get index" do
    get farmer_accounts_url
    assert_response :success
  end

  test "should get new" do
    get new_farmer_account_url
    assert_response :success
  end

  test "should create farmer_account" do
    assert_difference("FarmerAccount.count") do
      post farmer_accounts_url, params: { farmer_account: { amount: @farmer_account.amount, balance_amount: @farmer_account.balance_amount, date: @farmer_account.date, farmer_id: @farmer_account.farmer_id, reason: @farmer_account.reason } }
    end

    assert_redirected_to farmer_account_url(FarmerAccount.last)
  end

  test "should show farmer_account" do
    get farmer_account_url(@farmer_account)
    assert_response :success
  end

  test "should get edit" do
    get edit_farmer_account_url(@farmer_account)
    assert_response :success
  end

  test "should update farmer_account" do
    patch farmer_account_url(@farmer_account), params: { farmer_account: { amount: @farmer_account.amount, balance_amount: @farmer_account.balance_amount, date: @farmer_account.date, farmer_id: @farmer_account.farmer_id, reason: @farmer_account.reason } }
    assert_redirected_to farmer_account_url(@farmer_account)
  end

  test "should destroy farmer_account" do
    assert_difference("FarmerAccount.count", -1) do
      delete farmer_account_url(@farmer_account)
    end

    assert_redirected_to farmer_accounts_url
  end
end
