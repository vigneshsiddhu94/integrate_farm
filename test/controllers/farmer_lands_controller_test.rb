require "test_helper"

class FarmerLandsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @farmer_land = farmer_lands(:one)
  end

  test "should get index" do
    get farmer_lands_url
    assert_response :success
  end

  test "should get new" do
    get new_farmer_land_url
    assert_response :success
  end

  test "should create farmer_land" do
    assert_difference("FarmerLand.count") do
      post farmer_lands_url, params: { farmer_land: { acre: @farmer_land.acre, farmer_id: @farmer_land.farmer_id, tree_count: @farmer_land.tree_count } }
    end

    assert_redirected_to farmer_land_url(FarmerLand.last)
  end

  test "should show farmer_land" do
    get farmer_land_url(@farmer_land)
    assert_response :success
  end

  test "should get edit" do
    get edit_farmer_land_url(@farmer_land)
    assert_response :success
  end

  test "should update farmer_land" do
    patch farmer_land_url(@farmer_land), params: { farmer_land: { acre: @farmer_land.acre, farmer_id: @farmer_land.farmer_id, tree_count: @farmer_land.tree_count } }
    assert_redirected_to farmer_land_url(@farmer_land)
  end

  test "should destroy farmer_land" do
    assert_difference("FarmerLand.count", -1) do
      delete farmer_land_url(@farmer_land)
    end

    assert_redirected_to farmer_lands_url
  end
end
