require "test_helper"

class KopparaisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @kopparai = kopparais(:one)
  end

  test "should get index" do
    get kopparais_url
    assert_response :success
  end

  test "should get new" do
    get new_kopparai_url
    assert_response :success
  end

  test "should create kopparai" do
    assert_difference("Kopparai.count") do
      post kopparais_url, params: { kopparai: { date: @kopparai.date, kg: @kopparai.kg, rate: @kopparai.rate, total_amount: @kopparai.total_amount, user_id: @kopparai.user_id } }
    end

    assert_redirected_to kopparai_url(Kopparai.last)
  end

  test "should show kopparai" do
    get kopparai_url(@kopparai)
    assert_response :success
  end

  test "should get edit" do
    get edit_kopparai_url(@kopparai)
    assert_response :success
  end

  test "should update kopparai" do
    patch kopparai_url(@kopparai), params: { kopparai: { date: @kopparai.date, kg: @kopparai.kg, rate: @kopparai.rate, total_amount: @kopparai.total_amount, user_id: @kopparai.user_id } }
    assert_redirected_to kopparai_url(@kopparai)
  end

  test "should destroy kopparai" do
    assert_difference("Kopparai.count", -1) do
      delete kopparai_url(@kopparai)
    end

    assert_redirected_to kopparais_url
  end
end
