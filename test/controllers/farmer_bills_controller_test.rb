require "test_helper"

class FarmerBillsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @farmer_bill = farmer_bills(:one)
  end

  test "should get index" do
    get farmer_bills_url
    assert_response :success
  end

  test "should get new" do
    get new_farmer_bill_url
    assert_response :success
  end

  test "should create farmer_bill" do
    assert_difference("FarmerBill.count") do
      post farmer_bills_url, params: { farmer_bill: { asal_kai_count: @farmer_bill.asal_kai_count, asal_kai_rate: @farmer_bill.asal_kai_rate, farmer_id: @farmer_bill.farmer_id, neer_vattral__kai_count: @farmer_bill.neer_vattral__kai_count, neer_vattral_kai_rate: @farmer_bill.neer_vattral_kai_rate, procurement_date: @farmer_bill.procurement_date, total_amount: @farmer_bill.total_amount, user_id: @farmer_bill.user_id } }
    end

    assert_redirected_to farmer_bill_url(FarmerBill.last)
  end

  test "should show farmer_bill" do
    get farmer_bill_url(@farmer_bill)
    assert_response :success
  end

  test "should get edit" do
    get edit_farmer_bill_url(@farmer_bill)
    assert_response :success
  end

  test "should update farmer_bill" do
    patch farmer_bill_url(@farmer_bill), params: { farmer_bill: { asal_kai_count: @farmer_bill.asal_kai_count, asal_kai_rate: @farmer_bill.asal_kai_rate, farmer_id: @farmer_bill.farmer_id, neer_vattral__kai_count: @farmer_bill.neer_vattral__kai_count, neer_vattral_kai_rate: @farmer_bill.neer_vattral_kai_rate, procurement_date: @farmer_bill.procurement_date, total_amount: @farmer_bill.total_amount, user_id: @farmer_bill.user_id } }
    assert_redirected_to farmer_bill_url(@farmer_bill)
  end

  test "should destroy farmer_bill" do
    assert_difference("FarmerBill.count", -1) do
      delete farmer_bill_url(@farmer_bill)
    end

    assert_redirected_to farmer_bills_url
  end
end
