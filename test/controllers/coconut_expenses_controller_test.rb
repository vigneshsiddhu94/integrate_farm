require "test_helper"

class CoconutExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @coconut_expense = coconut_expenses(:one)
  end

  test "should get index" do
    get coconut_expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_coconut_expense_url
    assert_response :success
  end

  test "should create coconut_expense" do
    assert_difference("CoconutExpense.count") do
      post coconut_expenses_url, params: { coconut_expense: { date: @coconut_expense.date, name: @coconut_expense.name, reason: @coconut_expense.reason, total_amount: @coconut_expense.total_amount, user_id: @coconut_expense.user_id } }
    end

    assert_redirected_to coconut_expense_url(CoconutExpense.last)
  end

  test "should show coconut_expense" do
    get coconut_expense_url(@coconut_expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_coconut_expense_url(@coconut_expense)
    assert_response :success
  end

  test "should update coconut_expense" do
    patch coconut_expense_url(@coconut_expense), params: { coconut_expense: { date: @coconut_expense.date, name: @coconut_expense.name, reason: @coconut_expense.reason, total_amount: @coconut_expense.total_amount, user_id: @coconut_expense.user_id } }
    assert_redirected_to coconut_expense_url(@coconut_expense)
  end

  test "should destroy coconut_expense" do
    assert_difference("CoconutExpense.count", -1) do
      delete coconut_expense_url(@coconut_expense)
    end

    assert_redirected_to coconut_expenses_url
  end
end
