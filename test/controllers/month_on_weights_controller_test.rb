require "test_helper"

class MonthOnWeightsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @month_on_weight = month_on_weights(:one)
  end

  test "should get index" do
    get month_on_weights_url
    assert_response :success
  end

  test "should get new" do
    get new_month_on_weight_url
    assert_response :success
  end

  test "should create month_on_weight" do
    assert_difference("MonthOnWeight.count") do
      post month_on_weights_url, params: { month_on_weight: { business_id: @month_on_weight.business_id, date: @month_on_weight.date, total_female_count: @month_on_weight.total_female_count, total_male_count: @month_on_weight.total_male_count, total_weight: @month_on_weight.total_weight, user_id: @month_on_weight.user_id } }
    end

    assert_redirected_to month_on_weight_url(MonthOnWeight.last)
  end

  test "should show month_on_weight" do
    get month_on_weight_url(@month_on_weight)
    assert_response :success
  end

  test "should get edit" do
    get edit_month_on_weight_url(@month_on_weight)
    assert_response :success
  end

  test "should update month_on_weight" do
    patch month_on_weight_url(@month_on_weight), params: { month_on_weight: { business_id: @month_on_weight.business_id, date: @month_on_weight.date, total_female_count: @month_on_weight.total_female_count, total_male_count: @month_on_weight.total_male_count, total_weight: @month_on_weight.total_weight, user_id: @month_on_weight.user_id } }
    assert_redirected_to month_on_weight_url(@month_on_weight)
  end

  test "should destroy month_on_weight" do
    assert_difference("MonthOnWeight.count", -1) do
      delete month_on_weight_url(@month_on_weight)
    end

    assert_redirected_to month_on_weights_url
  end
end
