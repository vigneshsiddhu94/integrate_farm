Rails.application.routes.draw do
  resources :labour_accounts
  resources :customer_accounts
  resources :farmer_accounts
  resources :sales_and_deaths
  resources :month_on_weights
  resources :feed_availabilities
  resources :pending_works
  resources :loads
  resources :coconut_invoices
  resources :coconut_expenses
  resources :customers
  resources :vechicles
  resources :mattais
  resources :kopparais
  resources :salaries
  resources :farmer_bills
  resources :purchaser_bills
  resources :purchasers
  resources :farmer_lands
  resources :farmers
  resources :labours
  resources :places
	
 
    devise_for :users
    default_url_options :host => "goatfarm.com"

    resources :farm_expenses
	#Routes For Cow
	root to: 'home#main_page'
	resources :cows, only: [:new, :create, :edit, :update, :index]
	resources :calves, only: [:new, :create, :edit, :update, :index]
	resources :feed_rates, only: [:new, :create, :edit, :update]
	resources :feeds, only: [:new, :create, :edit, :update, :index, :destroy]
	resources :milk_productions, only: [:index, :new, :create, :edit, :update]
	# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
	get 'home' => 'home#main_page'
	get 'cow_health' => 'cows#cow_health'
	post 'cow_health' => 'cows#create_health'
	get 'history' => 'milk_productions#history_cow', as: 'history'
	get 'milk_account' => 'milk_productions#today'
	get 'health' => 'cows#health'
	post 'date_account' => 'milk_productions#today'

	#Routes For Goat
	resources :goats, only: [:new, :create, :edit, :update, :index, :destroy ]
	resources :diseases, only: [:new, :create, :edit, :update, :index, :destroy]
	resources :mate_goats, only: [:new, :create, :edit, :update, :index, :destroy ]
	resources :parent_goats, only: [:new, :create, :edit, :update, :index, :destroy ]
	get 'goat_status' => 'goats#goat_status'
	post 'goat_status' => 'goats#create_status'
	get 'goat_insurance' => 'goats#goat_insurance'
	post 'goat_insurance' => 'goats#create_insurance'
	get 'farm_status' => 'goats#farm_status'
	get 'find_goat' => 'goats#find_goat'
	get 'goat_diseases' => 'diseases#goat_diseases'
	get 'goat_disease' => 'diseases#goat_disease'
	post 'goat_disease' => 'diseases#create_goat_disease'
	get 'goat_disease/:id/edit', to: 'diseases#edit_goat_disease', as: 'edit_goat_disease'
	post 'update_goat_disease' => 'diseases#update_goat_disease'
	get 'goat_death' => 'diseases#goat_death'
	post 'goat_death' => 'diseases#create_goat_death'
	resources :main_works, only: [:index, :new, :create, :edit, :update, :destroy]
	resources :daily_works, only: [:index, :new, :create, :edit, :update, :destroy]
	get 'good_morning' => 'daily_works#good_morning'
	post 'date_work' => 'daily_works#date_work'
	get 'date_work' => 'daily_works#date_work'
    get 'invoice_year' => 'invoices#invoice_year'
	resources :invoices, only: [:new, :create, :edit, :update, :index, :show ] 
	resources :rack_details, only: [:new, :create, :edit, :update, :index, :destroy ]
	resources :batches, only: [:new, :create, :edit, :update, :index, :destroy ]
	resources :goat_weights, only: [:new, :create, :edit, :update, :index, :destroy ]
	get  'batch_weight' => 'goat_weights#batch_weight'
	post  'create_batch_weight' => 'goat_weights#create_batch_weight'
	post  'year_income' => 'home#year_income'
	resources :lands, only: [:new, :create, :edit, :update, :index, :show ] 
	resources :crops, only: [:new, :create, :edit, :update, :index, :show ] 
	resources :waters, only: [:new, :create, :edit, :update, :index, :show ] 
	resources :fertilizers, only: [:new, :create, :edit, :update, :index, :show ] 
	get  'land_water' => 'waters#land_water'
	post 'land_water_create' => 'waters#land_water_create'
	get  'land_crop' => 'crops#land_crop'
	post 'land_crop_create' => 'crops#land_crop_create'	
	get 'fodder_management' => 'lands#fodder_management'	
	resources :dry_fodders, only: [:new, :create, :edit, :update, :index, :show ] 
	resources :crop_activities, only: [:new, :create, :edit, :update, :index, :show ] 
	get 'goat_data' => 'goats#get_goat_data'
	get 'get_hole_goat_data' => 'goats#get_hole_goat_data'
	get 'goat_kid_data' => 'goats#goat_kid_data'
	get 'kid_weight_data' => 'goats#kid_weight_data'
	get 'goat_data_batch_wise' => 'batches#goat_data_batch_wise'
	get 'goat_pending_work' => 'pending_works#goat_pending_work'
	get 'daily_pending_work' => 'pending_works#daily_pending_work'

	# Coconut business routes start
	resources :places
	resources :labours
	resources :farmers
	resources :farmer_lands
	resources :vechicles
	resources :purchasers
	resources :purchaser_bills
	resources :farmer_bills
	resources :salarys
	resources :loads
	resources :coconut_invoices
	resources :kopparais
	resources :mattais
	resources :expensess
	get 'find_farmer' => 'farmers#find_farmer'
	post 'tally_farmer' => 'farmers#tally_farmer'
	post 'tally_customer' => 'farmers#tally_customer'
	post 'tally_labour' => 'farmers#tally_labour'
	post 'update_pay' => 'farmers#update_pay'
	
	# Coconut business routes ends
	
	# pdf generation
	get 'download_form' => 'milk_productions#download_form'
	# post 'download_data' => 'milk_productions#download_data'
	post 'download_data', to: 'milk_productions#download_data', as: 'download_data'
	get 'today_farm_update' => 'milk_productions#today_farm_update'
	post 'create_today_farm_status' => 'milk_productions#create_today_farm_status'
	get 'today_work', to: 'milk_productions#generate_pdf', as: 'generate_pdf'
	resources :feed_consumptions
	
end
