json.extract! farmer, :id, :name, :is_activate, :mobile_number, :user_id, :place_id, :created_at, :updated_at
json.url farmer_url(farmer, format: :json)
