json.extract! coconut_expense, :id, :reason, :name, :date, :total_amount, :user_id, :created_at, :updated_at
json.url coconut_expense_url(coconut_expense, format: :json)
