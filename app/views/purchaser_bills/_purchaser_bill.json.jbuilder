json.extract! purchaser_bill, :id, :asal_vhurai, :asal_kai, :neer_vattral_vhurai, :neer_vattral_kai, :number_of_person, :asal, :kai, :total_coconut, :procurement_date, :user_id, :purchase_id, :place_id, :vechicle_id, :procurement_place_id, :farmer_id, :created_at, :updated_at
json.url purchaser_bill_url(purchaser_bill, format: :json)
