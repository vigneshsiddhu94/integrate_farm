json.set! :data do
  json.array! @feed_availabilities do |feed_availability|
    json.partial! 'feed_availabilities/feed_availability', feed_availability: feed_availability
    json.url  "
              #{link_to 'Show', feed_availability }
              #{link_to 'Edit', edit_feed_availability_path(feed_availability)}
              #{link_to 'Destroy', feed_availability, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end