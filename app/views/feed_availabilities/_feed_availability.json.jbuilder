json.extract! feed_availability, :id, :date, :fodder_variety, :stored_kg, :rate, :total_amount, :current_kg, :business_id, :user_id, :status, :created_at, :updated_at
json.url feed_availability_url(feed_availability, format: :json)
