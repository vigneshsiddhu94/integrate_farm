json.extract! purchaser, :id, :name, :is_activate, :mobile_number, :user_id, :place_id, :created_at, :updated_at
json.url purchaser_url(purchaser, format: :json)
