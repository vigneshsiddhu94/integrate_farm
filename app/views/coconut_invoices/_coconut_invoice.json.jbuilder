json.extract! coconut_invoice, :id, :lorry, :total_amount, :date, :user_id, :load_id, :customer_id, :created_at, :updated_at
json.url coconut_invoice_url(coconut_invoice, format: :json)
