json.extract! labour, :id, :name, :is_activate, :mobile_number, :user_id, :place_id, :created_at, :updated_at
json.url labour_url(labour, format: :json)
