json.set! :data do
  json.array! @farmer_accounts do |farmer_account|
    json.partial! 'farmer_accounts/farmer_account', farmer_account: farmer_account
    json.url  "
              #{link_to 'Show', farmer_account }
              #{link_to 'Edit', edit_farmer_account_path(farmer_account)}
              #{link_to 'Destroy', farmer_account, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end