json.extract! farmer_account, :id, :amount, :reason, :date, :farmer_id, :balance_amount, :created_at, :updated_at
json.url farmer_account_url(farmer_account, format: :json)
