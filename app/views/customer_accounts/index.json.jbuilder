json.set! :data do
  json.array! @customer_accounts do |customer_account|
    json.partial! 'customer_accounts/customer_account', customer_account: customer_account
    json.url  "
              #{link_to 'Show', customer_account }
              #{link_to 'Edit', edit_customer_account_path(customer_account)}
              #{link_to 'Destroy', customer_account, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end