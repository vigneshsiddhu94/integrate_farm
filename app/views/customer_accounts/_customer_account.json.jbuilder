json.extract! customer_account, :id, :amount, :reason, :date, :customer_id, :balance_amount, :created_at, :updated_at
json.url customer_account_url(customer_account, format: :json)
