json.extract! farmer_bill, :id, :asal_kai_count, :asal_kai_rate, :neer_vattral__kai_count, :neer_vattral_kai_rate, :total_amount, :procurement_date, :user_id, :farmer_id, :created_at, :updated_at
json.url farmer_bill_url(farmer_bill, format: :json)
