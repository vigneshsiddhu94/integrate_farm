json.extract! month_on_weight, :id, :total_male_count, :total_female_count, :total_weight, :date, :business_id, :user_id, :created_at, :updated_at
json.url month_on_weight_url(month_on_weight, format: :json)
