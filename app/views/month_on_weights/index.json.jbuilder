json.set! :data do
  json.array! @month_on_weights do |month_on_weight|
    json.partial! 'month_on_weights/month_on_weight', month_on_weight: month_on_weight
    json.url  "
              #{link_to 'Show', month_on_weight }
              #{link_to 'Edit', edit_month_on_weight_path(month_on_weight)}
              #{link_to 'Destroy', month_on_weight, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end