json.set! :data do
  json.array! @loads do |load|
    json.partial! 'loads/load', load: load
    json.url  "
              #{link_to 'Show', load }
              #{link_to 'Edit', edit_load_path(load)}
              #{link_to 'Destroy', load, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end