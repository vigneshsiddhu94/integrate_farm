json.extract! kopparai, :id, :kg, :rate, :date, :total_amount, :user_id, :created_at, :updated_at
json.url kopparai_url(kopparai, format: :json)
