json.extract! salary, :id, :knr_count, :pattai_count, :salary, :date, :user_id, :labour_id, :created_at, :updated_at
json.url salary_url(salary, format: :json)
