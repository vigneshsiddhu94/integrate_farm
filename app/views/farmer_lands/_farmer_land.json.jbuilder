json.extract! farmer_land, :id, :acre, :tree_count, :farmer_id, :created_at, :updated_at
json.url farmer_land_url(farmer_land, format: :json)
