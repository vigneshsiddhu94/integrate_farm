json.set! :data do
  json.array! @farmer_lands do |farmer_land|
    json.partial! 'farmer_lands/farmer_land', farmer_land: farmer_land
    json.url  "
              #{link_to 'Show', farmer_land }
              #{link_to 'Edit', edit_farmer_land_path(farmer_land)}
              #{link_to 'Destroy', farmer_land, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end