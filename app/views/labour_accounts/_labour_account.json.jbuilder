json.extract! labour_account, :id, :amount, :reason, :date, :customer_id, :balance_amount, :created_at, :updated_at
json.url labour_account_url(labour_account, format: :json)
