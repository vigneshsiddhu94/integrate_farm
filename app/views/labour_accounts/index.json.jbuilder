json.set! :data do
  json.array! @labour_accounts do |labour_account|
    json.partial! 'labour_accounts/labour_account', labour_account: labour_account
    json.url  "
              #{link_to 'Show', labour_account }
              #{link_to 'Edit', edit_labour_account_path(labour_account)}
              #{link_to 'Destroy', labour_account, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end