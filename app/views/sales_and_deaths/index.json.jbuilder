json.set! :data do
  json.array! @sales_and_deaths do |sales_and_death|
    json.partial! 'sales_and_deaths/sales_and_death', sales_and_death: sales_and_death
    json.url  "
              #{link_to 'Show', sales_and_death }
              #{link_to 'Edit', edit_sales_and_death_path(sales_and_death)}
              #{link_to 'Destroy', sales_and_death, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end