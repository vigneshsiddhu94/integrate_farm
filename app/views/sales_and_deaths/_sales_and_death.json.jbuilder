json.extract! sales_and_death, :id, :male_count, :action_type, :goat_type, :female_count, :date, :business_id, :user_id, :rate, :total_weight, :total_amount, :created_at, :updated_at
json.url sales_and_death_url(sales_and_death, format: :json)
