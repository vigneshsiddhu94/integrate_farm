class Business < ApplicationRecord
  belongs_to :user
  has_many :feed_consumptions
  has_many :user_employees, inverse_of: :business
  has_many :milk_productions, inverse_of: :business
  has_many :cows, inverse_of: :businesses
  has_many :lands, inverse_of: :business
  has_many :land_crops, inverse_of: :business
  has_many :waters, inverse_of: :business
  has_many :land_waters, inverse_of: :business
  has_many :pending_works, inverse_of: :business
  has_many :farm_expenses, inverse_of: :business
  has_many :feeds, inverse_of: :business
  has_many :feed_availabilities, inverse_of: :business

end
