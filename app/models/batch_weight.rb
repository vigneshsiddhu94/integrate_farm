class BatchWeight < ApplicationRecord
  belongs_to :business
  belongs_to :user
  belongs_to :rack_detail
  belongs_to :month_on_weight

  enum kid_stage: {'0-1 Month': 1, '1-2 Month': 2, '2-3 Month': 3, '3-4 Month': 4, '4-5 Month': 5, '5-6 Month': 6, '6-7 Month': 7, '7-8 Month': 8, '8-9 Month': 9, '9-10 Month': 10, '10-11 Month': 11, '11-12 Month':12, '12-13 Month': 13, '13-14 Month': 14, '14-15 month': 15}
end
