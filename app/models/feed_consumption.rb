class FeedConsumption < ApplicationRecord
    validates :date, presence: true
    belongs_to :business
    validates :concentrate_feed, :thippi, :asola, :dry_fodder, :green_fodder, :agathi_mulberry, :silage, :wastage_green, :wastage_dry, numericality: { greater_than_or_equal_to: 0 }


    def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new	
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
	
		# __________________________#Land Data _____________________________________________
		@land = Land.where(business_id: business_id).all.order('name ASC')
		land_data = [["Name", "Crop", "Plant Date", "Water Days"]]
		@land.each do |item|
		land_data << [
			item.name,
			(item.land_crops.last.crop.name if item.land_crops.present?),
			((Date.today - item.land_crops.last.date).to_i if item.land_crops.present?),
			((Date.today - item.land_waters.last.date).to_i if item.land_waters.present?)
		]
		end
		pdf.move_down(20)
		pdf.text "Land and Water Detail", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(land_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
    
    # __________________________#Feed Data _____________________________________________
    parsed_date = Date.parse(date)
    @feed_consumption = FeedConsumption.where(
                  business_id: business_id, 
                  date: parsed_date.beginning_of_month..parsed_date.end_of_month
                ).order('date DESC')
    # Table Header
    feed_consumption_data = [
    ["Date", "Concentrate Feed", "Thippi", "Asola", "Dry Fodder", "Green Fodder",
    "Agathi/Mulberry", "Silage", "Wastage Green", "Wastage Dry", "Cutting Land"]
    ]
    # Table Data
    @feed_consumption.each do |item|
    feed_consumption_data << [
    item.date,
    item.concentrate_feed,
    item.thippi,
    item.asola,
    item.dry_fodder,
    item.green_fodder,
    item.agathi_mulberry,
    item.silage,
    item.wastage_green,
    item.wastage_dry,
    item.cutting_land
    ]
    end
    # PDF Formatting
    pdf.move_down(20)
    pdf.text "Feed Consumption", align: :left, style: :bold, color: "008000"
    pdf.move_down(5)
    # Generate Table
    pdf.table(feed_consumption_data, header: true, row_colors: ["DDDDDD", "FFFFFF"],
    cell_style: { border_color: '000000', border_style: :solid, border_width: 1,
    size: 10, padding: 5 }) do
    row(0).font_style = :bold
    columns(1..3).align = :center
    columns(5..7).align = :right
    self.header = true
    end

    # __________________________#Goat Feed Prediction _____________________________________________
		
    # @male_thala = Goat.male_thalachery(current_user)
    # @female_thala = Goat.female_thalachery(current_user)
    # @boyer = Goat.male_boyer(current_user)
    # @female_pregnancy =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Confirmed', 'Calve Stage'], live:'Yes').count
		# @early_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: 'Pregnancy', live:'Yes').count
		# @female_non_pregnancy =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Heat', 'Non Pregnancy', 'Abort'], live:'Yes').count
		# @milking_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Milking'], live:'Yes').count
		# @doubt_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Doubt'], live:'Yes').count
    # @kid_female = Goat.kid_female_thalachery(current_user)
    # @kid_male = Goat.kid_male_thalachery(current_user)
		# @kid = (@kid_female.count) + (@kid_male.count)
		# @matured_male_kid = Goat.matured_male_kid(current_user).count
		# @matured_female_kid = Goat.matured_female_kid(current_user).count

    # goat_silage = 1.5
    # goat_green = 1
    # goat_dryfodder = 1  



	  # feed_data = [["Goat","Concentrate Feed","Thippi","Asola","Green Fodder", "Dry Fodder", "Silage", "agathi/mulberry"]]
		# @land.each do |item|
		# land_data << [
		# 	item.name,
		# 	(item.land_crops.last.crop.name if item.land_crops.present?),
		# 	((Date.today - item.land_crops.last.date).to_i if item.land_crops.present?),
		# 	((Date.today - item.land_waters.last.date).to_i if item.land_waters.present?)
		# ]
		# end
		# pdf.move_down(20)
		# pdf.text "Feed Detail", align: :left, style: :bold, color: "008000"
		# pdf.move_down(5)
		# pdf.table(land_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
		# 	row(0).font_style = :bold
		# 	columns(1..3).align = :center
		# 	columns(5..7).align = :right
		# 	self.header = true
		# end

		pdf.render
    end
  end
  