class GoatWeight < ApplicationRecord
  
  belongs_to :goat, inverse_of: :goat_weights

  validates :weight, presence: true, numericality: { only_integer: true }
  validates_presence_of :date

  scope :goat_weight_all, lambda { |user|
    joins(:goat).where("user_id =? and live = ?",user.id, 1).order('token_no ASC') }


  def self.batch_weight_create(batch_id,weight,date)
  	goats = BatchGoat.joins(:batch).where(batch_id: batch_id).pluck(:goat_id)
  	if goats.empty?
  	  return goats
  	else 
  	  individual_weight =  (weight.to_i / goats.count)  
  	  goats.each do |goat|
  	    GoatWeight.create(goat_id: goat, weight: individual_weight, date: date)
  	  end
  	end
  end

end
