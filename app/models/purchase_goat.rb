class PurchaseGoat < ApplicationRecord
  	
  	belongs_to :goat, inverse_of: :purchase_goat, dependent: :destroy
	
	validates_presence_of :goat
	validates_presence_of :date_of_purchase
	validates_presence_of :rate
	validates_presence_of :place

	scope :in_year, -> (year) { where('extract(year from date_of_purchase) = ?', year) }

	# scope :male_thalachery, lambda { |user| joins(:goat).where(goats: { breed_variety: 1, gender: 1, user_id: user.id} ) }
	# scope :female_thalachery, lambda { |user| joins(:goat).where(goats: { breed_variety: 1, gender: 2, user_id: user.id} ) }
	# scope :thalachery, lambda { |user| joins(:goat).where(goats: { breed_variety: 1, gnder:[1,2], user_id: user.id} ) }
	# scope :male_boyer, lambda { |user| joins(:goat).where(goats: { breed_variety: 2, gender: 1, user_id: user.id} ) }

	
	# scope :male_boyer, -> { joins(:goat).where(goats: { breed_variety: 'Male Boyer'} ) }


	def self.total_investment(year = Date.today.year)
		thalacerry_investment + boyer_investment
	end

	def self.thalacerry_investment(year = Date.today.year)
		PurchaseGoat.in_year(year.to_s).thalachery.map(&:purchase_rate).sum
	end

	def self.boyer_investment(year = Date.today.year)
		PurchaseGoat.in_year(year.to_s).male_boyer.map(&:purchase_rate).sum
	end
end
