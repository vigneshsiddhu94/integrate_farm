class FeedAvailability < ApplicationRecord
  belongs_to :business
  belongs_to :user

  enum fodder_variety: {'Cow Concentrate Feed': 1, 'Goat Concentrate Feed': 2, 'Silage': 3, 'Solam': 4, 'Coolam': 5, 'Kadalai Kodi': 6}
  validate :same_stock_present
  # validates :current_kg, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true

  before_update :update_current_stock


  def same_stock_present
   if !stored_kg_changed? && !rate_changed? && !status_changed? && !total_amount_changed? && !date_changed? && !current_kg_changed? && FeedAvailability.exists?(fodder_variety: self.fodder_variety, status: true)
      errors.add(:fodder_variety, 'already exists for this month')
      return false
    end
    return true
  end
    

  def update_current_stock
    feed_availability = FeedAvailability.find_by(id: self.id, status: true)
    if feed_availability.stored_kg != self.stored_kg 
      current_stored_kg = self.stored_kg - feed_availability.stored_kg 
      self.current_kg = self.current_kg + current_stored_kg
    end
  end
end
