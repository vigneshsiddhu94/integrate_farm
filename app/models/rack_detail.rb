class RackDetail < ApplicationRecord
	
	belongs_to :user, inverse_of: :rack_details	
	has_many :batches, inverse_of: :rack_detail
    has_many :batch_weights, inverse_of: :rack_detail

    validates_presence_of   :name
    validates  :capacity, presence: true, numericality: { only_integer: true }

    scope :rack_details_all, lambda { |user| where(user_id: user.id) }

end
