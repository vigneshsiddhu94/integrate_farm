class FarmerBill < ApplicationRecord
  belongs_to :user
  belongs_to :farmer

  after_create :top_up_farmer_account

  private

  def top_up_farmer_account
    FarmerAccount.top_up_for(self.farmer_id, self.total_amount, self.procurement_date)
  end
end
