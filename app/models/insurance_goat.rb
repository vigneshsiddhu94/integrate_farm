class InsuranceGoat < ApplicationRecord
	belongs_to :goat, inverse_of: :insurance_goats

	validates_presence_of :policy_no
	validates_presence_of :start_date
	validates_presence_of :exp_date
	validates_presence_of :amount

end
