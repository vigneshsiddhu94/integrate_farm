class PendingWork < ApplicationRecord
  belongs_to :user, inverse_of: :pending_works
  belongs_to :business, inverse_of: :pending_works

  enum work_type: { 'Immediate': 1, 'Daily': 2, 'Goat': 3}
end
