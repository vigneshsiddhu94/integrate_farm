class Kopparai < ApplicationRecord
  belongs_to :user

  enum quality: {'Big': 1, 'Medium': 2, 'Small': 3 }
end
