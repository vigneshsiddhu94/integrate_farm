class Fertilizer < ApplicationRecord
	# has_many :fertilizer_activities, inverse_of: :fertilizer

	validates_presence_of :name
	validates_presence_of :details
end
