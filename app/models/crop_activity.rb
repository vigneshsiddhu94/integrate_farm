class CropActivity < ApplicationRecord
	# has_many :fertilizer_activities, inverse_of: :crop_activity
	belongs_to :land_crop, inverse_of: :crop_activities

	validates_presence_of :date
	validates_presence_of :activity
	validates_presence_of :comment	
	validates_presence_of :end_date


	enum activity: {'Cutting':1, 'Fertilizer':2, 'Weed':3}
	enum status: {'Yes':1, 'No':2}

	scope :crop_activity_live, -> { where(status: 1).order('created_at DESC') }
	scope :crop_activity_not_live, -> { where(status: 2).order('created_at DESC') }
	scope :crop_cutting_live, -> (land_crops_id) { where(land_crop_id: land_crops_id, activity: 1, status:1).order('created_at Asc') }
	scope :crop_cutting_not_live, -> (land_crops_id) { where(land_crop_id: land_crops_id, activity: 1, status: 2).order('created_at Asc') }

end
