class DryFodder < ApplicationRecord

	validates_presence_of	:purchase_date
	validates_presence_of	:close_date
	validates_presence_of	:amount
	validates_presence_of	:qty
	validates_presence_of	:land_qty
	validates_presence_of	:variety
	validates_presence_of	:status
	validates_presence_of	:comment

	enum status: { 'Yes': 1, 'No': 2}
	enum variety: { 'Solam': 1, 'Groundnut': 2, 'Cowpea': 3, 'Pigeonpea': 4, 'Sorghum': 5, 'Alfaalfa':6}
	scope :dry_fodder_live, -> { where(status: 1).order('created_at ASC') }
	scope :dry_fodder_no_live, -> { where(status: 2).order('created_at ASC') }
	scope :dry_fodder, -> (variety) { where(variety: variety, status: 1).order('created_at ASC') }

	def self.fodder_availability(variety = fodder)
		qty = 0
		DryFodder.where(variety: variety, status: 1).each do |dry_fodder|
			qty += dry_fodder.qty
		end 
		return qty 
	end

end