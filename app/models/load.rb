class Load < ApplicationRecord
  belongs_to :coconut_invoice
  
  enum quality: {'KNR': 1, '4Pattai': 2, 'Balras': 3 }
  enum paruthi: {'80': 1, '100': 2, '120': 3, '200': 4, '250': 5}
end
