class Land < ApplicationRecord
	has_many :land_crops, inverse_of: :land
	has_many :land_waters, inverse_of: :land
	belongs_to :business, inverse_of: :lands


	validates_presence_of :name
	validates_presence_of :cent

	enum soil: {'Black Cotton': 1, 'Red': 2}
end
