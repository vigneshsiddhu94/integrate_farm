class Goat < ApplicationRecord
	
	belongs_to :user, inverse_of: :goats
	has_many :goat_calving_periods, inverse_of: :goat
	has_many :insurance_goats, inverse_of: :goat
	has_many :female_mat_goats, :class_name => 'MateGoat', :foreign_key => 'female_id'
	has_many :male_mat_goats, :class_name => 'MateGoat', :foreign_key => 'male_id'
	has_many :children_parent_goats, :class_name => 'ParentGoat', :foreign_key => 'parent_id'
	has_many :disease_goats, inverse_of: :goat
	has_many :daily_works, inverse_of: :goat
	has_one :death_goat, inverse_of: :goat
	has_one :purchase_goat, inverse_of: :goat
  	has_one :parent_goat, :class_name => 'ParentGoat', :foreign_key => 'children_id'
	has_one :goat_sale, inverse_of: :goat
	has_one :batch_goat, inverse_of: :goat
	has_many :goat_weights, inverse_of: :goat

	validates :token_no, presence: true, uniqueness: { scope: :user_id }
	validates :breed_variety, presence: true
	validates :weight, presence: true, numericality: { only_float: true }
	validates :date_of_birth, presence: true
	validates :age, presence: true, numericality: { only_integer: true }
	validates :live, presence: true
	

  	# accepts_nested_attributes_for :purchase_goat, :allow_destroy => true
  	# accepts_nested_attributes_for :goat_calving_periods, reject_if: proc { |attributes| attributes['goat_status'].blank? }
  	# accepts_nested_attributes_for :rack_goats 
	enum gender: {'Male': 1, 'Female': 2, 'Kid Male':3, 'Kid Female':4}
	enum breed_variety: { 'Thalachery': 1, 'Boyer': 2, 'Semmari': 3, 'Vella': 4, 'Kodi': 5, 'Kanni': 6, 'Mecheri': 7, 'Jamunapari': 8, 'Beetal': 9, 'Sirohi': 10, 'Osmanabadi': 11}
	# enum breed_variety: { 'Male Thalachery': 1, 'Female Thalachery': 2, 'Male Boyer': 3, 'Female Semmari': 4, 'Male Vella': 5, 'Female Vella': 6, 'Male Semmari': 7, 'Female Boyer': 8, 'Kid Male Thalachery': 9, 'Kid Female Thalachery': 10, 'Kid Female Semmari': 11, 'Kid Male Vella': 12, 'Kid Female Vella': 13, 'Kid Male Semmari': 14}
	enum live: {'Yes': 1, 'No': 2}	
	scope :male_thalachery, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 1, gender: 1).order('token_no ASC') }
	scope :female_thalachery, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: [1,2,3,4,5,6,7,8,9,10,11], gender: 2).order('token_no ASC') }
	scope :male_boyer, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 2, gender: 1).order('token_no ASC') }
	scope :matured_male_kid, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 1, gender: 3).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :matured_female_kid, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 1, gender: 4).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :kid_female_thalachery, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 1, gender: 4).where("DATE(date_of_birth) > Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :kid_male_thalachery, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 1, gender: 3).where("DATE(date_of_birth) > Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :kid_male_semmari, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 3, gender: 3).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :kid_female_semmari, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 3, gender: 4).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :male_semmari, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 3, gender: 1).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :female_semmari, lambda { |user|
    	where(user_id: user.id, live:1, breed_variety: 3, gender: 2).where("DATE(date_of_birth) < Date(?)", Date.today - 90).order( 'token_no ASC') }
	scope :live_goat, lambda { |user| where("user_id = ? and live = ?",user.id, 1).order('token_no ASC') }
	scope :kid_goat, lambda { |user|
    	where(user_id: user.id,live: 1, breed_variety: 1, gender: [3,4]).left_outer_joins(:parent_goat).where(parent_goats: {parent: nil}).order( 'token_no ASC') }
    scope :edit_kid_goat, lambda { |user|
        where(user_id: user.id,live: 1, breed_variety: 1, gender: [3,4]).order( 'token_no ASC') }
	scope :in_batch, -> { joins(:batch_goats).where.not(batch_goats: { goat_id: nil }) }
	
	def purchase_rate
	  weight * purchase_goat.rate
	end

	def self.without_batch_goat(user)
		no_batch = Goat.where("user_id = ? and live = ?",user.id, 1).left_outer_joins(:batch_goat).where(batch_goats: {goat_id: nil})
		no_status = Goat.where("user_id = ? and live = ?",user.id, 1).joins(:batch_goat).where(batch_goats: {status: 2})
		without_batch_goat = no_batch + no_status
		return without_batch_goat.sort_by(&:token_no)
	end 

	def self.edit_without_batch_goat(user,batch_id)
		no_batch = Goat.live_goat(user).left_outer_joins(:batch_goats).in_batch
		all_female = Goat.live_goat(user)
		without_batch_goat = no_batch + all_female

    return without_batch_goat.sort_by(&:token_no)
		# all_goat = Goat.where("user_id = ? and live = ?",user.id, 1).joins(:batch_goat)
		# no_batch = Goat.where("user_id = ? and live = ?",user.id, 1).left_outer_joins(:batch_goat).where(batch_goats: {goat_id: nil})
		# no_status = Goat.where("user_id = ? and live = ?",user.id, 1).joins(:batch_goat).where(batch_goats: {status: 2})
		# edit_batch = Goat.where("user_id = ? and live = ?",user.id, 1).joins(:batch_goat).where(batch_goats: {batch_id: batch_id, status: 1})
		# without_batch_goat = no_batch + all_goat 
		
		# return without_batch_goat.sort_by(&:token_no)
	end 

	def self.today_goat_status(user)
		result = {
			early_milking: { count: 0, data: [] },
			mid_milking: { count: 0, data: [] },
			end_milking: { count: 0, data: [] },
			pregnancy: { count: 0, data: [] },
			heat: { count: 0, data: [] },
			non_pregnancy: { count: 0, data: [] },
			confirmed_stage: { count: 0, data: [] },
			calve_stage: { count: 0, data: [] },
			abortion: { count: 0, data: [] }
		}

		Goat.female_thalachery(user).each do |goat|
			goat.goat_calving_periods.each do |calve_period|
			next unless calve_period.live == 'Yes'

			date = calve_period.goat.children_parent_goats.joins(:children).pluck('date_of_birth').uniq.sort.reverse.first
			data = {
				token_no: calve_period.goat.token_no,
				status: calve_period.goat_status,
				date: Date.today,
				days: (Date.today - calve_period.date.to_date).to_i
			}
			if calve_period.goat_status == "Milking"
				data[:kid_count] = calve_period.goat.children_parent_goats.joins(:children).where('date_of_birth = ?', date).count
			end

			case data[:status]
			when "Milking"
				if data[:days] <= 30
				result[:early_milking][:data] << data
				result[:early_milking][:count] += 1
				elsif data[:days] <= 60
				result[:mid_milking][:data] << data
				result[:mid_milking][:count] += 1
				else
				result[:end_milking][:data] << data
				result[:end_milking][:count] += 1
				end
			when "Pregnancy"
				result[:pregnancy][:data] << data
				result[:pregnancy][:count] += 1
			when "Heat"
				result[:heat][:data] << data
				result[:heat][:count] += 1
			when "Non Pregnancy"
				result[:non_pregnancy][:data] << data
				result[:non_pregnancy][:count] += 1
			when "Confirmed"
				result[:confirmed_stage][:data] << data
				result[:confirmed_stage][:count] += 1
			when "Calve Stage"
				result[:calve_stage][:data] << data
				result[:calve_stage][:count] += 1
			when "Abort"
				result[:abortion][:data] << data
				result[:abortion][:count] += 1
			end
			end
		end

  		result
	end

	def self.hole_goat_status(user)
		data = []
		Goat.female_thalachery(user).each do |goat|
			data << get_status(goat)
		end
		return data
	end

	def self.get_status(goat)
	    result = []
		current_date = Date.today.prev_month
		(1..16).each do |_|
		end_date_month = current_date.end_of_month
			[end_date_month, current_date.beginning_of_month + 14].each do |target_date|
				period = goat.goat_calving_periods.where('date <= ?', target_date)
				calving_period = period.present? ? period.last : nil
				if calving_period.nil?
				result_key = target_date == end_date_month ? :end_date_month : :fifteenth_date_month
				result << {
					token_no: goat.token_no,
					status: '-',
					date: target_date,
					days: 0
				}
				else
				day_in_status = (calving_period.date.to_date - target_date).to_i
				result_key = target_date == end_date_month ? :end_date_month : :fifteenth_date_month
				result << {
					token_no: goat.token_no,
					status: calving_period.goat_status,
					date: target_date,
					days: day_in_status
				}
				end
			end
		current_date = current_date.prev_month
	    end
		return result
	end

	def self.goat_kidded_count(user)
		# code need to be change for more efficient by adding date field in parent goat and update childerean date of birth so that can query easily
		data = []
		Goat.female_thalachery(user).each do |goat|
			data << kid_count(goat)
		end
		return data
	end

	def self.kid_weight(user)
		# code need to be change for more efficient by adding date field in parent goat and update childerean date of birth so that can query easily
		data = []
		goats = Goat.where(user_id: user.id, live: 1, breed_variety: 1, gender: 3).where("DATE(date_of_birth) > Date(?)", Date.today - 90).order('date_of_birth DESC')
		goats.each do |goat|
			days = (Date.today - goat.date_of_birth).to_i
			if days <= 95
				data << goat
			end
		end
		goats = Goat.where(user_id: user.id, live: 1, breed_variety: 1, gender: 4).where("DATE(date_of_birth) > Date(?)", Date.today - 90).order('date_of_birth DESC')
		goats.each do |goat|
			days = (Date.today - goat.date_of_birth).to_i
			if days <= 95
				data << goat
			end
		end
		return data
	end

	def self.kid_count(goat)
		result = []
		dob = goat.children_parent_goats.joins(:children).pluck('date_of_birth').uniq.sort.reverse
		dob.each_with_index do |date,index|
		  if index+1 == dob.length
			result << {
			token_no: goat.token_no,
			date: date,
			kid_count: goat.children_parent_goats.joins(:children).where('date_of_birth = ?', date).count,
			kid_cycle: 0
			} 
		  else
			result << {
			token_no: goat.token_no,
			date: date,
			kid_count: goat.children_parent_goats.joins(:children).where('date_of_birth = ?', date).count,
			kid_cycle: (((date - dob[index+1]).to_i)/30)
			}
		  end
		end
	    return result
	end

	def self.today_goat_status_by_batch(user)
		results = []
		Batch.includes(batch_goats: { goat: :goat_calving_periods }).where(user_id: user.id).each do |batch|
			batch_data = {
			name: batch.name,
			rack: batch.rack_detail.name,
			data: []
			}
			batch.batch_goats.each do |batch_goat|
				batch_goat.goat.goat_calving_periods.each do |calve_period|
					next unless calve_period.live == 'Yes'
					data = {
					token_no: calve_period.goat.token_no,
					status: calve_period.goat_status,
					date: Date.today,
					days: (Date.today - calve_period.date.to_date).to_i
					# health: calve_period.date.to_date -
					}
					batch_data[:data] << data
				end
			end
			results << batch_data
		end
		results
	end

	def  self.number_wise_today_goat_status_by_batch(user)
		Goat.female_thalachery(user).order(:token_no).flat_map do |goat|
			goat.goat_calving_periods.select { |cp| cp.live == 'Yes' }.map do |calve_period|{token_no: calve_period.goat.token_no,status: calve_period.goat_status}
			end
    end
	end

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________# Parent Mapping Data Monthly____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		# mate_goat_table_data = []
		each_kid_goat_table_data = [["DOB","Token Number","Gender", "Breed Vareity","Weight", "Live", "Created" ]]
		kid_goat = Goat.where(created_at: start_date..end_date).order(created_at: :asc)
		kid_goat.each do |item|
			each_kid_goat_table_data << [
				item.date_of_birth&.strftime("%Y-%m-%d") || "N/A",
				item.token_no || "N/A",
				item.gender || "N/A",
				item.breed_variety || "N/A",
				item.weight || "N/A",
				item.live || "N/A",
				item.created_at&.strftime("%Y-%m-%d") || "N/A",
			]
		end
		pdf.move_down(20)
		pdf.text "For #{month_name} Month New Kid Data Record Created #{kid_goat.count}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_kid_goat_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end

	def sale_month_from_birth(date_of_birth)
		(date_of_birth + 7.months).strftime('%B') # Returns the full month name
	end

	def self.generate_sale_prediction_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
	
		# Invoice and Yearly Sales
		generate_yearly_sales_table(pdf)
		
		# Sales Prediction
		generate_sales_prediction_table(pdf, current_user)
		  
		pdf.render
	  end
	  
	  private
	  
	  # Generate Sales Prediction Table
	  def self.generate_sales_prediction_table(pdf, current_user)
		sales_kid_goat_table_data = [["Month", "Male", "Female", "Male Sales Amount", "Female Sales Amount", "Total Amount", "Expenses", "Income"]]
		male_kid_goats = Goat.live_goat(current_user).where(gender: 3)
		female_kid_goats = Goat.live_goat(current_user).where(gender: 4)
	  
		current_month = Date.today.month
		remaining_months = (current_month..12).map { |m| Date::MONTHNAMES[m] }
		remaining_months.each_with_index do |month, index|
		  start_date = Date.new(Date.today.year, index + 1, 1)
		  end_date = start_date.end_of_month
		  current_year = Date.today.year
		  male_count = count_goats_for_sale(male_kid_goats, month)
		  female_count = count_goats_for_sale(female_kid_goats, month)
		  
		  male_sales = calculate_sales(male_count, 375, 25) # Male goat sale price
		  female_sales = calculate_sales(female_count, 450, 25) # Female goat sale price
		  total_sales = male_sales + female_sales
		  expenses = FarmExpense.where(date: start_date..end_date).sum(:amount)
		  if expenses.zero?
			# Try to get the total expenses for the previous month (current year)
			previous_month_start = (start_date - 1.month).beginning_of_month
			previous_month_end = (start_date - 1.month).end_of_month
			previous_month_expenses = FarmExpense.where(date: previous_month_start..previous_month_end).sum(:amount)
	  
			if previous_month_expenses.zero?
			  last_record_start = Date.today.beginning_of_year
			  last_record_end = last_record_start.end_of_month
			  last_year_december_expenses = FarmExpense.where(date: last_record_start..last_record_end).sum(:amount)				
			  # Use the December last year's expenses if previous month is also zero
			  expenses_to_use = last_year_december_expenses
			else
			  expenses_to_use = previous_month_expenses
			end
	  
			# Update the current month's expenses with the fetched value
			FarmExpense.where(date: start_date..end_date).update_all(amount: expenses_to_use)
			expenses = expenses_to_use
		  end
		  sales_kid_goat_table_data << [
			month,
			male_count || "N/A",
			female_count || "N/A",
			format_currency(male_sales),
			format_currency(female_sales),
			format_currency(total_sales),
			format_currency(expenses),
			format_currency(total_sales - expenses)
		  ]
		end
	  
		pdf.move_down(20)
		pdf.text "For #{Date.today.year} Sales Predicted (Male: 375rs, Female: 450rs, Avg weight: 25kg)", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
	  
		add_table_to_pdf(pdf, sales_kid_goat_table_data)
	  end
	  
	  # Generate Yearly Sales and Expense Table
	  def self.generate_yearly_sales_table(pdf)
		sales_kid_goat_table_data = [["Month", "Male", "Female", "Sale Amount", "Expenses", "Income"]]
		
		current_month = Date.today.month
		months_to_date = (Date.today.beginning_of_year.month..current_month).map { |m| Date::MONTHNAMES[m] }
	  
		months_to_date.each_with_index do |month, index|
		  start_date = Date.new(Date.today.year, index + 1, 1)
		  end_date = start_date.end_of_month
	  
		  expenses = FarmExpense.where(date: start_date..end_date).sum(:amount)
		  invoices = Invoice.where(sale_date: start_date..end_date)
		  total_amount = invoices.sum(:total_amount)
	  
		  sales_kid_data = invoices.joins(goat_sales: :goat)
								   .where("goats.breed_variety = ? AND goats.gender IN (?)", "1", ["3", "4"])
								   .group("goats.gender").count
	  
		  sales_male_count = sales_kid_data["Kid Male"].to_i
		  sales_female_count = sales_kid_data["Kid Female"].to_i
		  income = total_amount - expenses
		  sales_kid_goat_table_data << [
			month,
			sales_male_count || "N/A",
			sales_female_count || "N/A",
			format_currency(total_amount),
			format_currency(expenses),
			format_currency(income)
		  ]
		end
	  
		pdf.move_down(20)
		pdf.text "For #{Date.today.year} Invoice Sales Amount", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
	  
		add_table_to_pdf(pdf, sales_kid_goat_table_data)
	  end
	  
	  # Add Table to PDF
	  def self.add_table_to_pdf(pdf, table_data)
		pdf.table(
		  table_data,
		  header: true,
		  row_colors: ["DDDDDD", "FFFFFF"],
		  cell_style: {
			border_color: '000000',
			border_style: :solid,
			border_width: 1,
			size: 10,
			padding: 5
		  }
		) do
		  row(0).font_style = :bold
		  columns(1..5).align = :center
		end
	  end
	  
	  # Helper Methods
	  def self.sale_month_from_birth(birth_date)
		(birth_date + 7.months).strftime("%B")
	  end
	  
	  def self.count_goats_for_sale(goats, target_month)
		goats.count { |goat| sale_month_from_birth(goat.date_of_birth) == target_month }
	  end
	  
	  def self.calculate_sales(count, price_per_goat, weight)
		count * price_per_goat * weight
	  end
	  
	  def self.format_currency(amount)
		"RS. #{'%.2f' % amount}"
	  end

	end


