class Crop < ApplicationRecord
	has_many :land_crops, inverse_of: :crop
	
	validates_presence_of :name
	validates_presence_of :yield
	validates_presence_of :details
	validates_presence_of :intercrop
	validates_presence_of :nutrition
	enum variety: {'Monocot':1, 'Dicot': 2 }
	enum period: {'40':1, '45':2, '50':3, '60':4, '80':5, '90':6, '120':7, '130':8, '180':9, '1':10, '2':11, '3':12, '4':13, '5':14 }
	enum seed_acre: {  '1Kg':1, '2Kg':2, '3Kg':3, '4Kg':4, '5Kg':5, '6Kg':6, '7Kg':7, '8Kg':8, '9Kg':9, '10Kg':10, '11Kg':11, '12Kg':12, '13Kg':13, '14Kg':14, '15Kg':15, '16Kg':16, '17Kg':17, '18Kg':18, '19Kg':19, '20Kg':20, '21Kg':21, '22Kg':22, '23Kg':23, '24Kg':24, '25Kg':25, '26Kg':26, '27Kg':27, '28Kg':28, '29Kg':29, '30Kg':30,  '31Kg':31, '32Kg':32, '33Kg':33, '34Kg':34, '35Kg':35, '36Kg':36, '37Kg':37, '38Kg':38, '39Kg':39, '40Kg':40,'41Kg':41, '42Kg':42, '43Kg':43, '44Kg':44, '45Kg':45, '46Kg':46, '47Kg':47, '48Kg':48, '49Kg':49, '50Kg':50  }
	enum water_requirement: { 'High':1, 'Medium':2, 'Low':3}
	enum sowing_distance: { '1 ft':1, '1.5 ft':2, '2 ft':3, '2.5 ft':4, '3 ft':5,'3.5 ft':6,  '4 ft':7, '4.5 ft':8, '5 ft':9, '5.5 ft':10, '6 ft':11, '6.5 ft':12, '7 ft':13, '7.5 ft':14, '8 ft':15, '8.5': 16}
	enum season: {'January':1, 'February':2, 'March':3, 'April':4, 'May':5, 'June':6, 'July':7, 'August':8, 'September':9, 'October':10, 'November':11, 'December':12}
end
