class FeedRate < ApplicationRecord

	validates_presence_of :concentrate_feed
	validates_presence_of :green_fodder
	validates_presence_of :dry_fodder
	validates_presence_of :chaval_powder
	
end
