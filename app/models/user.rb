class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

	has_many :goats, inverse_of: :user
	has_many :main_works, inverse_of: :user
	has_many :rack_details, inverse_of: :user
	has_many :diseases, inverse_of: :user
	has_many :farm_expenses, inverse_of: :user
	has_many :batches, inverse_of: :user	
	has_many :invoices, inverse_of: :user
	has_many :purchaser_bill, inverse_of: :user
	has_many :coconut_invoices, inverse_of: :user
	has_many :employees, :class_name => 'UserEmployee'
	has_one :business, inverse_of: :user
	has_many :pending_works, inverse_of: :user
	has_many :feed_availabilities, inverse_of: :user
	has_many :feeds, inverse_of: :user
	
	enum type_of_business: {'Agriculture': 1, 'Coconut Procurement Center': 2,}
	

end
