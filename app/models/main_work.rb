class MainWork < ApplicationRecord

	has_many :daily_works, inverse_of: :main_work, dependent: :destroy 
	belongs_to :user, inverse_of: :main_works	



	validates_presence_of  :breed_variety
    validates_presence_of  :day
    validates_presence_of  :action

	enum breed_variety: { 'Male Thalachery': 1, 'Female Thalachery': 2, 'Male Boyer': 3, 'Female Semmari': 4, 'Male Vella': 5, 'Female Vella': 6, 'Male Semmari': 7, 'Female Boyer': 8, 'Kid Male Thalachery': 9, 'Kid Female Thalachery': 10, 'Kid Female Semmari': 11, 'Kid Male Vella': 12, 'Kid Female Vella': 13, 'Kid Male Semmari': 14 }

	enum action: { 'Mother Sepration': 1, 'Weight Sepration': 2, 'Injection': 3, 'Milking': 4, 'Pregnancy': 5, 'Heat': 6, 'Moving': 7, 'Feed': 8, 'New Born': 9,  'Trim Hooves': 10, 'Non Pregnancy': 11, 'Weight Checkup': 12, 'Horns Reove': 13, 'Male Castrating': 14, 'In Ability': 15, 'Other': 16, 'Abort': 17, 'Confirmed': 18, 'Calve Stage': 19, 'Deworm': 20}

	scope :male_thalachery, -> { where(breed_variety: 'Male Thalachery') }
	scope :female_thalachery, -> { where(breed_variety: 'Female Thalachery')}
	scope :pregnancy_female, -> { where(breed_variety: 'Female Thalachery',action: 'Pregnancy') }
	scope :milking_female, -> { where(breed_variety: 'Female Thalachery',action: 'Milking') }
	scope :non_pregnancy_female, -> { where(breed_variety: 'Female Thalachery',action: 'Non Pregnancy') }
	scope :heat_female, -> { where(breed_variety: 'Female Thalachery',action: 'Heat') }
	scope :abort_female, -> { where(breed_variety: 'Female Thalachery',action: 'Abort') }
	scope :confirmed_female, -> { where(breed_variety: 'Female Thalachery',action: 'Confirmed') }
	scope :calve_stage_female, -> { where(breed_variety: 'Female Thalachery',action: 'Calve Stage') }
	scope :male_boyer, -> { where(breed_variety: 'Male Boyer') }
	scope :kid_female_thalachery, -> { where(breed_variety: 'Kid Female Thalachery') }
	scope :kid_male_thalachery, -> { where(breed_variety: 'Kid Male Thalachery') }
	scope :kid_male_semmari, -> { where(breed_variety: 'Kid Male Semmari') }
	scope :kid_female_semmari, -> { where(breed_variety: 'Kid Female Semmari') }
	scope :male_semmari, -> { where(breed_variety: 'Male Semmari') }
	scope :female_semmari, -> { where(breed_variety: 'FeMale Semmari') }


	def self.today
		today = Date.today
		male_goat = Goat.female_thalachery
		male_work = MainWork.female_thalachery
		male_goat.each do |goat|
			goat_day = (Date.today - goat.date_of_birth).to_i 
			main_works  = MainWork.female_thalachery.where(:day => goat_day)
			if main_works.present?
				main_works.each do |main_work|
				DailyWork.create(goat_id: goat.id, main_work_id: main_work.id)
				end
			end
		end
		DailyWork.all
	end

end
