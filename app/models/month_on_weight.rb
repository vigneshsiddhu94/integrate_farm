class MonthOnWeight < ApplicationRecord
  belongs_to :business
  belongs_to :user

  has_many :batch_weights, inverse_of: :month_on_weight

  accepts_nested_attributes_for :batch_weights, reject_if: :all_blank
  validate :month_check

  def month_check
   existing_dates = MonthOnWeight.pluck(:date)
    existing_dates.each do |stored_date|
      stored_month = stored_date.strftime("%B %Y")
      data_month = self.date.strftime("%B %Y")
      if stored_month == data_month
        errors.add(:date, 'already exists for this month')
        return false
      end
    end
    return true
  end

  def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		# __________________________#Total Weight Data_____________________________________________
		parsed_date = Date.parse(date)  
		month = parsed_date.month
    year = parsed_date.year 
    last_month = parsed_date.last_month
    start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
    end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
    last_month_start_date = parsed_date.last_month.beginning_of_month.strftime("%Y-%m-%d")
    last_month_end_date = parsed_date.last_month.end_of_month.strftime("%Y-%m-%d")
    #death kid counts
    death_goat = DeathGoat.where(died_date: start_date..end_date)
    if death_goat.present?
      death_kid_data = death_goat.joins(:goat).where("goats.breed_variety IN (?) AND goats.gender IN (?)", ["1"], ["3", "4"]).group("goats.gender").count
      death_male_count = death_kid_data["Kid Male"].to_i if death_kid_data.present?
      death_female_count = death_kid_data["Kid Female"].to_i if death_kid_data.present?
      death_weight = death_goat.sum(:weight)
    end
    #sales kid count
    invoice = Invoice.where(sale_date: start_date..end_date)
    if invoice.present?
      sales_kid_data = invoice.joins(goat_sales: :goat).where("goats.breed_variety IN (?) AND goats.gender IN (?)", ["1"], ["3", "4"]).group("goats.gender").count
      sales_male_count = sales_kid_data["Kid Male"].to_i if sales_kid_data.present?
      sales_female_count = sales_kid_data["Kid Female"].to_i if sales_kid_data.present?
      sales_weight = invoice.sum(:total_weight) if invoice.present?
    end
    #new kid count
    birth_male_kid = Goat.where(gender: 'Kid Male').where(date_of_birth: start_date..end_date).count
    birth_female_kid = Goat.where(gender: 'Kid Female').where(date_of_birth: start_date..end_date).count
    #Kid counts
    sales_male_count ||= 0
    sales_female_count ||= 0
    sales_weight ||= 0
    death_male_count ||= 0
    death_female_count ||= 0
    death_weight ||= 0
    sales_count = sales_male_count + sales_female_count
    death_count = death_male_count + death_female_count
    birth_kid_count = birth_male_kid + birth_female_kid

    #Last Month data
    last_month_weight = MonthOnWeight.where('extract(month from date) = ? AND extract(year from date) = ?', last_month.month, last_month.year)
    male_last_month_count = last_month_weight.present? ? last_month_weight.first.total_male_count : 0
    female_last_month_count = last_month_weight.present? ? last_month_weight.first.total_female_count : 0
    total_last_month_count = male_last_month_count + female_last_month_count
    weight_last_month = last_month_weight.present? ? last_month_weight.first.total_weight : 0
    #This month weight should be
    predicted_male_count = male_last_month_count + birth_male_kid - sales_male_count - death_male_count
    predicted_female_count  = female_last_month_count + birth_female_kid - sales_female_count - death_female_count
    predicted_total_kid = predicted_male_count + predicted_female_count
    predicted_total_weight = last_month_weight.present? ? ((((predicted_male_count - birth_male_kid) + (predicted_female_count - birth_female_kid))*4) + ((birth_male_kid + birth_female_kid)*2))+ last_month_weight.first.total_weight : 0
    #Current Month Data
    month_weight = MonthOnWeight.where('extract(month from date) = ? AND extract(year from date) = ?', month, year)
    male_now = month_weight.present? ? month_weight.first.total_male_count : 0 
    female_now = month_weight.present? ?  month_weight.first.total_female_count : 0
    total_kid_now = male_now + female_now
    weight_now = month_weight.present? ?  month_weight.first.total_weight : 0
    weight_gain = weight_now -  weight_last_month
    if total_kid_now.zero?
      avg_weight_goat = 0 
    else
      avg_weight_goat = weight_gain.to_f / total_kid_now
    end
    
    # __________________________Last Month Weight Data__________________________________________________
    last_month_weight_data_table_data = [["Male","Female","Total Count", "Total Weight"]]
		last_month_weight_data_table_data << [
      male_last_month_count,
      female_last_month_count,
      total_last_month_count,
      weight_last_month
    ]
		pdf.move_down(20)
		pdf.text "#{parsed_date.last_month.strftime("%B")} Kid Count And Weight", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(last_month_weight_data_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

    # __________________________This Month Sales and Death and New Kid__________________________________________________
    last_month_weight_data_table_data = [["Sales Male","Sales Female","Death Male","Death Female","New Kid Male","New Kid Female"]]
		last_month_weight_data_table_data << [
      sales_male_count,
      sales_female_count,
      death_male_count,
      death_female_count,
      birth_male_kid,
      birth_female_kid
    ]
		pdf.move_down(20)
		pdf.text "#{parsed_date.strftime("%B")} Sales and Death and New Kid", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(last_month_weight_data_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

    # __________________________This Month Count Prediction__________________________________________________
    last_month_weight_data_table_data = [["Male","Female","Total Count", "Total Weight"]]
		last_month_weight_data_table_data << [
      predicted_male_count,
      predicted_female_count,
      predicted_total_kid,
      predicted_total_weight
    ]
		pdf.move_down(20)
		pdf.text "#{parsed_date.strftime("%B")} Kid Count Prediction And Should Match", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(last_month_weight_data_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

    # __________________________This Month Noted Weight Growth__________________________________________________
    last_month_weight_data_table_data = [["Male","Female","Total Count", "Total Weight", "Last Month Weight" ,"Weight Gain", "Avg/goat"]]
		last_month_weight_data_table_data << [
      male_now,
      female_now,
      total_kid_now,
      weight_now,
      weight_last_month,
      weight_gain,
      avg_weight_goat
    ]
		pdf.move_down(20)
		pdf.text "#{parsed_date.strftime("%B")} Noted Weight Growth", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(last_month_weight_data_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
    end
		pdf.render
	end

end
