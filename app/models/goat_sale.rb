class GoatSale < ApplicationRecord
  	belongs_to :goat, inverse_of: :goat_sale 
  	belongs_to :invoice, inverse_of: :goat_sales

	validates_presence_of :weight
	validates_presence_of :goat
	validates :amount, presence: true, numericality: { only_integer: true }

	enum purpose_of_sale: {'Breed': 1, 'Meat': 2}

	after_create :goat_live_update


    scope :saled_kid_male, lambda { |user| Goat.where(user_id: user.id).joins(:goat_sale).where(breed_variety: 1, gender: 3).order( 'token_no ASC') }
    scope :saled_kid_female, lambda { |user| Goat.where(user_id: user.id).joins(:goat_sale).where(breed_variety: 1, gender: 4).order( 'token_no ASC') }
    scope :Saled_female, lambda { |user| Goat.where(user_id: user.id).joins(:goat_sale).where(breed_variety: 1, gender: 2).order( 'token_no ASC') }


	# scope :saled_kid_male, -> { Goat.joins(:goat_sale).where(breed_variety: ['Kid Male Thalachery']).order( 'token_no ASC') }
	# scope :saled_kid_female, -> { Goat.joins(:goat_sale).where(breed_variety: ['Kid Female Thalachery']).order( 'token_no ASC') }
	# scope :Saled_female, -> { Goat.joins(:goat_sale).where(breed_variety: ['Female Thalachery']).order( 'token_no ASC') }

	def goat_live_update
		Goat.where(id: self.goat_id).update(live: 2)
	end 

	# def saled_amount
	# 	kid_male_amount = .pluck(:amount).inject{ |sum,n| sum+n }
	# 	kid_female_amount = Goat.joins(:goat_sale).where(breed_variety: ['Kid FeMale Thalachery']).pluck(:amount).inject{ |sum,n| sum+n }
	# 	female_amount = Goat.joins(:goat_sale).where(breed_variety: ['FeMale Thalachery']).pluck(:amount).inject{ |sum,n| sum+n }
		
	# end
end
