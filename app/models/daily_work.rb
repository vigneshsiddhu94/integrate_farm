class DailyWork < ApplicationRecord
  
  	belongs_to :goat, inverse_of: :daily_works
  	belongs_to :main_work, inverse_of: :daily_works

	enum status: { 'Not Yet': 1, 'Done': 2}

	# before_validation :set_created_at
  	
	validates :status, presence: true, uniqueness: { scope: [:goat,:main_work] }, on: :create

	def self.today_what(work_date)
		# if DailyWork.last.present?
			# if DailyWork.last.created_at.strftime("%B, %d %y") != Date.today.strftime("%B, %d %y")
			daily_work = DailyWork.where(created_at: work_date.all_day)
			if daily_work.blank?
				goats = Goat.kid_female_thalachery
				if goats.present?
					goats.each do |goat|
						date = goat.date_of_birth
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.kid_female_thalachery.where("day like ?", "%#{goat_month}%")
						day_works = MainWork.kid_female_thalachery.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goats = Goat.kid_male_thalachery
				if goats.present?
					goats.each do |goat|
						date = goat.date_of_birth
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.kid_male_thalachery.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.kid_male_thalachery.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goats = Goat.male_thalachery
				if goats.present?
					goats.each do |goat|
						date = goat.date_of_birth
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.male_thalachery.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.male_thalachery.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goat_calving_periods = GoatCalvingPeriod.pregnancy_goat.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
					date = goat_period.date
					goat_day = (work_date - date).to_i
					goat_month = work_date.strftime("%B %d")
					month_work = MainWork.pregnancy_female.where("day like ?", "%#{goat_month}%") 
					day_works = MainWork.pregnancy_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
					DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goat_calving_periods = GoatCalvingPeriod.milking.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.milking_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.milking_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goat_calving_periods = GoatCalvingPeriod.heat.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.heat_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.heat_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goat_calving_periods = GoatCalvingPeriod.non_pregnancy.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.non_pregnancy_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.non_pregnancy_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goat_calving_periods = GoatCalvingPeriod.abortion.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.abort_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.abort_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goat_calving_periods = GoatCalvingPeriod.confirmed_stage.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.confirmed_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.confirmed_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goat_calving_periods = GoatCalvingPeriod.calve_stage.joins(:goat).where(:goats => {:live => 1})
				if goat_calving_periods.present?
					goat_calving_periods.each do |goat_period|
						date = goat_period.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.calve_stage_female.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.calve_stage_female.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat_id: goat_period.goat_id,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
				goats = Goat.male_boyer
				if goats.present?
					goats.each do |goat|
						date = goat.date_of_birth
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.male_boyer.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.male_boyer.where(day: goat_day)
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goats = Goat.male_semmari
				if goats.present?
					goats.each do |goat|
						date = goat.date_of_birth
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.male_semmari.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.male_semmari.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end

				goats = Goat.female_semmari
				if goats.present?
					goats.each do |goat|
						date = goat.goat_calving_periods.where(live: 'Yes').last.date
						goat_day = (work_date - date).to_i
						goat_month = work_date.strftime("%B %d")
						month_work = MainWork.female_semmari.where("day like ?", "%#{goat_month}%") 
						day_works = MainWork.female_semmari.where(day: goat_day) 
						if day_works.present?
							day_works.each do |day_work|
								DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work: day_work, status: 1)
							end
						end
						DailyWork.create(goat: goat,created_at: work_date.strftime("%Y-%m-%d 00:00:00"),main_work_id: month_work.first.id, status: 1) if month_work.present?
					end
				end
			end
		
		end

end

