class CoconutExpense < ApplicationRecord
  belongs_to :user

  enum reason: {'Male Labour': 1, 'Female Labour': 2, 'Disel':3, 'Emi':4, 'Tractor Rent':5, 'Manager':6, 'Supervisor': 7, 'Driver':8, 'Loadman':9, 'Petrol':10, 'Vechicle Servcie': 11, 'Medical': 12, 'Two Wheeler': 13, 'Tea': 14, 'Vadai': 15, 'Other': 16}
end
