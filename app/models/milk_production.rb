class MilkProduction < ApplicationRecord

	belongs_to :cow, inverse_of: :milk_productions
	belongs_to :business, inverse_of: :milk_productions

	validates_presence_of :milk_date
	# validates_presence_of :fat
	# validates_presence_of :snf
	# validates_presence_of :rate
	enum day_time: { Morning: 'morning', Evening: 'evening' }
	attribute :download_data, :integer, default: 0
	enum download_data: {'Today Farm Data': 1,'Monthly Expenses': 2,'Matting Data': 3, 'Parent And Child Data': 4,'Death Data': 5, 'Invoice': 6,"New Kid Data": 7,'Weight Data': 8, "Sales Goal": 9}

	validates :day_time, :uniqueness => {:scope => [:milk_date, :cow]}
	validates_presence_of :day_time

	def self.generate_cow_data_pdf(date,current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# Parse the date and set the month details
		parsed_date = Date.parse(date)
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month
		end_date = parsed_date.end_of_month

		cow_milk_data = []

		(start_date..end_date).each do |date|
			milk_date_records = MilkProduction.where(milk_date: date, business_id: business_id)
			cow_ids = milk_date_records.pluck(:cow_id).uniq

			cow_ids.each do |cow_id|
			cow_records = milk_date_records.where(cow_id: cow_id)
			cow = cow_records.first.cow

			morning_record = cow_records.find_by(day_time: 'Morning')
			evening_record = cow_records.find_by(day_time: 'Evening')

			morning_litre = morning_record ? morning_record.litre : 0
			evening_litre = evening_record ? evening_record.litre : 0
			total_litre = morning_litre + evening_litre

			cow_milk_data << {
				milk_date: date,
				cow_token: cow.token_no,
				total_litre: total_litre
			}
			end
		end
		# Group cow milk data by date
		grouped_cow_milk_data = cow_milk_data.group_by { |item| item[:milk_date] }
		unique_cow_tokens = cow_milk_data.map { |item| item[:cow_token] }.uniq
		cow_milk_table_data = [["Date"] + unique_cow_tokens + ["Total Milk"]]
		grouped_cow_milk_data.each do |date, records|
			row = [date.strftime("%Y-%m-%d")]
			total_litres = 0
			unique_cow_tokens.each do |token|
			token_record = records.find { |r| r[:cow_token] == token }
			litres = token_record ? token_record[:total_litre] : 0
			row << litres
			total_litres += litres
			end

			row << total_litres
			cow_milk_table_data << row
		end
		pdf.move_down(20)
		pdf.text "Cow Milk Per Day On Month: #{month_name}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(cow_milk_table_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..-1).align = :center
		end

		pdf.render
	end

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new	
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		# __________________________#All Cow Data _____________________________________________
		@cow = Cow.where(business_id: business_id).all
		date = date.present? ? date : (MilkProduction.last.milk_date if MilkProduction.last.present?) || Date.today
		milk_cow = MilkProduction.where(milk_date: date, business_id: business_id).pluck(:cow_id).uniq
		@account_data = []
		litre = 0
		rate = 0
		amount = 0
		avg_rate_per_day = 0
		milk_cow.each do |cow_id|
			cow_date = MilkProduction.where(cow_id: cow_id, milk_date: date, business_id: business_id)
			cow_date.each do |cow|
				litre += cow.litre
				rate += cow.rate
				amount += cow.litre * cow.rate
			end
		end
		begin
		  avg_rate_per_day = (amount/litre).round(2)
		rescue ZeroDivisionError => e
		  result = nil
		end
		feed_rate = Feed.find_by(animal: 'Cow', date: date).present? ? Feed.find_by(animal: 'Cow', date: date).feed_cost : 0 
		income = amount - feed_rate
		@account_data += [date: date, litre: litre, rate: avg_rate_per_day, feed_rate: feed_rate, amount: amount, income: income]
		today_cow_account_data= [["Date", "Total Litre", "Rate", "Amount","Feed Cost", "Total Income"]]
		@account_data.each do |item|
		today_cow_account_data << [
			item[:date],
			item[:litre],
			item[:rate],
			item[:amount],
			item[:feed_rate],
			item[:income]
		]
		end
		pdf.move_down(20)
		pdf.text "Today Income And Loss", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(today_cow_account_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#Each Cow Data _____________________________________________
		milk_date_records = MilkProduction.where(milk_date: date, business: business_id)
		cow_ids = milk_date_records.pluck(:cow_id).uniq
		@cow_milk = []
		cow_ids.each do |cow_id|
		cow_records = milk_date_records.where(cow_id: cow_id, business_id: business_id)
		cow = cow_records.first.cow
		morning_record = cow_records.find_by(day_time: 'Morning')
		evening_record = cow_records.find_by(day_time: 'Evening')
		morning = morning_record ? morning_record.litre : 0
		evening = evening_record ? evening_record.litre : 0
		morning_rate = morning_record ? morning_record.rate : 0
		evening_rate = evening_record ? evening_record.rate : 0
		rate = (morning_rate + evening_rate) / 2
		morning_id = morning_record&.id || cow_records.first&.id
		evening_id = evening_record&.id || cow_records.first&.id
		total_litre = (morning || 0) + (evening || 0)
		# feed_rate = morning_record&.feed_rate || 0
		# feed_rate = (morning_record&.feed_rate || 0) + (evening_record&.feed_rate || 0)
		@cow_milk << {
			morning_id: morning_id,
			evening_id: evening_id,
			cow_id: cow_id,
			cow_token: cow.token_no,
			milk_date: date,
			morning: morning,
			evening: evening,
			rate: rate,
			total_litre: total_litre
			# feed_rate: feed_rate
		}
		end
		cow_milk_table_data = [["Date","Cow Token", "Morning", "Evening", "Rate", "Total Litre"]]
		@cow_milk.each do |item|
		cow_milk_table_data << [
			item[:milk_date],
			item[:cow_token],
			item[:morning],
			item[:evening],
			item[:rate],
			item[:total_litre]
		]
		end
		pdf.move_down(20)
		pdf.text "Cow Milk Per Day And Expenses", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(cow_milk_table_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#Feed Data For Cow_____________________________________________
		@feed = feed_rate = Feed.where(animal: 'Cow', date: date)
		if @feed.present?
			feed_data = [["Date","Animal", "Concentrate/Kg", "Green Fodder/Kg", "Dry Variety", "Dry Fodder/Kg", "Feed Cost"]]
			@feed.each do |item|
			feed_data << [
				item.date,
				item.animal,
				item.concentrate,
				item.green_fodder,
				item.dry_fodder,
				item.dry_fodder_variety,
				item.feed_cost
			]
			end
			pdf.move_down(20)
			pdf.text "Cow Feed and Fooder Details", align: :left, style: :bold, color: "008000"
			pdf.move_down(5)
			pdf.table(feed_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
				row(0).font_style = :bold
				columns(1..3).align = :center
				columns(5..7).align = :right
				self.header = true
			end
		end

		# __________________________#Feed Data For Goat _____________________________________________
		@feed = feed_rate = Feed.where(animal: 'Goat', date: date)
		if @feed.present?
			feed_data = [["Date","Animal", "Concentrate/Kg", "Green Fodder/Kg", "Dry Variety", "Dry Fodder/Kg", "Feed Cost"]]
			@feed.each do |item|
			feed_data << [
				item.date,
				item.animal,
				item.concentrate,
				item.green_fodder,
				item.dry_fodder,
				item.dry_fodder_variety,
				item.feed_cost
			]
			end
			pdf.move_down(20)
			pdf.text "Goat Feed and Fooder Details", align: :left, style: :bold, color: "008000"
			pdf.move_down(5)
			pdf.table(feed_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
				row(0).font_style = :bold
				columns(1..3).align = :center
				columns(5..7).align = :right
				self.header = true
			end
		end

		# __________________________#Land Data _____________________________________________
		@land = Land.where(business_id: business_id).all.order('name ASC')
		land_data = [["Name", "Crop", "Plant Date", "Water Days"]]
		@land.each do |item|
		land_data << [
			item.name,
			(item.land_crops.last.crop.name if item.land_crops.present?),
			((Date.today - item.land_crops.last.date).to_i if item.land_crops.present?),
			((Date.today - item.land_waters.last.date).to_i if item.land_waters.present?)
		]
		end
		pdf.move_down(20)
		pdf.text "Land and Water Detail", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(land_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#Expenses Data _____________________________________________
		@expensess = FarmExpense.where(business_id: business_id, date: date).all.order('created_at ASC')
		expenses_data = [["Date", "Category", "Notes", "Amount"]]
		@expensess.each do |item|
		expenses_data << [
			date,
			item.category,
			item.note,
			item.amount
		]
		end
		pdf.move_down(20)
		pdf.text "Today Farm Expenses", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(expenses_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#PendingWork _____________________________________________
		@pending_work = PendingWork.where(business_id: business_id).all.where(status: false).order('created_at DESC')
		immediate_pending_work = []
		daily_pending_work = []
		goat_pending_work = []
	@pending_work.each do |item|
			case item.work_type
			when "Immediate"
				immediate_pending_work << [item.work_type, item.work]
			when "Daily"
				daily_pending_work << [item.work_type, item.work]
			when "Goat"
				goat_pending_work << [item.work_type, item.work]
			end
		end
		all_pending_work = immediate_pending_work + daily_pending_work + goat_pending_work
		pending_work = [["Work Type","Work List"]]
		all_pending_work.each do |item|
			pending_work << [
				item.first,  # Work Type
				item.second  # Work List
			]
		end
		pdf.move_down(20)
		pdf.text "Pending Work", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(all_pending_work, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
    end
	
end

