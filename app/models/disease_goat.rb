class DiseaseGoat < ApplicationRecord

	belongs_to :goat, inverse_of: :disease_goats
	belongs_to :disease, inverse_of: :disease_goats
  	
  	validates :status, presence: true
	validates :disease_date, presence: true
	enum status: { 'Not': 1, 'Done': 2}

    scope :disease_goat_all, lambda { |user| joins(:disease).where(diseases: {user_id: user.id}).order('disease_date ASC') }



end
