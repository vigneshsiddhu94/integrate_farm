class UserEmployee < ApplicationRecord
  belongs_to :user
  belongs_to :employee, class_name: "User", foreign_key: "employee_id", optional: true
  belongs_to :business
  
  
  enum role: { 'Owner': 1,'Manager': 2, 'Supervisor': 3}



end
