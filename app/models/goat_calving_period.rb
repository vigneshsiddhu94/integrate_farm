class GoatCalvingPeriod < ApplicationRecord

	belongs_to :goat, inverse_of: :goat_calving_periods

	validates_presence_of :goat
	validates_presence_of :goat_status
	validates_presence_of :live
	validates_presence_of :date

	before_validation :set_date
	before_validation :check_goat
	before_create :live_update

	enum goat_status: { 'Pregnancy': 1, 'Milking': 2, 'Heat': 3, 'Non Pregnancy': 4, 'Abort': 5, 'Confirmed': 6, 'Calve Stage': 7, 'Doubt': 8 }
	enum live: {'Yes': 1, 'No': 2}	
	scope :pregnancy_goat, -> { where(goat_status: 'Pregnancy', live:'Yes').order('token_no ASC') }
	scope :milking, -> { where(goat_status: 'Milking', live:'Yes').order( 'token_no ASC') }
	scope :heat, -> { where(goat_status: 'Heat', live:'Yes').order( 'token_no ASC') }
	scope :non_pregnancy, -> { where(goat_status: 'Non Pregnancy', live:'Yes').order( 'token_no ASC') }
	scope :confirmed_stage, -> { where(goat_status: 'Confirmed', live:'Yes').order('token_no ASC') }
	scope :calve_stage, -> { where(goat_status: 'Calve Stage', live:'Yes').order('token_no ASC') }
	scope :abortion, -> { where(goat_status: 'Abort', live:'Yes').order( 'token_no ASC') }
	scope :find_goat_status, -> (id) {where(goat_id: id, live:'Yes')}


	def set_date
  		self.date = DateTime.now if self.date == nil
  	end

  	def check_goat
  		goat = Goat.find_by(id: self.goat_id) if self.goat_id.present?
  		if goat.breed_variety != 'Thalachery' && goat.gener != 'Female'
  			errors.add(:base, "Should Only Female Thalachery Goat")
  		end	
  	end

  	def live_update
  		calve_periods = GoatCalvingPeriod.where(goat_id: self.goat_id, live: 'Yes')
  		if calve_periods.present?
  			calve_periods.each do |goat_calving_period|
				if goat_calving_period.date <= self.date 
	  				goat_calving_period.update(live: 'No') 
	  			else
	  				self.live = 'No' if goat_calving_period.present?
	  			end
	  		end
	  	end

  	end
	

end
