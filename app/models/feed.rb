class Feed < ApplicationRecord

	belongs_to :business
    belongs_to :user

	validates_presence_of :date
	# validates_presence_of :green
	# validates_presence_of :dry
	# validates_presence_of :chaval
	enum dry_fodder_variety: {'Solam': 1, 'Coolam': 2, 'Kadalai Kodi': 3}
	enum animal: {'Cow': 1, "Goat": 2}
	validate :feed_entry_duplication


	before_create :create_fodder_stock
	before_update :check_and_update_fodder_stock
	after_create :daily_feed_cost
	after_update :daily_feed_cost


	def feed_entry_duplication
		animal_feed = Feed.find_by(animal: self.animal, date: self.date)
		if animal_changed? && animal_feed.present?
		 	errors.add(:animal, 'Feed Already Added')
			return false
		end
		return true
	end

	def create_fodder_stock
		create_feed_stock("Cow Concentrate Feed", self.concentrate) if self.animal == 'Cow'
		create_feed_stock("Goat Concentrate Feed", self.concentrate) if self.animal == 'Goat'
		create_feed_stock("Silage", self.green_fodder)
		dry_fodder_varieties = ['Solam', 'Coolam', 'Kadalai Kodi']
		create_feed_stocks_for_dry_fodder(dry_fodder_varieties, self.dry_fodder)
	end

	def create_feed_stock(fodder_variety, amount)
		feed_stock = FeedAvailability.find_by(fodder_variety: fodder_variety, status: true)
		return unless feed_stock.present?
		current_kg = feed_stock.current_kg || feed_stock.stored_kg
		new_kg = current_kg - amount
		if new_kg < 0
			raise StandardError, "#{fodder_variety} feed storage finished. Cannot update stock."
		else
			feed_stock.update(current_kg: new_kg)
		end
	end

	def create_feed_stocks_for_dry_fodder(dry_fodder_varieties, amount)
		dry_fodder_varieties.each do |variety|
			feed_stock = FeedAvailability.find_by(fodder_variety: variety, status: true)
			next unless feed_stock.present?
			current_kg = feed_stock.current_kg || feed_stock.stored_kg
			new_kg = current_kg - amount
			feed_stock.update(current_kg: new_kg) if new_kg >= 0
		end
	end

	def check_and_update_fodder_stock
		feed = Feed.find(self.id)
		if self.animal == 'Cow'
			feed_stock_con = FeedAvailability.find_by(fodder_variety: "Cow Concentrate Feed", status: true)
			if feed_stock_con.present?
				if feed_stock_con.current_kg.present?
					value = feed.concentrate - self.concentrate
					concentrate_feed_stock = feed_stock_con.current_kg + value
				else
					concentrate_feed_stock = feed_stock_con.stored_kg - self.concentrate
				end
				if concentrate_feed_stock < 0
					raise StandardError, "#{feed_stock_con.fodder_variety} feed storage finished. Cannot update stock."
				else
					feed_stock_con.update(current_kg: concentrate_feed_stock)
				end
			end
		else
			feed_stock_goat = FeedAvailability.find_by(fodder_variety: "Goat Concentrate Feed", status: true)
			if feed_stock_goat.present?
				if feed_stock_goat.current_kg.present?
					value = feed.concentrate - self.concentrate
					concentrate_feed_stock = feed_stock_goat.current_kg + value
				else
					concentrate_feed_stock = feed_stock_goat.stored_kg - self.concentrate
				end
				if concentrate_feed_stock < 0
					raise StandardError, "#{feed_stock_con.fodder_variety} feed storage finished. Cannot update stock."
				else
					feed_stock_goat.update(current_kg: concentrate_feed_stock)
				end
			end
	    end
		feed_stock_green = FeedAvailability.find_by(fodder_variety: "Silage", status: true)
		if feed_stock_green.present?
			if feed_stock_green.current_kg.present?
				value = feed.green_fodder - self.green_fodder 
				silage_feed_stock = feed_stock_green.current_kg + value
			else
			  	silage_feed_stock = feed_stock_green.stored_kg - self.green_fodder
			end
			if silage_feed_stock < 0
					raise StandardError, "#{feed_stock_con.fodder_variety} feed storage finished. Cannot update stock."
			else
				feed_stock_green.update(current_kg: silage_feed_stock)
			end	
		end

		if ['Solam', 'Coolam', 'Kadalai Kodi'].include?(self.dry_fodder_variety)
			feed_stock_dry = FeedAvailability.find_by(fodder_variety: self.dry_fodder_variety, status: true)
			if feed_stock_dry.present?
				if feed_stock_dry.current_kg.present?
					value = feed.dry_fodder - self.dry_fodder 
					dry_feed_stock = feed_stock_dry.current_kg + value
				else
				  	dry_feed_stock = feed_stock_dry.stored_kg - self.dry_fodder
				end
				if dry_feed_stock < 0
					raise StandardError, "#{feed_stock_con.fodder_variety} feed storage finished. Cannot update stock."
				else
					feed_stock_dry.update(current_kg: dry_feed_stock)
				end	
			end
		end
	end

	def daily_feed_cost
		if self.animal == 'Cow'
			feed_stock_con = FeedAvailability.find_by(fodder_variety: "Cow Concentrate Feed", status: true)
		else
			feed_stock_con = FeedAvailability.find_by(fodder_variety: "Goat Concentrate Feed", status: true)
		end
		
		con_rate = feed_stock_con.present? ? self.concentrate * feed_stock_con.rate : 0
		
		feed_stock_silage = FeedAvailability.find_by(fodder_variety: "Silage", status: true)
		green_fodder_rate = feed_stock_silage.present? ? self.green_fodder * feed_stock_silage.rate : 0
		
		dry_fodder_rate = 0
		if ['Solam', 'Coolam', 'Kadalai Kodi'].include?(self.dry_fodder_variety)
			feed_stock_dry = FeedAvailability.find_by(fodder_variety: self.dry_fodder_variety, status: true)
			dry_fodder_rate = feed_stock_dry.present? ? self.dry_fodder * feed_stock_dry.rate : 0
		end
		
		total = con_rate + green_fodder_rate + dry_fodder_rate
		self.update_column(:feed_cost, total)
	end
end
