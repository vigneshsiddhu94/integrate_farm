class SalesAndDeath < ApplicationRecord
  belongs_to :business
  belongs_to :user

  enum action_type: { 'Sales': 1, 'Death': 2}
  enum goat_type: {'Kid': 1, 'Goat': 2}
end
