class Calve < ApplicationRecord

	belongs_to :cow, inverse_of: :calves

	validates_presence_of :date_of_birth
	validates_presence_of :breed_variety
	validates_presence_of :gender
	validates_presence_of :token_no, uniqueness: true

	enum gender: { Male: 'Male', Female: 'Female'}

end
