class Invoice < ApplicationRecord
	has_many :goat_sales, inverse_of: :invoice, dependent: :destroy
    belongs_to :user, inverse_of: :invoices

	validates_presence_of :sale_date
	validates :total_amount, presence: true, numericality: { only_integer: true }
    validates :discount, presence: true, numericality: { only_integer: true }

	accepts_nested_attributes_for :goat_sales, reject_if: :all_blank

    scope :invoice_all, lambda { |user| where(user_id: user.id) }
    scope :year_saled_kid_male, lambda {|user,year| Goat.where(breed_variety: 1, gender: 3, user_id: user.id).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    scope :year_saled_kid_female, lambda { |user,year| Goat.where(breed_variety: 1, gender: 4, user_id: user.id).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    scope :year_saled_female,  lambda {|user,year| Goat.where(breed_variety: 1, gender: 2, user_id: user.id).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    scope :year_earn, lambda {|user,year| where('extract(year from sale_date::date) = ? and user_id = ?', year, user.id).pluck(:total_amount).inject{ |sum,n| sum+n }}

    # scope :year_saled_kid_male, -> (year) { Goat.where(breed_variety: ['Kid Male Thalachery']).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    # scope :year_saled_kid_female, -> (year) { Goat.where(breed_variety: ['Kid Female Thalachery']).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    # scope :year_saled_female, -> (year) { Goat.where(breed_variety: ['Female Thalachery']).joins(goat_sale: :invoice).where('extract(year  from sale_date::date) = ?', year).count }
    # scope :year_earn, -> (year) { where('extract(year  from sale_date::date) = ?', year).pluck(:total_amount).inject{ |sum,n| sum+n }}

    def self.select_uniq_year(user)
        print(user,'user')
        Invoice.where(user_id: user.id).select(:sale_date).map {|p| p.sale_date && p.sale_date.year}.uniq
    end

	def calculate_total_weight
		self.total_weight = goat_sales.sum(:weight)
	end

    def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________# Parent Mapping Data Monthly____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		# mate_goat_table_data = []
		each_invoice_table_data = [["Date","No Of Goats","Amount","Created" ]]
		invoice = Invoice.where(created_at: start_date..end_date).order(created_at: :asc)
		invoice.each do |item|
			each_invoice_table_data << [
				item.sale_date&.strftime("%Y-%m-%d") || "N/A",
				item.goat_sales.count || "N/A",
				item.total_amount || "N/A",
				item.created_at&.strftime("%Y-%m-%d") || "N/A",

			]
		end
		pdf.move_down(20)
		pdf.text "For #{month_name} Month Invoice Record Created #{invoice.count}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_invoice_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end

end
