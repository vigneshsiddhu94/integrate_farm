class MateGoat < ApplicationRecord

    belongs_to :male, :class_name => 'Goat'
	belongs_to :female, :class_name => 'Goat'

  	validates_presence_of :date_of_mat

  	after_create :goat_status_change

	scope :mate_goat_all, lambda { |user| joins(:male).where(goats: {user_id: user.id}) }


	def goat_status_change
  	  GoatCalvingPeriod.create(goat_id: self.female_id, goat_status: 'Pregnancy', date: self.date_of_mat)
  	end

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________# Mate Data Monthly____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		# mate_goat_table_data = []
		each_mate_goat_table_data = [["Date", "Male","Female", "Comment", "Created"]]
		mate_goat = MateGoat.where(created_at: start_date..end_date).order(created_at: :asc)
		mate_goat.each do |item|
			each_mate_goat_table_data << [
				item.date_of_mat.strftime("%Y-%m-%d") || "N/A",
				item.male.token_no || "N/A",
				item.female.token_no || "N/A",
				item.comment || "N/A",
				item.created_at&.strftime("%Y-%m-%d") || "N/A"
			]
		end
		pdf.move_down(20)
		pdf.text "For #{month_name} Month Matting Data Record Created #{mate_goat.count}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_mate_goat_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end
  	
end
