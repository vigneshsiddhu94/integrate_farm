class LandCrop < ApplicationRecord
	has_many   :crop_activities, inverse_of: :land_crop
	belongs_to :land, inverse_of: :land_crops
	belongs_to :crop, inverse_of: :land_crops
	belongs_to :business, inverse_of: :land_crops
	has_many :crop_yields, dependent: :destroy

	validates_presence_of :date
	validates_presence_of :fodder

	enum status: {'Yes':1, 'No': 2}
	enum fodder: {'Green':1, 'Dry': 2}

	scope :land_crop_live, -> { where(status: 1) }
	scope :land_crop_dry_live, -> { where(fodder: 2, status: 1) }
	scope :land_crop_green_live, -> { where(fodder: 1, status: 1) }

	before_create :land_crop_status

	def land_name
		land&.name # Fetch land's name safely
	end

	def land_crop_status
		# if self.new_record? != true
		if LandCrop.where(land_id: self.land_id, crop_id: self.crop_id, status: 1).present?
			land_crop = LandCrop.where(land_id: self.land_id, crop_id: self.crop_id, status: 1)
			land_crop.update(status: 2) if land_crop.present?
			# if self.fodder == land_crop.fodder
			if land_crop.present?
				CropActivity.where(land_crop_id: land_crop.first.id).each do |crop_activity|
					crop_activity.update(status: 2) if CropActivity.where(land_crop_id: land_crop.first.id).present?
				end
			end
		end
	end

end
