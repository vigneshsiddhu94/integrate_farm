class CalvingPeriod < ApplicationRecord

	belongs_to :cow, inverse_of: :calving_periods

	validates_presence_of :calve_date
	validates_presence_of :cow_status
	
	enum cow_status: { Pregnancy: 'pregnancy', Milking: 'milking', Insemination: 'insemination', Heat: 'heat', other: 'Other'}
end
