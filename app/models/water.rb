class Water < ApplicationRecord
	has_many :land_waters, inverse_of: :water
	belongs_to :business, inverse_of: :waters
	
	validates_presence_of :name
	validates_presence_of :level
	validates_presence_of :details

	enum source: {'Well':1, 'Bore':2, 'Riverwell':3}
end
