class CoconutInvoice < ApplicationRecord
  has_many :loads, inverse_of: :coconut_invoice, dependent: :destroy
  belongs_to :user
  belongs_to :customer

  accepts_nested_attributes_for :loads, reject_if: :all_blank
end
