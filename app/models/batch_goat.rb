class BatchGoat < ApplicationRecord
  
  belongs_to :batch, inverse_of: :batch_goats
  belongs_to :goat, inverse_of: :batch_goat

  before_create :set_today_date
  
  before_validation :date
  validates_presence_of :status

  enum status: {'Yes': 1, 'No': 2}

  def set_today_date
  	self.date = Date.today if self.date == nil
  end

  def self.batch_goat_weight(batch, date)
  	goats = BatchGoat.joins(:batch).where(batch_id: batch, status: 1).pluck(:goat_id)
    weight = 0
    goats.each do |goat|
      if GoatWeight.where(goat_id: goat).where("date BETWEEN ? AND ?", date.beginning_of_month,date.end_of_month ).present?
        goat_weight = GoatWeight.where(goat_id: goat).where("date BETWEEN ? AND ?", date.beginning_of_month,date.end_of_month ).last.weight 
        weight += goat_weight
      else
        weight = 0
      end
    end
    return weight
    # BatchGoat.joins(goat: :goat_weights).pluck(goat_weights: {weight: 10 })
    #     SELECT "batch_goats".* FROM "batch_goats" INNER JOIN "goats" ON "goats"."id" = "batch_goats"."goat_id"
    # INNER JOIN "batches" ON "batches"."id" = "batch_goats"."batch_id"
    # INNER JOIN "goat_weights" ON "goat_weights"."goat_id" = "goats"."id";
  end

end
