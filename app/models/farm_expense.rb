class FarmExpense < ApplicationRecord

	belongs_to :user, inverse_of: :farm_expenses	
	belongs_to :business, inverse_of: :farm_expenses

	validates_presence_of :category
	validates_presence_of :date
    validates :amount, presence: true, numericality: { only_integer: true }


    scope :farm_expenses_all, lambda { |user| where(user_id: user.id).order('created_at DESC')}

	enum category: {'Farm helper': 1, 'Goat Concentrate feed': 2, 'Cow Concentrate feed': 3, 'Tea and Snacks': 4, 'Petrol': 5, 'Diesel': 6, 'Food': 7, 'Agri helper male': 8, 'Agri helper female': 9, 'Medical': 10, 'Green fodder': 11, 'Milk powder': 12, 'Farm development': 13, 'Insurance': 14, 'Fertilizer': 15, 'Land lease': 16, 'Power weeder': 17, 'Transport': 18, 'Current bill': 19, 'Other': 20, 'Bike maintenance': 21, 'Seed': 22, 'Farm maintenance': 23, 'Dry Fooder': 24, 'Chemical Fertilizer': 25, 'Tractor': 26}

	def self.year_expenses(user, year)
	  expenses = FarmExpense.where('extract (year from date::date) = ? and user_id = ?', year, user.id).pluck(:amount).inject {|sum,n| sum+n}
	  return expenses
	end

	def self.rate_monthly_expenses(user,year,rate)
		if FarmExpense.where('extract(year from date) = ? and user_id = ?',year,user.id).present?
		  month = FarmExpense.where('extract(year from date) = ? and user_id = ?',year,user.id).map {|i| i.date.month }.uniq
		  if GoatWeight.where('extract(month from date) = ? and extract(year from date) = ?',month,year).present?
		      month_by_month = []
		      i = 0
			  month.each do |month|
			    expense = FarmExpense.where('extract(month from date) = ? and extract(year from date) = ? and user_id = ?',month,year,user.id).pluck(:amount).inject {|sum,n| sum+n}
			    weight = GoatWeight.where('extract(month from date) = ? and extract(year from date) = ?',1,2023).select('DISTINCT ON (goat_id) *').order(:goat_id, updated_at: :desc).pluck(:weight).inject {|sum,n| sum+n}
			    income = weight * rate.to_i
			    profit = income - expense
			    month_by_month[i] = [month: month, expense: expense, weight: weight, income: income, profit: profit]
			    i += 1
		      end
		  end
		  return month_by_month
		else
		  return nil
		end
	end

	def self.monthly_expenses(user,year)
		if FarmExpense.where('extract(year from date) = ? and user_id = ?',year,user.id).present?
		  month = FarmExpense.where('extract(year from date) = ? and user_id = ?',year,user.id).map {|i| i.date.month }.uniq
		      month_by_month = []
		      i = 0
			  month.each do |month|
			    expense = FarmExpense.where('extract(month from date) = ? and extract(year from date) = ? and user_id = ?',month,year,user.id).pluck(:amount).inject {|sum,n| sum+n}
			    if GoatWeight.where('extract(month from date) = ? and extract(year from date) = ?',month,year).present?
				    weight = GoatWeight.where('extract(month from date) = ? and extract(year from date) = ?',month,year).select('DISTINCT ON (goat_id) *').order(:goat_id, updated_at: :desc).pluck(:weight).inject {|sum,n| sum+n}
				    income = weight * 350
				    profit = income - expense
				    month_by_month[i] = [month: month, expense: expense, weight: weight, income: income, profit: profit]
				    i += 1
				else
					weight = 0
					income = weight * 350
				    profit = income - expense
				    month_by_month[i] = [month: month, expense: expense, weight: weight, income: income, profit: profit]
				    i += 1
		      	end
		    end
		  return month_by_month
		else
		  return nil
		end
	end

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________#Total Expenses_____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		total_expenses = 0
		month_expenses = []
		category_expenses = []
		FarmExpense.categories.each do |category_name, category_value|
			category_amount = FarmExpense.where(category: category_name).where('extract(month from date) = ?', month).sum(:amount)
			total_expenses += category_amount

			category_expenses << {
				category: category_name,
				category_expenses: category_amount
			}
		end
		month_expenses << {
			"month": month,
			"total_expenses": total_expenses
		}
		total_expenses_table_data = [["Month","Total Expenses"]]
		month_expenses.each do |item|
			total_expenses_table_data << [
				month_name,
				item[:total_expenses],
			]
			end
		pdf.move_down(20)
		pdf.text "#{month_name} Month Expenses", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(total_expenses_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#Expenses By Category Wise_____________________________________________
		category_expenses_table_data = [["Category","Total Expenses"]]
		category_expenses.each do |item|
			category_expenses_table_data << [
				item[:category],
				item[:category_expenses],
			]
		end
		pdf.move_down(20)
		pdf.text "Expenses By Category Wise", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(category_expenses_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end

		# __________________________#ALl Data_____________________________________________
		each_expenses_table_data = [["Date", "Category","Note", "Expenses"]]
		each_expenses = FarmExpense.where(date: start_date..end_date).order(date: :asc)
		each_expenses.each do |item|
			each_expenses_table_data << [
				item.date.strftime("%Y-%m-%d"),
				item.category,
				item.note,
				item.amount
			]
		end
		pdf.move_down(20)
		pdf.text "All Expenses Data ", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_expenses_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end
end
