class Customer < ApplicationRecord
  belongs_to :user
  belongs_to :place
  has_many :coconut_invoices, inverse_of: :customer, dependent: :destroy
end
