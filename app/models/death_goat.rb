class DeathGoat < ApplicationRecord

	belongs_to :goat, inverse_of: :death_goat
	belongs_to :disease, inverse_of: :death_goat
	validates :weight, numericality: { greater_than_or_equal_to: 0 }, allow_nil: true

	scope :death_all,lambda { |user| joins(:goat).where(goats: {user_id: user.id}) }

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________# Parent Mapping Data Monthly____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		# mate_goat_table_data = []
		each_death_goat_table_data = [["Date","Token No","Breed","Disease","Comment","Created" ]]
		death_goat = DeathGoat.where(created_at: start_date..end_date).order(created_at: :asc)
		death_goat.each do |item|
			each_death_goat_table_data << [
				item.died_date&.strftime("%Y-%m-%d") || "N/A",
				item.goat.token_no || "N/A",
				item.goat.breed_variety || "N/A",
				item.disease.name || "N/A",
				item.comment || "N/A",
				item.created_at&.strftime("%Y-%m-%d") || "N/A",

			]
		end
		pdf.move_down(20)
		pdf.text "For #{month_name} Month Death Data Record Created #{death_goat.count}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_death_goat_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end

end
