class Batch < ApplicationRecord
  
  belongs_to :user, inverse_of: :batches
  belongs_to :rack_detail, inverse_of: :batches
  has_many :batch_goats, inverse_of: :batch

  enum status: {'Yes': 1, 'No': 2}

  scope :batches_all, lambda { |user| where(user_id: user.id) }

  accepts_nested_attributes_for :batch_goats, update_only: true, reject_if: :all_blank, allow_destroy: true  

  validates_presence_of :name


end
