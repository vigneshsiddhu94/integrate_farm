class Cow < ApplicationRecord

	has_many :milk_productions, inverse_of: :cow
	has_many :calving_periods, inverse_of: :cow
	has_many :calves, inverse_of: :cow
	has_many :feeds, inverse_of: :cow
	belongs_to :business, inverse_of: :cows

	validates_presence_of :token_no, uniqueness: true
	validates_presence_of :milk_assurance
	validates_presence_of :cow_rate
	validates_presence_of :date_of_purchase
	validates_presence_of :breed_variety
	# validates_presence_of :cow_image
	# validates_presence_of :calving_period
	validates_presence_of :age

	accepts_nested_attributes_for :calving_periods
	accepts_nested_attributes_for :milk_productions
end
