class Farmer < ApplicationRecord
  belongs_to :user
  belongs_to :place
  has_many :purchaser_bill, inverse_of: :farmer
end
