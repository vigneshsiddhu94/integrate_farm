class CropYield < ApplicationRecord
    belongs_to :land_crop
  
    validates :start_date, :end_date, :yield_per_kg, :production_cost, :per_kg_rate, presence: true
    validates :yield_per_kg, :production_cost, :per_kg_rate, numericality: { greater_than_or_equal_to: 0 }
  
    validate :end_date_after_start_date
  
    private
  
    def end_date_after_start_date
      return if end_date.blank? || start_date.blank?
      errors.add(:end_date, "must be after the start date") if end_date <= start_date
    end
  end
  