class Disease < ApplicationRecord
	
	has_many :disease_goats, inverse_of: :disease, dependent: :delete_all
	has_one :death_goat, inverse_of: :disease
	belongs_to :user, inverse_of: :diseases	

	validates_presence_of :name, uniqueness: true
	validates_presence_of :symptoms, uniqueness: true
	validates_presence_of :impact_goat, uniqueness: true
	validates_presence_of :treatment, uniqueness: true

	scope :disease_all,lambda { |user| where("user_id =?",user.id).order('name ASC')  }


end
