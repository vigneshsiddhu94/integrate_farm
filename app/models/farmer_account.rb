class FarmerAccount < ApplicationRecord
  belongs_to :farmer

  # Callback to top up account only on creation
  before_create :top_up_account

  # Class method to top up the farmer account
  def self.top_up_for(farmer_id, amount, date = nil)
    farmer_account = FarmerAccount.find_or_initialize_by(farmer_id: farmer_id)
    
    # Calculate the new balance
    tally_amount = farmer_account.balance_amount.to_f + amount.to_f
    
    # Update the farmer account's amount and balance
    farmer_account.update_columns(amount: amount,
                                  date: date,
                                  balance_amount: tally_amount)

    # Return balance message
    farmer_account.calculate_balance_message(tally_amount)
  end

  private

  # Ensure this only runs before creating the record
  def top_up_account
    if self.new_record?
      # Only top up if it's a new record (creation)
      self.class.top_up_for(farmer_id, amount, date)
    end
  end

  # Helper method to calculate the balance message
  def calculate_balance_message(tally_amount)
    if tally_amount.positive?
      "Sri Balaji Traders has to pay amount RS #{tally_amount}"
    elsif tally_amount.negative?
      "#{farmer.name} has to pay amount RS #{tally_amount.abs}"
    else
      "Balance amount is zero."
    end
  end
end
