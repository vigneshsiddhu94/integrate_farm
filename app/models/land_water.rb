class LandWater < ApplicationRecord
	belongs_to :land, inverse_of: :land_waters
	belongs_to :water, inverse_of: :land_waters
	belongs_to :business, inverse_of: :land_waters

	validates_presence_of :date

	enum status: {'Yes':1, 'No':2}

	scope :land_water_live, -> { where(status: 1) }
	
	before_create :land_water_status

	def land_water_status
		# if self.new_record? != true
		if LandWater.where(land_id: self.land_id, status: 1).present?
			land_waters = LandWater.where(land_id: self.land_id, status: 1)
			land_waters.each do |land_water|
				land_water.update(status: 2)
			end
		end
	end
end
