class ParentGoat < ApplicationRecord
 
	belongs_to :children, :class_name => 'Goat'
	belongs_to :parent, :class_name => 'Goat'
	after_save :female_status_change

	validates :parent, uniqueness: { scope: :children }
	validates :children, uniqueness: true
	
	scope :parent_goat_all,lambda { |user| joins(:children).where(goats: {user_id: user.id}) }

	def female_status_change
	  birth_date = Goat.find_by(id: self.children_id).date_of_birth
	  goat = GoatCalvingPeriod.where(goat_id: self.parent_id, goat_status: 'Milking', date: birth_date)
	  if goat.empty?
	    GoatCalvingPeriod.create(goat_id: self.parent_id, goat_status: 'Milking', date: birth_date)
	  end
	end

	def self.generate_pdf(date, current_user)
		pdf = Prawn::Document.new
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id

		# __________________________# Parent Mapping Data Monthly____________________________________________
		parsed_date = Date.parse(date)
		month = parsed_date.month
		month_name = parsed_date.strftime("%B")
		start_date = parsed_date.beginning_of_month.strftime("%Y-%m-%d")
        end_date = parsed_date.end_of_month.strftime("%Y-%m-%d")
		# mate_goat_table_data = []
		each_parent_mapping_goat_table_data = [["Created","Child Token Number","Parent Token Number", ]]
		parent_mapping_goat = ParentGoat.where(created_at: start_date..end_date).order(created_at: :asc)
		parent_mapping_goat.each do |item|
			each_parent_mapping_goat_table_data << [
				item.created_at&.strftime("%Y-%m-%d") || "N/A",
				item.children.token_no || "N/A",
				item.parent.token_no || "N/A"
			]
		end
		pdf.move_down(20)
		pdf.text "For #{month_name} Month Parent Mapping Data Record Created #{parent_mapping_goat.count}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(each_parent_mapping_goat_table_data,  header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..3).align = :center
			columns(5..7).align = :right
			self.header = true
		end
		pdf.render
	end
end
