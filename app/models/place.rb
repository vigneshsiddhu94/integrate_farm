class Place < ApplicationRecord
  belongs_to :user
  has_many :customers, inverse_of: :place
  # has_many :purchaser_bill, inverse_of: :place
  # has_many :purchaser_bills, :class_name => 'ProcurementPlace', :foreign_key => 'place_id'

  validates :name, presence: true, uniqueness: true
end
