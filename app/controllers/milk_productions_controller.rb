class MilkProductionsController < BaseController

	def index
		@milk_productions = MilkProduction.all.order('created_at DESC')
	end

	def today
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		@cow = Cow.where(business_id: business_id).all
		date = params[:date].present? ? params[:date] : (MilkProduction.last.milk_date if MilkProduction.last.present?) || Date.today
		milk_cow = MilkProduction.where(milk_date: date, business_id: business_id).pluck(:cow_id).uniq
		@account_data = []
		litre = 0
		feed_rate = 0
		rate = 0
		amount = 0
		avg_rate_per_day = 0
		milk_cow.each do |cow_id|
			cow_date = MilkProduction.where(cow_id: cow_id, milk_date: date, business_id: business_id)
			cow_date.each do |cow|
				litre += cow.litre
				rate += cow.rate
				amount += cow.litre * cow.rate
				# feed_rate += cow.feed_rate
			end
		end
		begin
		  avg_rate_per_day = (amount/litre).round(2)
		rescue ZeroDivisionError => e
		  result = nil
		end
		@account_data += [date: date, litre: litre, rate: avg_rate_per_day, amount: amount]
		milk_date_records = MilkProduction.where(milk_date: date, business: business_id)
		cow_ids = milk_date_records.pluck(:cow_id).uniq
		@cow_milk = []
		cow_ids.each do |cow_id|
		cow_records = milk_date_records.where(cow_id: cow_id, business_id: business_id)
		cow = cow_records.first.cow
		morning_record = cow_records.find_by(day_time: 'Morning')
		evening_record = cow_records.find_by(day_time: 'Evening')
		morning = morning_record ? morning_record.litre : 0
		evening = evening_record ? evening_record.litre : 0
		morning_rate = morning_record ? morning_record.rate : 0
		evening_rate = evening_record ? evening_record.rate : 0
		rate = (morning_rate + evening_rate) / 2
		morning_id = morning_record&.id || cow_records.first&.id
		evening_id = evening_record&.id || cow_records.first&.id
		total_litre = (morning || 0) + (evening || 0)
		# feed_rate = morning_record&.feed_rate || 0
		# feed_rate = (morning_record&.feed_rate || 0) + (evening_record&.feed_rate || 0)
		@cow_milk << {
			morning_id: morning_id,
			evening_id: evening_id,
			cow_id: cow_id,
			cow_token: cow.token_no,
			milk_date: date,
			morning: morning,
			evening: evening,
			rate: rate,
			total_litre: total_litre,
			# feed_rate: feed_rate
		}
		end

	end

	def history_cow
		dates = Date.today
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		@milk_productions = []
		@milk_production = MilkProduction.find_by(milk_date: dates, business: business_id)
		if @milk_production.nil?
			dates = Date.yesterday
			@milk_production = MilkProduction.find_by(milk_date: dates,  business: business_id)
		end
		if @milk_production.present?
			cow_token = @milk_production[0].cow.token_no
			date = @milk_production[0].milk_date
			morning = evening = morning_id = evening_id = 0
			@milk_production.each do |production|
				if production.day_time == 'Morning'
				morning = production.litre
				morning_id = production.id
				elsif production.day_time == 'Evening'
				evening = production.litre
				evening_id = production.id
				end
			end
			total_litre = @milk_production.sum(&:litre)
			@milk_productions << {
				morning_id: morning_id,
				evening_id: evening_id,
				cow_id: @milk_production[0].cow_id,
				cow_token: cow_token,
				milk_date: date,
				morning: morning,
				evening: evening,
				total_litre: total_litre
			}
		end
		
		# dates = MilkProduction.order('milk_date DESC').pluck(:milk_date).uniq if params[:cow_id].present?
		# @milk_productions = []
		# dates.each do |date|
		# 	@milk_production = MilkProduction.find_by(cow_id: params[:cow_id], milk_date: date) if params[:cow_id].present?
		# 	if @milk_production.present?
		# 		cow_token = @milk_production[0].cow.token_no
		# 		date = @milk_production[0].milk_date
		# 		if @milk_production[0].day_time == 'Morning' 
		# 			morning = @milk_production[0].litre
		# 			morning_id = @milk_production[0].id
		# 		elsif @milk_production[1].present?
		# 			morning = @milk_production[1].litre if @milk_production[1].day_time == 'Morning'
		# 			morning_id = @milk_production[1].id if @milk_production[1].day_time == 'Morning'
		# 		else
		# 			morning = 0
		# 			morning_id = @milk_production[0].id if  @milk_production[0].present?
		# 			morning_id = @milk_production[1].id if  @milk_production[1].present?
		# 		end
		# 		if @milk_production[0].day_time == 'Evening' 
		# 			evening = @milk_production[0].litre
		# 			evening_id = @milk_production[0].id
		# 		elsif @milk_production[1].present?
		# 			evening = @milk_production[1].litre if @milk_production[1].day_time == 'Evening'
		# 			evening_id = @milk_production[1].id if @milk_production[1].day_time == 'Evening'
		# 		else
		# 			evening = 0
		# 			evening_id = @milk_production[0].id if  @milk_production[0].present?
		# 			evening_id = @milk_production[1].id if  @milk_production[1].present?
		# 		end
		# 		if @milk_production[0]&&@milk_production[1].present? 
		# 			total_litre = @milk_production[0].litre + @milk_production[1].litre
		# 		else
		# 			total_litre = @milk_production[0].litre
		# 		end
		# 		@milk_productions += [morning_id: morning_id, evening_id: evening_id, cow_id: @milk_production[0].cow_id, cow_token: cow_token, milk_date: date, morning: morning, evening: evening, total_litre:  total_litre]
		# 	end
		# end
	end

	def new
		@milk_productions = MilkProduction.new
		user_id = UserEmployee.find_by(employee_id: 1).business.user.id
		@all_cow = Cow.where(user_id: user_id)
		@cow = Cow.find_by(id: params[:cow_id])
		@cow = Cow.find_by(id: params[:cow_id]) if params[:cow_id].present?
	end 

	def edit
		user_id = UserEmployee.find_by(employee_id: 1).business.user.id
		@all_cow = Cow.where(user_id: user_id)
		@milk_productions = MilkProduction.find(params[:id])
		@cow = Cow.find_by(id: params[:cow_id]) if params[:cow_id].present?
	end

	def update
		@milk_production = MilkProduction.find(params[:id])
	   if @milk_production.update(milk_production_params)
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to milk_account_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to milk_account_url
	    end
  	end

	def create
		@milk = MilkProduction.new(milk_production_params)
  	  if @milk.save
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to milk_account_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to milk_account_url
	    end
	end

	def milk_production_params
		params.require(:milk_production).permit(:cow_id, :milk_date, :day_time, :rate, :snf, :fat, :litre, :feed_rate,:user_id, :business_id)
	end

	def download_form
		
	end

	def download_data
		redirect_to generate_pdf_path(data: params[:data], date: params[:date])
	end

	def generate_pdf
		date = params[:date].present? ? params[:date] : (MilkProduction.last.milk_date if MilkProduction.last.present?) || Date.today
		if params[:data] == 'Today Hole Data'
			pdf_data = MilkProduction.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Farm Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Today Farm Data'
			pdf_data = FeedConsumption.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Farm Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Monthly Expenses'
			pdf_data = FarmExpense.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Expenses Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Cow Data'
			pdf_data = MilkProduction.generate_cow_data_pdf(date, current_user)
			send_data pdf_data, filename: "Cow Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Weight Data'
			pdf_data = MonthOnWeight.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Weight Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Matting Data'
			pdf_data = MateGoat.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Matting Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Parent And Child Data'
			pdf_data = ParentGoat.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Parent And Child Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Death Data'
			pdf_data = DeathGoat.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Death Data #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Invoice'
			pdf_data = Invoice.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Invoice #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'New Kid Data' 
			pdf_data = Goat.generate_pdf(date, current_user)
			send_data pdf_data, filename: "Invoice #{date}.pdf", type: "application/pdf", disposition: "inline"
		elsif params[:data] == 'Sales Goal' 
			pdf_data = Goat.generate_sale_prediction_pdf(date, current_user)
			send_data pdf_data, filename: "Invoice #{date}.pdf", type: "application/pdf", disposition: "inline"
		end		  
		  
    end

	def today_farm_update
		# @land_waters = LandWater.where(business_id: current_user.business.id)
		# @feed_availabilities = FeedAvailability.where(business_id: current_user.business.id)
	end
  	
	def create_today_farm_status
		ActiveRecord::Base.transaction do
		  # Convert date param to Date object
		  date_param = params[:date].to_date
		  # Delete existing FeedConsumption records for the given date and business
		  FeedConsumption.where(date: date_param, business_id: current_user.business.id).destroy_all
	  
		  # Create LandWater records for each selected land
		  filtered_land_ids = params[:land_ids].reject(&:blank?)
		  land_ids = filtered_land_ids || []
	  
		  land_ids.each do |land_id|
			LandWater.create!(
			  water_id: params[:water_id],
			  land_id: land_id,
			  date: date_param,
			  business_id: current_user.business.id
			)
		  end
	  
		  # Create new FeedConsumption record
		  @feed_consumption = FeedConsumption.new(feed_consumption_params)
		  @feed_consumption.date = date_param
		  @feed_consumption.business_id = current_user.business.id
		  @feed_consumption.save!
		  redirect_to today_farm_update_path, notice: "Farm status created successfully."
		end
	  rescue ActiveRecord::RecordInvalid => e
		flash.now[:alert] = "Error creating farm status: #{e.message}"
		render :today_farm_update, status: :unprocessable_entity
	  end
	  
	  
	private
	  
	def feed_consumption_params
		params.permit(:date, :concentrate_feed, :thippi, :asola, :dry_fodder, :green_fodder,:agathi_mulberry,:silage,:wastage_green,:wastage_dry, :cutting_land)
	end 

end
