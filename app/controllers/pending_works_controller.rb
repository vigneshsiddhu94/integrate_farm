class PendingWorksController < ApplicationController
  before_action :set_pending_work, only: %i[ show edit update destroy ]

  # GET /pending_works or /pending_works.json
  def index
    @pending_works = PendingWork.where(business_id: 1, status: false, work_type: "Immediate").order(updated_at: :desc)
  end

  def goat_pending_work
    @pending_works = PendingWork.where(business_id: 1, status: false, work_type: "Goat").order(updated_at: :desc)
  end

  def daily_pending_work
    @pending_works = PendingWork.where(business_id: 1, status: false, work_type: "Daily").order(date: :ASC)
  end

  # GET /pending_works/1 or /pending_works/1.json
  def show
  end

  # GET /pending_works/new
  def new
    @pending_work = PendingWork.new
  end

  # GET /pending_works/1/edit
  def edit
  end

  # POST /pending_works or /pending_works.json
  def create
    @pending_work = PendingWork.new(pending_work_params)

    respond_to do |format|
      if @pending_work.save
        format.html { redirect_to pending_work_url(@pending_work), notice: "Pending work was successfully created." }
        format.json { render :show, status: :created, location: @pending_work }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @pending_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pending_works/1 or /pending_works/1.json
  def update
    respond_to do |format|
      if @pending_work.update(pending_work_params)
        format.html { redirect_to pending_work_url(@pending_work), notice: "Pending work was successfully updated." }
        format.json { render :show, status: :ok, location: @pending_work }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @pending_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pending_works/1 or /pending_works/1.json
  def destroy
    @pending_work.destroy

    respond_to do |format|
      format.html { redirect_to pending_works_url, notice: "Pending work was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pending_work
      @pending_work = PendingWork.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def pending_work_params
      params.require(:pending_work).permit(:work, :work_type, :date, :status, :user_id, :business_id)
    end
end
