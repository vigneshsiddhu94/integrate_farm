class LabourAccountsController < ApplicationController
  before_action :set_labour_account, only: %i[ show edit update destroy ]

  # GET /labour_accounts or /labour_accounts.json
  def index
    @labour_accounts = LabourAccount.all
  end

  # GET /labour_accounts/1 or /labour_accounts/1.json
  def show
  end

  # GET /labour_accounts/new
  def new
    @labour_account = LabourAccount.new
  end

  # GET /labour_accounts/1/edit
  def edit
  end

  # POST /labour_accounts or /labour_accounts.json
  def create
    @labour_account = LabourAccount.new(labour_account_params)

    respond_to do |format|
      if @labour_account.save
        format.html { redirect_to labour_account_url(@labour_account), notice: "Labour account was successfully created." }
        format.json { render :show, status: :created, location: @labour_account }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @labour_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /labour_accounts/1 or /labour_accounts/1.json
  def update
    respond_to do |format|
      if @labour_account.update(labour_account_params)
        format.html { redirect_to labour_account_url(@labour_account), notice: "Labour account was successfully updated." }
        format.json { render :show, status: :ok, location: @labour_account }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @labour_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /labour_accounts/1 or /labour_accounts/1.json
  def destroy
    @labour_account.destroy!

    respond_to do |format|
      format.html { redirect_to labour_accounts_url, notice: "Labour account was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_labour_account
      @labour_account = LabourAccount.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def labour_account_params
      params.require(:labour_account).permit(:amount, :reason, :date, :customer_id, :balance_amount)
    end
end
