class CoconutExpensesController < ApplicationController
  before_action :set_coconut_expense, only: %i[ show edit update destroy ]

  # GET /coconut_expenses or /coconut_expenses.json
  def index
    @coconut_expenses = CoconutExpense.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /coconut_expenses/1 or /coconut_expenses/1.json
  def show
  end

  # GET /coconut_expenses/new
  def new
    @coconut_expense = CoconutExpense.new
  end

  # GET /coconut_expenses/1/edit
  def edit
  end

  # POST /coconut_expenses or /coconut_expenses.json
  def create
    @coconut_expense = CoconutExpense.new(coconut_expense_params)

    respond_to do |format|
      if @coconut_expense.save
        format.html { redirect_to coconut_expense_url(@coconut_expense), notice: "Coconut expense was successfully created." }
        format.json { render :show, status: :created, location: @coconut_expense }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @coconut_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coconut_expenses/1 or /coconut_expenses/1.json
  def update
    respond_to do |format|
      if @coconut_expense.update(coconut_expense_params)
        format.html { redirect_to coconut_expense_url(@coconut_expense), notice: "Coconut expense was successfully updated." }
        format.json { render :show, status: :ok, location: @coconut_expense }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @coconut_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coconut_expenses/1 or /coconut_expenses/1.json
  def destroy
    @coconut_expense.destroy

    respond_to do |format|
      format.html { redirect_to coconut_expenses_url, notice: "Coconut expense was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coconut_expense
      @coconut_expense = CoconutExpense.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def coconut_expense_params
      params.require(:coconut_expense).permit(:reason, :name, :date, :total_amount, :user_id)
    end
end
