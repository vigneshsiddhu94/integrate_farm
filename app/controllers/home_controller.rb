class HomeController < BaseController
	
  def main_page
    if current_user.type_of_business == "Agriculture"
  	  @mother_goats = Goat.live_goat(current_user).where(gender: 2).count
      @buck =  Goat.live_goat(current_user).where(gender: 1).count
      @male_kid_goats =  Goat.live_goat(current_user).where(gender: 3).count
      @female_kid_goats =  Goat.live_goat(current_user).where(gender: 4).count
      @farm_expenses = FarmExpense.monthly_expenses(current_user,Time.zone.now.year)
      
    elsif current_user.type_of_business == "Coconut Procurement Center"
      
      last_month_coconut_rate_purchase_avg = nil
      last_month_coconut_rate_sales_avg = nil
      current_month_coconut_rate_purchase_avg = nil
      current_month_coconut_rate_sales_avg = nil

      current_total_coconut = nil
      current_total_coconut_value = nil
      number_load_coconut_stocked = nil
      @farmer_pending_amount = FarmerBill.where(user_id: current_user.id, paid: false).pluck(:total_amount).sum
      @today_other_expenses = CoconutExpense.where(date: Date.today, user_id: current_user.id).pluck(:total_amount).sum     
      
      
      last_month_start_date = Date.today.prev_month.beginning_of_month
      last_month_end_date = Date.today.prev_month.end_of_month
      invoices_within_date_range = CoconutInvoice.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id)

      @last_month_purchased_coconut_count = PurchaserBill.where(procurement_date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_coconut).sum
      @last_month_purchased_coconut_amount =  FarmerBill.where(procurement_date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_amount).sum
      @last_month_number_of_load_count = CoconutInvoice.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).count
      @last_month_saled_coconut_count = invoices_within_date_range.map { |invoice| invoice.loads.pluck(:coconut_count) }.flatten.sum
      @last_month_coconut_sales_amount = CoconutInvoice.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_amount).sum
      @last_month_salary = Salary.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:salary).sum
      @last_month_other_expenses = CoconutExpense.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_amount).sum
      @last_month_kopparai_sales = Kopparai.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_amount).sum
      @last_month_mattai_sales = Mattai.where(date: last_month_start_date..last_month_end_date, user_id: current_user.id).pluck(:total_amount).sum
      @last_month_profit = (@last_month_coconut_sales_amount - (@last_month_purchased_coconut_amount - @last_month_salary - @last_month_other_expenses))

      current_date = Date.today
      start_date = current_date.beginning_of_month
      end_date = current_date.end_of_month
      invoices_within_date_range = CoconutInvoice.where(date: start_date..end_date, user_id: current_user.id)

      @today_coconut_purchased_count = PurchaserBill.where(procurement_date: Date.today, user_id: current_user.id).pluck(:total_coconut).sum
      @today_salary = Salary.where(date: Date.today, user_id: current_user.id).pluck(:salary).sum
      @current_month_purchased_coconut_count = PurchaserBill.where(procurement_date: start_date..end_date, user_id: current_user.id).pluck(:total_coconut).sum
      @current_month_purchased_coconut_amount =  FarmerBill.where(procurement_date: start_date..end_date, user_id: current_user.id).pluck(:total_amount).sum
      @current_month_number_of_load_count = CoconutInvoice.where(date: start_date..end_date, user_id: current_user.id).count
      @current_month_saled_coconut_count = invoices_within_date_range.map { |invoice| invoice.loads.pluck(:coconut_count) }.flatten.sum
      @current_month_coconut_sales_amount = CoconutInvoice.where(date: start_date..end_date, user_id: current_user.id).pluck(:total_amount).sum
      @current_month_salary = Salary.where(date: start_date..end_date, user_id: current_user.id).pluck(:salary).sum
      @current_month_other_expenses = CoconutExpense.where(date: start_date..end_date, user_id: current_user.id).pluck(:total_amount).sum
      @current_month_kopparai_sales = Kopparai.where(date: start_date..end_date, user_id: current_user.id).pluck(:total_amount).sum
      @current_month_mattai_sales = Mattai.where(date: start_date..end_date, user_id: current_user.id).pluck(:total_amount).sum
      @current_month_profit = (@current_month_coconut_sales_amount - (@current_month_purchased_coconut_amount - @current_month_salary - @current_month_other_expenses))

      
    end

  end

  def year_income
    @total_goats = Goat.live_goat(current_user).count
    @farm_expenses = FarmExpense.rate_monthly_expenses(current_user,Time.zone.now.year,rate_params['amount_sale'])
  end

  private

  def rate_params
		params.permit(:amount_sale)
  end 

end

