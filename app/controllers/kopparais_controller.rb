class KopparaisController < ApplicationController
  before_action :set_kopparai, only: %i[ show edit update destroy ]

  # GET /kopparais or /kopparais.json
  def index
    @kopparais = Kopparai.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /kopparais/1 or /kopparais/1.json
  def show
  end

  # GET /kopparais/new
  def new
    @kopparai = Kopparai.new
  end

  # GET /kopparais/1/edit
  def edit
  end

  # POST /kopparais or /kopparais.json
  def create
    @kopparai = Kopparai.new(kopparai_params)
    respond_to do |format|
      if @kopparai.save
        format.html { redirect_to kopparai_url(@kopparai), notice: "Kopparai was successfully created." }
        format.json { render :show, status: :created, location: @kopparai }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @kopparai.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kopparais/1 or /kopparais/1.json
  def update
    respond_to do |format|
      if @kopparai.update(kopparai_params)
        format.html { redirect_to kopparai_url(@kopparai), notice: "Kopparai was successfully updated." }
        format.json { render :show, status: :ok, location: @kopparai }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @kopparai.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kopparais/1 or /kopparais/1.json
  def destroy
    @kopparai.destroy

    respond_to do |format|
      format.html { redirect_to kopparais_url, notice: "Kopparai was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kopparai
      @kopparai = Kopparai.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def kopparai_params
      params.require(:kopparai).permit(:kg, :rate, :date, :total_amount, :user_id, :quality)
    end
end
