class FertilizersController < BaseController
	def index
		@fertilizer = Fertilizer.all
	end

	def new
		@fertilizer = Fertilizer.new
	end

	def create
		fertilizer = Fertilizer.create(fertilizer_params)
		respond_to do |format|
		  	if fertilizer.save 
		  	 format.html {redirect_to fertilizers_url, flash: { success: 'Fertilizer Created' }}
		  	 format.json { render json: {message: 'Fertilizer Created'}, status: :created }
		  	else
		  	 format.html { redirect_to fertilizers_url, flash: { errors: fertilizer.errors.full_messages.to_sentence} }
			format.json { render json: {errors: fertilizer.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@fertilizer = Fertilizer.find(params[:id])
	end
	def show
		@fertilizer = Fertilizer.find(params[:id])
	end

	def update
		fertilizer = Fertilizer.find(params[:id])
	 	respond_to do |format|
			if fertilizer.update(fertilizer_params)
			# fertilizer.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to fertilizers_url, flash: { success: 'fertilizer Updated Successfully' } }
			format.json { render json: {message: 'fertilizer Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_fertilizer_path(id: params[:id]) , flash: { errors: fertilizer.errors.full_messages.to_sentence} }
			format.json { render json: {errors: fertilizer.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end


	private
	def fertilizer_params
		params.require(:fertilizer).permit(:name, :details)
	end
end
