class VechiclesController < ApplicationController
  before_action :set_vechicle, only: %i[ show edit update destroy ]

  # GET /vechicles or /vechicles.json
  def index
    @vechicles = Vechicle.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /vechicles/1 or /vechicles/1.json
  def show
  end

  # GET /vechicles/new
  def new
    @vechicle = Vechicle.new
  end

  # GET /vechicles/1/edit
  def edit
  end

  # POST /vechicles or /vechicles.json
  def create
    @vechicle = Vechicle.new(vechicle_params)

    respond_to do |format|
      if @vechicle.save
        format.html { redirect_to vechicle_url(@vechicle), notice: "Vechicle was successfully created." }
        format.json { render :show, status: :created, location: @vechicle }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @vechicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vechicles/1 or /vechicles/1.json
  def update
    respond_to do |format|
      if @vechicle.update(vechicle_params)
        format.html { redirect_to vechicle_url(@vechicle), notice: "Vechicle was successfully updated." }
        format.json { render :show, status: :ok, location: @vechicle }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @vechicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vechicles/1 or /vechicles/1.json
  def destroy
    @vechicle.destroy

    respond_to do |format|
      format.html { redirect_to vechicles_url, notice: "Vechicle was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vechicle
      @vechicle = Vechicle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def vechicle_params
      params.require(:vechicle).permit(:name, :capacity, :is_activate, :user_id)
    end
end
