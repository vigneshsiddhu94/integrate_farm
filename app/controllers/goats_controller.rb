class GoatsController < BaseController

	def index
    	@goats = Goat.live_goat(current_user)
    	invesment_boyer = 0
    	# @total_investment = PurchaseGoat.total_investment
    	@male_thala = Goat.male_thalachery(current_user)
    	@female_thala = Goat.female_thalachery(current_user)
    	@boyer = Goat.male_boyer(current_user)
    	@kid_female = Goat.kid_female_thalachery(current_user)
    	@kid_male = Goat.kid_male_thalachery(current_user)
    	@female_pregnancy =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Confirmed', 'Calve Stage'], live:'Yes').count
		@early_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: 'Pregnancy', live:'Yes').count
		@female_non_pregnancy =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Heat', 'Non Pregnancy', 'Abort'], live:'Yes').count
		@milking_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Milking'], live:'Yes').count
		@doubt_stage =  GoatCalvingPeriod.joins(:goat).where("goats.user_id = ? and goats.live =?", current_user.id, 1).where(goat_status: ['Doubt'], live:'Yes').count
		@kid = (@kid_female.count) + (@kid_male.count)
		@matured_male_kid = Goat.matured_male_kid(current_user).count
		@matured_female_kid = Goat.matured_female_kid(current_user).count
  	end 

	def new
		@goat = Goat.new
		@goat.goat_calving_periods.build
		# @goat.rack_goats.build
	end

	def create
		goat = Goat.new(goat_params)
		  respond_to do |format|
			if goat.save
			format.html {redirect_to goats_url, flash: { success: 'Goat Successfully Added ' } }
			format.json { render json: {message: 'Goat Successfully Added'}, status: :created }
			else
			format.html { redirect_to new_goat_path , flash: { errors: goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		  end
	end

	def edit
		@goat = Goat.find_by(id: params[:id], user_id: current_user.id)
		@goat_calving_periods = @goat.goat_calving_periods.last
	end

	def update
		goat = Goat.find_by(id: params[:id], user_id: current_user.id)
	 	respond_to do |format|
			if goat.update(goat_params)
			format.html {redirect_to goats_url, flash: { success: 'Goat Updated Successfully' } }
			format.json { render json: {message: 'Goat Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_goat_path(id: params[:id]) , flash: { errors: goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@goat = Goat.find(params[:id])
		if @goat.present?
			@goat.children_parent_goats.destroy_all
			@goat.parent_goat&.destroy
			@goat.goat_calving_periods.destroy_all
			@goat.insurance_goats.destroy_all
			@goat.daily_works.destroy_all
			@goat.female_mat_goats.destroy_all
			@goat.male_mat_goats.destroy_all
			@goat.disease_goats.destroy_all
			@goat.death_goat&.destroy
			@goat.purchase_goat&.destroy
			@goat.goat_sale&.destroy
			@goat.batch_goat&.destroy
			@goat.goat_weights.destroy_all
		end
		respond_to do |format|
			if @goat.destroy
			flash[:notice] = "Goat was successfully deleted."
			format.html {redirect_to goats_url, flash: { success: 'Goat Deleted Successfully' } }
			format.json { render json: {message: 'Goat Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_goat_path(id: params[:id]) , flash: { errors: goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			#   flash[:alert] = "Failed to delete the goat."
			#   redirect_to goat_path(@goat) # Redirect back to the goat's show page
			end
		end
	  end

	def farm_status
		if params['status'] == 'confirmed_pregnancy'
		  @confirmed =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Confirmed', live:'Yes').order('created_at DESC')
		  @mid_stage =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Mid Stage', live:'Yes').order('created_at DESC')
		  @calve_stage =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Calve Stage', live:'Yes').order('created_at DESC')
		end
		if params['status'] == 'early_pregnancy'
		  @pregnancy =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Pregnancy', live:'Yes').order('created_at DESC')
		end
		if params['status'] == 'doubt'
		  @doubt =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Doubt', live:'Yes').order('created_at DESC')
		end
		if params['status'] == 'non_pregnancy'
		  @non_pregnancy =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Non Pregnancy', live:'Yes').order('created_at DESC')
		  @abort =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Abort', live:'Yes').order('created_at DESC')
		end
		if params['status'] == 'milking'
		  @milking =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Milking', live:'Yes').order('created_at DESC')
		  @heat =  GoatCalvingPeriod.joins(:goat).where(goats: {user_id: current_user.id, live: 1}).where(goat_status: 'Heat', live:'Yes').order('created_at DESC')

		end
		if params['status'] == 'male_kid'
			@kid_all= Goat.where(breed_variety: 1, gender: 3, live: 1, user_id: current_user.id).order('date_of_birth DESC')
		end
		if params['status'] == 'female_kid'
			@kid_all= Goat.where(breed_variety: 1, gender: 4, live: 1, user_id: current_user.id).order('date_of_birth DESC')
		end
	end

	def find_goat
		@goat = Goat.find_by(token_no: params[:token_no],user_id: current_user.id)
		if @goat.present?
			@goat_mates = @goat.female_mat_goats.order('created_at DESC') 
			@goat_status = @goat.goat_calving_periods.order('created_at DESC')
			@goat_parent = @goat.parent_goat
			@goat_childrens = @goat.children_parent_goats.order('created_at DESC')
			@goat_disease = @goat.disease_goats.order('created_at DESC')
			@goat_work = @goat.daily_works.order('created_at DESC')
		end
	end

	def goat_status
		@goat_status = GoatCalvingPeriod.new
		@female_goat = Goat.female_thalachery(current_user)
	end

	def create_status
	goat_health = GoatCalvingPeriod.new(goat_calving_period_params)
	respond_to do |format|
		if goat_health.save
		format.html {redirect_to goats_path, flash: { success: 'Goat Status Added ' } }
		format.json { render json: {message: 'Goat Status Added'}, status: :created }
		else
		format.html { redirect_to goat_status_path , flash: { errors: goat_health.errors.full_messages.to_sentence} }
		format.json { render json: {errors: goat_health.errors.full_messages.to_sentence}, status: :unprocessable_entity }
		end
		end
	end

	def goat_insurance
		@insurance_goat = InsuranceGoat.new
		@goat_insurance = InsuranceGoat.all
	end

	def create_insurance
		insurance = InsuranceGoat.new(goat_insurance_params)
	respond_to do |format|
		if insurance.save
		format.html {redirect_to goat_insurance_path, flash: { success: 'Insurance Created ' } }
		format.json { render json: {message: 'Insurance Created'}, status: :created }
		else
		format.html { redirect_to goat_insurance_path , flash: { errors: insurance.errors.full_messages.to_sentence} }
		format.json { render json: {errors: insurance.errors.full_messages.to_sentence}, status: :unprocessable_entity }
		end
		end
	end

	def create_mat
		mat = MatGoat.new(goat_mat_params)
		respond_to do |format|
			if mat.save
			format.html {redirect_to goat_mat_path, flash: { success: 'Mate With Details Stored  ' } }
			format.json { render json: {message: 'Mate With Details Stored'}, status: :created }
			else
			format.html { redirect_to goat_mat_path , flash: { errors: mat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
		end
		end
	end

	# def get_goat_data
	# 	pdf = Prawn::Document.new
	# 	parsed_date = Date.today
	# 	month_name = parsed_date.strftime("%B")
	# 	result = Goat.today_goat_status(current_user)
	# 	# Prepare the main data table
	# 	table_data = [["Token No", "Status", "Kid Count", "Date", "Days"]] # Headers
	# 	result.each do |category, details|
	# 		details[:data].each do |data|
	# 			table_data << [
	# 				data[:token_no],
	# 				data[:status],
	# 				data[:kid_count] || "-", # Handle nil kid_count for non-milking statuses
	# 				data[:date].strftime('%Y-%m-%d'),
	# 				data[:days]
	# 			]
	# 		end
	# 	end
	# 	status_counts = result.flat_map { |_, details| details[:data] }
	# 												.group_by { |data| data[:status] }
	# 												.transform_values(&:size)
	# 	summary_data = [["Status", "Count"]] # Headers
	# 	status_counts.each do |status, count|
	# 		summary_data << [status, count]
	# 	end

	# 	desc_result = Goat.number_wise_today_goat_status_by_batch(current_user)
	# 	table_data_1 = [["Token No", "Status"]] # Headers
	# 	desc_result.each do |details|
	# 		details[:data].each do |data|
	# 			table_data_1 << [
	# 				data[:token_no],
	# 				data[:status]
	# 			]
	# 		end
	# 	end

	# 	# Add tables to PDF
	# 	pdf.move_down(20)
	# 	pdf.text "Today Goat Data: #{month_name}", align: :left, style: :bold, color: "008000"
	# 	pdf.move_down(5)
	# 	pdf.table(table_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
	# 		row(0).font_style = :bold
	# 		columns(1..-1).align = :center
	# 	end

	# 	pdf.move_down(20)
	# 	pdf.text "Count Status Summary: #{month_name}", align: :left, style: :bold, color: "008000"
	# 	pdf.move_down(5)
	# 	pdf.table(table_data_1, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
	# 		row(0).font_style = :bold
	# 		columns(1).align = :center
	# 	end

	# 	send_data pdf.render, filename: "today_goat_data.pdf", type: "application/pdf", disposition: "inline"
	#   # respond_to do |format|
	# 	# format.html
	# 	# format.pdf do
	# 	#   render pdf: "Goat Month Data #{Date.today}",
	# 	# 		 template: "pdf/today_data",
	# 	# 		 page_size: 'A4',
	# 	# 		 locals: { result: result},
	# 	# 		 formats: [:html],
	# 	# 		 stylesheets: ["pdf"],
	# 	# 		 disposition: :inline,
	# 	# 		 layout: 'pdf'
	# 	# end
	#   # end
	# end

	def get_goat_data
		pdf = Prawn::Document.new
		parsed_date = Date.today
		month_name = parsed_date.strftime("%B")

		# Fetch the data
		result = Goat.today_goat_status(current_user)
		desc_result = Goat.number_wise_today_goat_status_by_batch(current_user)

		# Prepare the main data table
		table_data = prepare_table_data(result)
		status_summary_data = prepare_status_summary(result)
		table_data_1 = prepare_desc_table_data(desc_result)

		# Add tables to PDF
		add_main_table_to_pdf(pdf, table_data, month_name)
		add_summary_table_to_pdf(pdf, status_summary_data, month_name)
		add_desc_table_to_pdf(pdf, table_data_1, month_name)

		send_data pdf.render, filename: "today_goat_data.pdf", type: "application/pdf", disposition: "inline"
	end

	private

	def prepare_table_data(result)
		table_data = [["Token No", "Status", "Kid Count", "Date", "Days"]] # Headers
		result.each do |_, details|
			details[:data].each do |data|
				table_data << [
					data[:token_no],
					data[:status],
					data[:kid_count] || "-", # Handle nil kid_count for non-milking statuses
					data[:date].strftime('%Y-%m-%d'),
					data[:days]
				]
			end
		end
		table_data
	end

	def prepare_status_summary(result)
		status_counts = result
			.flat_map { |_, details| details[:data] }
			.group_by { |data| data[:status] }
			.transform_values(&:size)

		summary_data = [["Status", "Count"]] # Headers
		status_counts.each do |status, count|
			summary_data << [status, count]
		end
		summary_data
	end

	def prepare_desc_table_data(desc_result)
		table_data = [["Token No", "Status"]] # Headers
		desc_result.each do |details|
				table_data << [
					details[:token_no],
					details[:status]
				]
		end
		table_data
	end

	def add_main_table_to_pdf(pdf, table_data, month_name)
		pdf.move_down(20)
		pdf.text "Today Goat Data: #{month_name}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(table_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1..-1).align = :center
		end
	end

	def add_summary_table_to_pdf(pdf, summary_data, month_name)
		pdf.move_down(20)
		pdf.text "Count Status Summary: #{month_name}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(summary_data, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
		row(0).font_style = :bold
		columns(1).align = :center
	  end
	end

	def add_desc_table_to_pdf(pdf, table_data_1, month_name)
		pdf.move_down(20)
		pdf.text "Detailed Goat Status: #{month_name}", align: :left, style: :bold, color: "008000"
		pdf.move_down(5)
		pdf.table(table_data_1, header: true, row_colors: ["DDDDDD", "FFFFFF"], cell_style: { border_color: '000000', border_style: :solid, border_width: 1, size: 10, padding: 5 }) do
			row(0).font_style = :bold
			columns(1).align = :center
		end
	end


	def get_hole_goat_data
		result = Goat.hole_goat_status(current_user)
		respond_to do |format|
		  format.html
		  format.pdf do
			render pdf: "Goat Hole Data #{Date.today}",
				   template: "pdf/all_data",
				   page_size: 'A4',
				   locals: { result: result},
				   formats: [:html],
				   disposition: :inline,
				   layout: 'pdf',
				   stylesheets: ["pdf"]
		  end
		end
	end
	def goat_kid_data
		result = Goat.goat_kidded_count(current_user)
		respond_to do |format|
		  format.html
		  format.pdf do
			render pdf: "Goat Kid Data #{Date.today}",
				   template: "pdf/kid_data",
				   page_size: 'A4',
				   locals: { result: result},
				   formats: [:html],
				   disposition: :inline,
				   layout: 'pdf'
		  end
		end
	end

	def kid_weight_data
		result = Goat.kid_weight(current_user)
		respond_to do |format|
		  format.html
		  format.pdf do
			render pdf: "Weight Data #{Date.today}",
				   template: "pdf/kid_weight_data",
				   page_size: 'A4',
				   locals: { result: result},
				   formats: [:html],
				   disposition: :inline,
				   layout: 'pdf'
		  end
		end
	end


	private

	def goat_params
		params.require(:goat).permit(:user_id,:token_no, :date_of_birth, :gender, :breed_variety, :age, :weight)
		# ,  purchase_goat_attributes: [:id,:date_of_purchase, :rate, :place], goat_calving_periods_attributes: [:id,:goat_status], rack_goats_attributes: [:id, :rack_detail_id])
	end

	def goat_calving_period_params
		params.require(:goat_calving_period).permit(:goat_id, :goat_status, :date, :message)
	end 

	def goat_insurance_params
		params.require(:insurance_goat).permit(:goat_id, :policy_no, :start_date, :exp_date, :amount)
	end 
	
end
