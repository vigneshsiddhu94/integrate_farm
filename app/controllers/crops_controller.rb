class CropsController < BaseController

	def index
		@crop = Crop.all
	end

	def new
		@crop = Crop.new
	end

	def create
		crop = Crop.create(crop_params)
		respond_to do |format|
		  	if crop.save 
		  	 format.html {redirect_to crops_url, flash: { success: 'Crop Created Successfully' }}
		  	 format.json { render json: {message: 'Crop Created Successfully'}, status: :created }
		  	else
		  	 format.html { redirect_to crops_url, flash: { errors: crop.errors.full_messages.to_sentence} }
			format.json { render json: {errors: crop.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@crop = Crop.find(params[:id])
	end
	def show
		@crop = Crop.find(params[:id])
	end

	def update
		crop = Crop.find(params[:id])
	 	respond_to do |format|
			if crop.update(crop_params)
			# crop.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to crops_url, flash: { success: 'Crop Updated Successfully' } }
			format.json { render json: {message: 'Crop Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_crop_path(id: params[:id]) , flash: { errors: crop.errors.full_messages.to_sentence} }
			format.json { render json: {errors: crop.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def land_crop
		@land_crop = LandCrop.new
		@land_crops = LandCrop.all.order(:status)
	end

	def land_crop_create
		land_crop = LandCrop.create(land_crop_params)
		respond_to do |format|
		  	if land_crop.save
		  	 format.html {redirect_to land_crop_url, flash: { success: 'Land Connected With Crop' }}
		  	 format.json { render json: {message: 'Land Connected With Crop'}, status: :created }
		  	else
		  	 format.html { redirect_to land_crop_url, flash: { errors: land_crop.errors.full_messages.to_sentence} }
			format.json { render json: {errors: land_crop.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end


	private
	def crop_params
		params.require(:crop).permit(:name, :details, :yield, :varieties , :nutrition, :period, :seed_acre, :intercrop, :water_requirement, :sowing_distance, :season, :business_id)
	end
	def land_crop_params
		params.require(:land_crop).permit(:land_id, :crop_id, :date,:fodder, :status, :business_id)
	end

end

