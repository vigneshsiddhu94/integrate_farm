class CoconutInvoicesController < ApplicationController
  before_action :set_coconut_invoice, only: %i[ show edit update destroy ]

  # GET /coconut_invoices or /coconut_invoices.json
  def index
    @coconut_invoices = CoconutInvoice.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
    @customer = Customer.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /coconut_invoices/1 or /coconut_invoices/1.json
  def show
  end

  # GET /coconut_invoices/new
  def new
    @coconut_invoice = CoconutInvoice.new
    @customer = Customer.where(user_id: current_user.id)
  end

  # GET /coconut_invoices/1/edit
  def edit
    @customer = Customer.where(user_id: current_user.id)
  end

  # POST /coconut_invoices or /coconut_invoices.json
  def create
    @coconut_invoice = CoconutInvoice.new(coconut_invoice_params)

    respond_to do |format|
      if @coconut_invoice.save
        format.html { redirect_to coconut_invoice_url(@coconut_invoice), notice: "Coconut invoice was successfully created." }
        format.json { render :show, status: :created, location: @coconut_invoice }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @coconut_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coconut_invoices/1 or /coconut_invoices/1.json
  def update
    respond_to do |format|
      if @coconut_invoice.update(coconut_invoice_params)
        format.html { redirect_to coconut_invoice_url(@coconut_invoice), notice: "Coconut invoice was successfully updated." }
        format.json { render :show, status: :ok, location: @coconut_invoice }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @coconut_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coconut_invoices/1 or /coconut_invoices/1.json
  def destroy
    @coconut_invoice.destroy

    respond_to do |format|
      format.html { redirect_to coconut_invoices_url, notice: "Coconut invoice was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coconut_invoice
      @coconut_invoice = CoconutInvoice.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def coconut_invoice_params
      params.require(:coconut_invoice).permit(:lorry, :total_amount, :date, :user_id, :load_id, :customer_id, loads_attributes: [:id,:quality, :paruthi, :sakku, :rate, :coconut_count, :total_amount,:_destroy])
    end
end
