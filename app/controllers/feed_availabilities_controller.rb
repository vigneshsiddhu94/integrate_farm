class FeedAvailabilitiesController < ApplicationController
  before_action :set_feed_availability, only: %i[ show edit update destroy ]

  # GET /feed_availabilities or /feed_availabilities.json
  def index
    @feed_availabilities = FeedAvailability.all
  end

  # GET /feed_availabilities/1 or /feed_availabilities/1.json
  def show
  end

  # GET /feed_availabilities/new
  def new
    @feed_availability = FeedAvailability.new
  end

  # GET /feed_availabilities/1/edit
  def edit
  end

  # POST /feed_availabilities or /feed_availabilities.json
  def create
    @feed_availability = FeedAvailability.new(feed_availability_params)

    respond_to do |format|
      if @feed_availability.save
        format.html { redirect_to feed_availability_url(@feed_availability), notice: "Feed availability was successfully created." }
        format.json { render :show, status: :created, location: @feed_availability }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @feed_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /feed_availabilities/1 or /feed_availabilities/1.json
  def update
    respond_to do |format|
    if @feed_availability.update(feed_availability_params)
        format.html { redirect_to feed_availability_url(@feed_availability), notice: "Feed availability was successfully updated." }
        format.json { render :show, status: :ok, location: @feed_availability }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @feed_availability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /feed_availabilities/1 or /feed_availabilities/1.json
  def destroy
    @feed_availability.destroy!

    respond_to do |format|
      format.html { redirect_to feed_availabilities_url, notice: "Feed availability was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feed_availability
      @feed_availability = FeedAvailability.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def feed_availability_params
      params.require(:feed_availability).permit(:date, :fodder_variety, :stored_kg, :rate, :total_amount, :current_kg, :business_id, :user_id, :status)
    end
end
