class InvoicesController < BaseController

	def index
		@invoice = Invoice.invoice_all(current_user)
		@year = Invoice.select_uniq_year(current_user)
		@male_kid_amount = GoatSale.saled_kid_male(current_user).pluck(:amount).inject{ |sum,n| sum+n } if GoatSale.saled_kid_male(current_user).present?
		@female_kid_amount = GoatSale.saled_kid_female(current_user).pluck(:amount).inject{ |sum,n| sum+n } if GoatSale.saled_kid_female(current_user).present?
		@female_amount = GoatSale.Saled_female(current_user).pluck(:amount).inject{ |sum,n| sum+n } if GoatSale.Saled_female(current_user).present?
		@invoice_total_amount = Invoice.invoice_all(current_user).pluck(:total_amount).inject{ |sum,n| sum+n } if Invoice.invoice_all(current_user).present?
		@total_Expeses =  FarmExpense.farm_expenses_all(current_user).pluck(:amount).inject{ |sum,n| sum+n } if FarmExpense.all.present?
	end

    def invoice_year
        @invoice = Invoice.where('extract(year  from sale_date::date) = ? and user_id = ?', params['year'], current_user.id)
    end

	def new
		@invoice = Invoice.new
		@invoice.goat_sales.build
	end

	def create
		invoice = Invoice.create(invoice_params)
		respond_to do |format|
		  	if invoice.save 
		  	 format.html {redirect_to invoices_url, flash: { success: 'Bill generated' }}
		  	 format.json { render json: {message: 'Bill generated'}, status: :created }
		  	else
		  	 format.html { redirect_to invoices_url, flash: { errors: invoice.errors.full_messages.to_sentence} }
			format.json { render json: {errors: invoice.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@invoice = Invoice.find(params[:id])
		@goat_sale = @invoice.goat_sales
	end
	def show
		@invoice = Invoice.find(params[:id])
	end

	def update
		invoice = Invoice.find(params[:id])
	 	respond_to do |format|
			if invoice.update(invoice_params)
			# invoice.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to invoices_url, flash: { success: 'invoice Updated Successfully' } }
			format.json { render json: {message: 'invoice Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_invoice_path(id: params[:id]) , flash: { errors: invoice.errors.full_messages.to_sentence} }
			format.json { render json: {errors: invoice.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	
	private
	def invoice_params
		params.require(:invoice).permit(:user_id, :total_amount,:total_weight, :discount, :sale_date, goat_sales_attributes: [:id,:goat_id, :weight, :amount, :purpose_of_sale])
	end
end
