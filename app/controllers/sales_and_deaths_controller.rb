class SalesAndDeathsController < ApplicationController
  before_action :set_sales_and_death, only: %i[ show edit update destroy ]

  # GET /sales_and_deaths or /sales_and_deaths.json
  def index
    @sales_and_deaths = SalesAndDeath.all
  end

  # GET /sales_and_deaths/1 or /sales_and_deaths/1.json
  def show
  end

  # GET /sales_and_deaths/new
  def new
    @sales_and_death = SalesAndDeath.new
  end

  # GET /sales_and_deaths/1/edit
  def edit
  end

  # POST /sales_and_deaths or /sales_and_deaths.json
  def create
    @sales_and_death = SalesAndDeath.new(sales_and_death_params)

    respond_to do |format|
      if @sales_and_death.save
        format.html { redirect_to sales_and_death_url(@sales_and_death), notice: "Sales and death was successfully created." }
        format.json { render :show, status: :created, location: @sales_and_death }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @sales_and_death.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_and_deaths/1 or /sales_and_deaths/1.json
  def update
    respond_to do |format|
      if @sales_and_death.update(sales_and_death_params)
        format.html { redirect_to sales_and_death_url(@sales_and_death), notice: "Sales and death was successfully updated." }
        format.json { render :show, status: :ok, location: @sales_and_death }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @sales_and_death.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_and_deaths/1 or /sales_and_deaths/1.json
  def destroy
    @sales_and_death.destroy!

    respond_to do |format|
      format.html { redirect_to sales_and_deaths_url, notice: "Sales and death was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_and_death
      @sales_and_death = SalesAndDeath.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sales_and_death_params
      params.require(:sales_and_death).permit(:male_count, :action_type, :goat_type, :female_count, :date, :business_id, :user_id, :rate, :total_weight, :total_amount)
    end
end
