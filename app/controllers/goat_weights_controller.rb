class GoatWeightsController < BaseController

	def index
    	@goat_weights = GoatWeight.goat_weight_all(current_user).order('created_at DESC')
  	end 

  	def batch_weight
  		@goat_weights = GoatWeight.goat_weight_all(current_user).order('created_at DESC')
  	end


	def new
		@goat_weight = GoatWeight.new
	end

	def create
  		goat_weight = GoatWeight.new(goat_weight_params)
		respond_to do |format|
			if goat_weight.save
			format.html  {redirect_to goat_weights_path, flash: { success: 'Goat Weight Stored  ' } }
			format.json { render json: {message: 'Goat Weight Stored'}, status: :created }
			else
			format.html { redirect_to new_goat_weight_path , flash: { errors: goat_weight.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def create_batch_weight
  		goat_weights = GoatWeight.batch_weight_create(params[:batch_id],params[:weight],params[:date])
  		respond_to do |format|
			if goat_weights.blank?
			format.html { redirect_to batch_weight_path(id: params[:id]) , flash: { errors: "Batch Doesnot Have Batch Goat"} }
			format.json { render json: {errors: batch_weight.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			else
			format.html {redirect_to batches_path, flash: { success: 'Goat All In Batch Weights Are Updated Successfully' } }
			format.json { render json: {message: 'Goat All In Batch Weights Are Updated Successfully'}, status: :created }
			end
		end
  	end

  	def edit
  		@goat_weight = GoatWeight.find(params[:id])
  	end

  	def update
  		goat_weight = GoatWeight.find(params[:id])
	 	respond_to do |format|
			if goat_weight.update(goat_weight_params)
			format.html {redirect_to goat_weights_path, flash: { success: 'Goat Weight Updated Successfully' } }
			format.json { render json: {message: 'Goat Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_goat_weight_path(id: params[:id]) , flash: { errors: goat_weight.errors.full_messages.to_sentence} }
			format.json { render json: {errors: goat_weight.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @goat_weight = GoatWeight.find(params[:id])
	  respond_to do |format|
			if @goat_weight.destroy
			format.html {redirect_to goat_weights_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_goat_weight_path(id: params[:id]) , flash: { errors: goat_weight.errors.full_messages.to_sentence} }
			format.json { render json: {errors: goat_weight.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

  	
  	private

  	def goat_weight_params
		params.require(:goat_weight).permit(:goat_id, :weight, :date)
	end 

	def batch_weight_params
		params.permit(:batch_id, :weight, :date)
	end 

end
