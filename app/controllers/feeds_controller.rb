class FeedsController < BaseController
	before_action :set_feed_usage, only: %i[ show edit update destroy ]
	rescue_from StandardError, with: :handle_feed_stock_error
	def index
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		@feeds = Feed.where(business_id: business_id).order('created_at DESC')
	end	

	def new
		@feed = Feed.new
	end

	# GET /pending_works/1/edit
	def edit
	end
	# PATCH/PUT /pending_works/1 or /pending_works/1.json
	def update
		respond_to do |format|
		if @feed.update(feed_params)
			format.html { redirect_to feeds_url(@feed), notice: "Feed Usage was successfully updated." }
			format.json { render :show, status: :ok, location: @feed }
		else
			format.html { render :edit, status: :unprocessable_entity }
			format.json { render json: @feed.errors, status: :unprocessable_entity }
		end
		end
	end

	# DELETE /feeds/1 or /feeds/1.json
	def destroy
		@feed.destroy
		respond_to do |format|
			format.html { redirect_to feeds_url, notice: "Feed Usage was successfully destroyed." }
			format.json { head :no_content }
		end
	end

	def create
		@feed = Feed.new(feed_params)
		respond_to do |format|
			if @feed.save
				format.html { redirect_to feeds_url(@month_on_weight), notice: "Month on weight was successfully created." }
				format.json { render :show, status: :created, location: @feed }
			else
			format.html { render :new, status: :unprocessable_entity }
			format.json { render json: @feed.errors, status: :unprocessable_entity }
			end
		end
	end

	private
	
    def set_feed_usage
		@feed = Feed.find(params[:id])
    end
	
	def feed_params
		params.require(:feed).permit(:date, :animal, :dry_fodder, :green_fodder, :dry_fodder_variety, :green_fodder_variety, :concentrate, :user_id, :business_id)
	end

	def handle_feed_stock_error(exception)
		redirect_back(fallback_location: root_path, alert: exception.message)
	end
end