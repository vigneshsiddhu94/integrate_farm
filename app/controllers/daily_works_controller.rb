class DailyWorksController < BaseController

	def good_morning
		#Goat Status Changes
		@pregnancy = [] 
		@confirmed = []
		@mid_stage = []
		@calve_stage = []
		@milking = []
		@heat = []
		@non_pregnancy = []
		@abort = []
		@pregnancy_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Pregnancy', live:'Yes').order('created_at DESC')
		@pregnancy_goat.each do |goat_status|
			if ( Date.today - (goat_status.date).to_date).to_i >= 70
				@pregnancy += GoatCalvingPeriod.where(id: goat_status.id)
			end
		end
		@confirmed_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Confirmed', live:'Yes').order('created_at DESC')
		@confirmed_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 65
					@confirmed += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		@calve_stage_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Calve Stage', live:'Yes').order('created_at DESC')
		@calve_stage_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 15
					@calve_stage += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		@milking_goat = GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Milking', live:'Yes').order('created_at DESC')
		@milking_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 90
					@milking += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		@heat_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Heat', live:'Yes').order('created_at DESC')
		@heat_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 25
					@heat += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		@non_pregnancy_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Non Pregnancy', live:'Yes').order('created_at DESC')
		@non_pregnancy_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 20
					@non_pregnancy += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		@abort_goat =  GoatCalvingPeriod.joins(:goat).where("goats.live =?", 1).where(goat_status: 'Abort', live:'Yes').order('created_at DESC')
		@abort_goat.each do |goat_status|
				if ( Date.today - (goat_status.date).to_date).to_i >= 21
					@abort += GoatCalvingPeriod.where(id: goat_status.id)
				end
			end
		#Disease Work
		@icu_goats = []
		start_date = Date.today - 15 
  		goats =  DiseaseGoat.where(:disease_date => start_date..Date.today).select(:goat_id).group( :goat_id).having("count(*) > 1").pluck(:goat_id)
  		goats.each do |goat_id|
  			@icu_goats += DiseaseGoat.where(goat_id: goat_id, :disease_date => start_date..Date.today)
  		end
  		# Daily Work list
		@daily_works = DailyWork.where(status: 'Not Yet').order('created_at DESC')
	end

	def date_work
		# byebug
		# work_date = Date.parse(params[:date_work]) if params[:date_work].present?
		work_date = Date.strptime(params[:date_work], '%m/%d/%y') if params[:date_work].present?.present?
		today_work = DailyWork.today_what(work_date) if work_date.present?
		@daily_works = DailyWork.where(status: 'Not Yet').order('created_at DESC')
	end
  	
  	def index
		@daily_works = DailyWork.all.order('created_at DESC')
  	end

  	def new
  		@daily_work = DailyWork.new
  	end

  	def create
  		daily_work = DailyWork.new(daily_work_params)
		respond_to do |format|
			if daily_work.save
			format.html {redirect_to daily_works_path, flash: { success: 'Daily Work Created' } }
			format.json { render json: {message: 'Daily Work Created'}, status: :created }
			else
			format.html { redirect_to daily_works_path , flash: { errors: daily_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		@daily_work = DailyWork.find(params[:id])
  	end

  	def update
  		daily_work = DailyWork.find(params[:id])
	 	respond_to do |format|
			if daily_work.update_attributes(daily_work_params)
			format.html {redirect_to good_morning_path, flash: { success: 'Work Updated Successfully' } }
			format.json { render json: {message: 'Work Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_daily_work_path(id: params[:id]) , flash: { errors: daily_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: daily_work.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @daily_work = DailyWork.find(params[:id])
	  respond_to do |format|
			if @daily_work.destroy
			format.html {redirect_to daily_works_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_daily_work_path(id: params[:id]) , flash: { errors: daily_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: daily_work.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

  	private

  	def daily_work_params
		params.require(:daily_work).permit(:goat_id, :main_work_id, :comment, :status)
	end



end