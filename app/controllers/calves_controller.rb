class CalvesController < BaseController

	def new
		@calves = Calve.new
		@cow = Cow.all.order('created_at DESC')
	end 

	def create
		@calve = Calve.new(calve_params)
  	  if @calve.save
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to home_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to home_url
	    end
	end

	def calve_params
		params.require(:calve).permit(:cow_id,:token_no,:date_of_birth,:breed_variety,:gender)
	end
end
