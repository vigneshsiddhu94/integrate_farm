class BatchesController < BaseController

	def index
    	@batches = Batch.batches_all(current_user).order('created_at DESC')    	
  	end 

	def new
		@batch = Batch.new
		@batch.batch_goats.build	
	end

	def create
  		batch = Batch.new(batch_params)
		respond_to do |format|
			if batch.save
			format.html  {redirect_to batches_path, flash: { success: 'Batch Stored  ' } }
			format.json { render json: {message: 'Batch Stored'}, status: :created }
			else
			format.html { redirect_to new_batch_path , flash: { errors: batch.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		@batch = Batch.find(params[:id])
  	end

  	def update
  		batch = Batch.find(params[:id])
	 	respond_to do |format|
			if batch.update(batch_params)
			format.html {redirect_to batches_path, flash: { success: 'Batch Updated Successfully' } }
			format.json { render json: {message: 'Batch Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_batch_path(id: params[:id]) , flash: { errors: batch.errors.full_messages.to_sentence} }
			format.json { render json: {errors: batch.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

	def goat_data_batch_wise
		result = Goat.today_goat_status_by_batch(current_user)
		respond_to do |format|
			format.html
			format.pdf do
			render pdf: "Goat Month Data By Batch Wise #{Date.today}",
					template: "pdf/today_data_batch_wise",
					page_size: 'A4',
					locals: { result: result},
					formats: [:html],
					disposition: :inline,
					layout: 'pdf'
			end
		end
	end

  	def destroy
	  @batch = Batch.find(params[:id])
	  respond_to do |format|
			if @batch.destroy
			format.html {redirect_to batches_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_batch_path(id: params[:id]) , flash: { errors: batch.errors.full_messages.to_sentence} }
			format.json { render json: {errors: batch.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

	private
 	def batch_params
		params.require(:batch).permit(:user_id,:rack_detail_id,:name,:no, :capacity, batch_goats_attributes: [:id, :goat_id, :status, :_destroy])
	end
end
