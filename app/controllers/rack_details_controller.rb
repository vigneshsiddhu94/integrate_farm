class RackDetailsController < BaseController
	
	def index
    	@rack_details = RackDetail.rack_details_all(current_user).order('created_at DESC')    	
  	end 

	def new
		@rack_detail = RackDetail.new
	end

	def create
  		rack_detail = RackDetail.new(rack_detail_params)
		respond_to do |format|
			if rack_detail.save
			format.html  {redirect_to rack_details_path, flash: { success: 'Rack Stored  ' } }
			format.json { render json: {message: 'Rack Stored'}, status: :created }
			else
			format.html { redirect_to new_rack_detail_path , flash: { errors: rack_detail.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		@rack_detail = RackDetail.find(params[:id])
  	end

  	def update
  		rack_detail = RackDetail.find(params[:id])
	 	respond_to do |format|
			if rack_detail.update(rack_detail_params)
			format.html {redirect_to rack_details_path, flash: { success: 'Rack Updated Successfully' } }
			format.json { render json: {message: 'Rack Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_rack_detail_path(id: params[:id]) , flash: { errors: rack_detail.errors.full_messages.to_sentence} }
			format.json { render json: {errors: rack_detail.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @rack_detail = RackDetail.find(params[:id])
	  respond_to do |format|
			if @rack_detail.destroy
			format.html {redirect_to rack_details_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_rack_detail_path(id: params[:id]) , flash: { errors: rack_detail.errors.full_messages.to_sentence} }
			format.json { render json: {errors: rack_detail.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

	private
 	def rack_detail_params
		params.require(:rack_detail).permit(:user_id,:name, :capacity)
	end

end
