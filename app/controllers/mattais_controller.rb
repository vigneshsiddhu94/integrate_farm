class MattaisController < ApplicationController
  before_action :set_mattai, only: %i[ show edit update destroy ]

  # GET /mattais or /mattais.json
  def index
    @mattais = Mattai.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /mattais/1 or /mattais/1.json
  def show
  end

  # GET /mattais/new
  def new
    @mattai = Mattai.new
  end

  # GET /mattais/1/edit
  def edit
  end

  # POST /mattais or /mattais.json
  def create
    @mattai = Mattai.new(mattai_params)

    respond_to do |format|
      if @mattai.save
        format.html { redirect_to mattai_url(@mattai), notice: "Mattai was successfully created." }
        format.json { render :show, status: :created, location: @mattai }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @mattai.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mattais/1 or /mattais/1.json
  def update
    respond_to do |format|
      if @mattai.update(mattai_params)
        format.html { redirect_to mattai_url(@mattai), notice: "Mattai was successfully updated." }
        format.json { render :show, status: :ok, location: @mattai }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @mattai.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mattais/1 or /mattais/1.json
  def destroy
    @mattai.destroy

    respond_to do |format|
      format.html { redirect_to mattais_url, notice: "Mattai was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mattai
      @mattai = Mattai.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def mattai_params
      params.require(:mattai).permit(:load, :rate, :date, :total_amount, :user_id)
    end
end
