class FarmersController < ApplicationController
  before_action :set_farmer, only: %i[ show edit update destroy ]

  # GET /farmers or /farmers.json
  def index
    @farmers = Farmer.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /farmers/1 or /farmers/1.json
  def show
  end

  # GET /farmers/new
  def new
    @farmer = Farmer.new
    @place = Place.where(user_id: current_user.id)
  end

  # GET /farmers/1/edit
  def edit
    @place = Place.where(user_id: current_user.id)
  end

  # POST /farmers or /farmers.json
  def create
    @farmer = Farmer.new(farmer_params)
    respond_to do |format|
      if @farmer.save
        format.html { redirect_to farmer_url(@farmer), notice: "Farmer was successfully created." }
        format.json { render :show, status: :created, location: @farmer }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmers/1 or /farmers/1.json
  def update
    respond_to do |format|
      if @farmer.update(farmer_params)
        format.html { redirect_to farmer_url(@farmer), notice: "Farmer was successfully updated." }
        format.json { render :show, status: :ok, location: @farmer }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmers/1 or /farmers/1.json
  def destroy
    @farmer.destroy
    respond_to do |format|
      format.html { redirect_to farmers_url, notice: "Farmer was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def find_farmer
    @farmer = Farmer.new
    @customer = Customer.new
    @labour = Labour.new
    @find_farmer = Farmer.where(user_id: current_user.id)
    @find_customer = Customer.where(user_id: current_user.id)
    @find_labour = Labour.where(user_id: current_user.id)
  end 

  def tally_farmer
    if params[:farmer].present? && params[:farmer][:id].present?
      @farmer = Farmer.find(params[:farmer][:id])
      redirect_to farmer_accounts_url(farmer_id: @farmer.id)
    end
  end

  def tally_customer
    if params[:customer].present?
      if params[:customer][:id].present?
        customer = Customer.find(params[:customer][:id])
        @customer_bills = CoconutInvoice.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
        # @customer_bills = CustomerBill.where(customer_id: customer).order(paid: :desc)
      end
    end
  end

  def tally_labour
    if params[:labour].present?
      if params[:labour][:id].present?
        labour = Labour.find(params[:labour][:id])
        @labour_bills = Salary.where(labour_id: labour).order(updated_at: :desc)
      end
    end
  end

  def update_pay
    if params[:farmer].present?
      if params[:farmer][:id].present?
        farmer = Farmer.find(params[:farmer][:id])
        @farmer_bills = FarmerBill.where(farmer_id: farmer).order(paid: :desc)
      end
    end
    if params[:purchaser].present?
      if params[:purchaser][:id].present?
        purchaser = Purchaser.find(params[:purchaser][:id])
        @purchaser_bills = PurchaserBill.where(purchaser_id: purchaser).order(paid: :desc)
      end
    end
    if params[:labour].present?
      if params[:labour][:id].present?
        @labour = Labour.find(params[:labour][:id]).order(paid: :desc)
        @labour_bills = Salary.where(labour_id: labour).order(updated_at: :desc)
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmer
      @farmer = Farmer.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def farmer_params
      params.require(:farmer).permit(:name, :is_activate, :mobile_number, :user_id, :place_id, :bank, :account_number, :ifsc, :branch)
    end
end
