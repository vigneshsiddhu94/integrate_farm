class LandsController < BaseController
	def index
		business_id = UserEmployee.find_by(employee_id: current_user.id).business.id
		@land = Land.where(business_id: business_id).all.order('name ASC')
		# @land = Land.all.order('created_at ASC')
	end

	def fodder_management
		@dry_fodder = DryFodder.dry_fodder_live
		@land_crop_dry  = LandCrop.land_crop_dry_live
		@land_crop_green  = LandCrop.land_crop_green_live
	end

	def new
		@land = Land.new
	end

	def create
		land = Land.create(land_params)
		respond_to do |format|
		  	if land.save 
		  	 format.html {redirect_to lands_url, flash: { success: 'Land Created' }}
		  	 format.json { render json: {message: 'Land Created'}, status: :created }
		  	else
		  	 format.html { redirect_to lands_url, flash: { errors: land.errors.full_messages.to_sentence} }
			format.json { render json: {errors: land.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@land = Land.find(params[:id])
	end
	def show
		@land = Land.find(params[:id])	
	end

	def update
		land = Land.find(params[:id])
	 	respond_to do |format|
			if land.update(land_params)
			# land.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to lands_url, flash: { success: 'land Updated Successfully' } }
			format.json { render json: {message: 'land Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_land_path(id: params[:id]) , flash: { errors: land.errors.full_messages.to_sentence} }
			format.json { render json: {errors: land.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	private
	def land_params
		params.require(:land).permit(:name, :cent, :soil, :business_id)
	end
end
