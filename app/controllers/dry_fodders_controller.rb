class DryFoddersController < BaseController
	def index
		@dry_fodders = DryFodder.dry_fodder_no_live
		@dry_fodder = DryFodder.dry_fodder_live
	end

	def new
		@dry_fodder = DryFodder.new
	end

	def create
		dry_fodder = DryFodder.create(dry_fodder_params)
		respond_to do |format|
		  	if dry_fodder.save 
		  	 format.html {redirect_to dry_fodders_url, flash: { success: 'Dry Fodder Created' }}
		  	 format.json { render json: {message: 'Dry Fodder Created'}, status: :created }
		  	else
		  	 format.html { redirect_to dry_fodders_url, flash: { errors: dry_fodder.errors.full_messages.to_sentence} }
			format.json { render json: {errors: dry_fodder.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@dry_fodder = DryFodder.find(params[:id])
	end
	def show
		@dry_fodder = DryFodder.find(params[:id])
	end

	def update
		dry_fodder = DryFodder.find(params[:id])
	 	respond_to do |format|
			if dry_fodder.update(dry_fodder_params)
			# dry_fodder.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to dry_fodders_url, flash: { success: 'Dry Fodder Updated Successfully' } }
			format.json { render json: {message: 'Dry Fodder Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_dry_fodder_path(id: params[:id]) , flash: { errors: dry_fodder.errors.full_messages.to_sentence} }
			format.json { render json: {errors: dry_fodder.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end


	private
	def dry_fodder_params
		params.require(:dry_fodder).permit(:purchase_date, :close_date, :amount, :qty, :land_qty, :variety, :status, :comment)
	end
end