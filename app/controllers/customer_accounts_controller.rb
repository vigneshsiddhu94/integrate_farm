class CustomerAccountsController < ApplicationController
  before_action :set_customer_account, only: %i[ show edit update destroy ]

  # GET /customer_accounts or /customer_accounts.json
  def index
    @customer_accounts = CustomerAccount.all
  end

  # GET /customer_accounts/1 or /customer_accounts/1.json
  def show
  end

  # GET /customer_accounts/new
  def new
    @customer_account = CustomerAccount.new
  end

  # GET /customer_accounts/1/edit
  def edit
  end

  # POST /customer_accounts or /customer_accounts.json
  def create
    @customer_account = CustomerAccount.new(customer_account_params)

    respond_to do |format|
      if @customer_account.save
        format.html { redirect_to customer_account_url(@customer_account), notice: "Customer account was successfully created." }
        format.json { render :show, status: :created, location: @customer_account }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @customer_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_accounts/1 or /customer_accounts/1.json
  def update
    respond_to do |format|
      if @customer_account.update(customer_account_params)
        format.html { redirect_to customer_account_url(@customer_account), notice: "Customer account was successfully updated." }
        format.json { render :show, status: :ok, location: @customer_account }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @customer_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_accounts/1 or /customer_accounts/1.json
  def destroy
    @customer_account.destroy!

    respond_to do |format|
      format.html { redirect_to customer_accounts_url, notice: "Customer account was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_account
      @customer_account = CustomerAccount.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def customer_account_params
      params.require(:customer_account).permit(:amount, :reason, :date, :customer_id, :balance_amount)
    end
end
