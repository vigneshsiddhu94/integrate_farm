class WatersController < BaseController
	def index
		@water = Water.all
	end

	def new
		@water = Water.new
	end

	def create
		water = Water.create(water_params)
		respond_to do |format|
		  	if water.save 
		  	 format.html {redirect_to waters_url, flash: { success: 'Water Created' }}
		  	 format.json { render json: {message: 'Water Created'}, status: :created }
		  	else
		  	 format.html { redirect_to waters_url, flash: { errors: water.errors.full_messages.to_sentence} }
			format.json { render json: {errors: water.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@water = Water.find(params[:id])
	end
	def show
		@water = Water.find(params[:id])
	end

	def update
		water = Water.find(params[:id])
	 	respond_to do |format|
			if water.update(water_params)
			# water.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to waters_url, flash: { success: 'water Updated Successfully' } }
			format.json { render json: {message: 'water Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_water_path(id: params[:id]) , flash: { errors: water.errors.full_messages.to_sentence} }
			format.json { render json: {errors: water.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def land_water
		@land_water = LandWater.new
		@land_waters = LandWater.land_water_live
	end

	def land_water_create
		land_water = LandWater.create(land_water_params)
		respond_to do |format|
		  	if land_water.save
		  	 format.html {redirect_to land_water_url, flash: { success: 'Land Connected With Water' }}
		  	 format.json { render json: {message: 'Land Connected With Water'}, status: :created }
		  	else
		  	 format.html { redirect_to land_water_url, flash: { errors: land_water.errors.full_messages.to_sentence} }
			format.json { render json: {errors: land_water.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end


	private
	def water_params
		params.require(:water).permit(:source, :name, :details, :level, :business_id)
	end
	def land_water_params
		params.require(:land_water).permit(:land_id, :water_id, :date, :business_id)
	end
end
