class FeedRatesController < BaseController


	def new
		@current_rate = FeedRate.where(status: true).order('created_at DESC')
		@feed_rate = FeedRate.new
	end

	def create
		  @feed_rate = FeedRate.new(feed_rate_params)
		  FeedRate.last.update(status: false) if FeedRate.last.present?
		  if @feed_rate.save
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to home_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to home_url
	    end
	end


	private

	def feed_rate_params
		params.require(:feed_rate).permit(:concentrate_feed,:green_fodder,:dry_fodder,:chaval_powder, :status)
	end
end