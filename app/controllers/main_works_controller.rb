class MainWorksController < BaseController

	def index
    	@male_thalacherryies = MainWork.male_thalachery
    	@female_thalacherryies = MainWork.female_thalachery
    	@boyers = MainWork.male_boyer
    	@kid_females = MainWork.kid_female_thalachery.order('day ASC')
    	@kid_males = MainWork.kid_male_thalachery
  	end 


  	def new
  		@main_work = MainWork.new
  	end

  	def create
  		main_work = MainWork.new(main_work_params)
		respond_to do |format|
			if main_work.save
			format.html {redirect_to main_works_path, flash: { success: 'Main Work Created' } }
			format.json { render json: {message: 'Main Work Created'}, status: :created }
			else
			format.html { redirect_to main_works_path , flash: { errors: main_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		@main_work = MainWork.find(params[:id])
  	end

  	def update
  		main_work = MainWork.find(params[:id])
	 	respond_to do |format|
			if main_work.update(main_work_params)
			format.html {redirect_to main_works_path, flash: { success: 'Work Updated Successfully' } }
			format.json { render json: {message: 'Work Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_main_work_path(id: params[:id]) , flash: { errors: main_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: main_work.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @main_work = MainWork.find(params[:id])
	  respond_to do |format|
			if @main_work.destroy
			format.html {redirect_to main_works_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_main_work_path(id: params[:id]) , flash: { errors: main_work.errors.full_messages.to_sentence} }
			format.json { render json: {errors: main_work.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

  	

	private

	def main_work_params
		params.require(:main_work).permit(:user_id,:breed_variety, :day, :action, :action_detail)
	end

end
