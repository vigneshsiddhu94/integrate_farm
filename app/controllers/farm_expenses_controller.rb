class FarmExpensesController < BaseController
  before_action :set_farm_expense, only: [:show, :edit, :update, :destroy]

  # GET /farm_expenses
  # GET /farm_expenses.json
  def index
    @farm_expenses = FarmExpense.farm_expenses_all(current_user).order('created_at DESC').limit(25)
    # @farm_expenses = FarmExpense.farm_expenses_all(current_user).paginate(page: params[:page], per_page: 5)
  end

  # GET /farm_expenses/1
  # GET /farm_expenses/1.json
  def show
  end

  # GET /farm_expenses/new
  def new
    @farm_expense = FarmExpense.new
  end

  # GET /farm_expenses/1/edit
  def edit
  end

  # POST /farm_expenses
  # POST /farm_expenses.json
  def create
    farm_expense = FarmExpense.new(farm_expense_params)

    respond_to do |format|
      if farm_expense.save
        format.html {redirect_to farm_expenses_url, flash: { success: 'Farm expense was successfully created' }}
         format.json { render json: {message: 'Farm expense was successfully created'}, status: :created }
        else
         format.html { redirect_to new_farm_expense_path, flash: { errors: farm_expense.errors.full_messages.to_sentence} }
      format.json { render json: {errors: farm_expense.errors.full_messages.to_sentence}, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farm_expenses/1
  # PATCH/PUT /farm_expenses/1.json
  def update
    farm_expense = FarmExpense.find(params[:id])
    respond_to do |format|
      if farm_expense.update(farm_expense_params)
        format.html {redirect_to farm_expenses_url, flash: { success: 'Farm expense was successfully updated' }}
         format.json { render json: {message: 'Farm expense was successfully updated'}, status: :updated }
        else
         format.html { redirect_to edit_farm_expense_path, flash: { errors: farm_expense.errors.full_messages.to_sentence} }
      format.json { render json: {errors: farm_expense.errors.full_messages.to_sentence}, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farm_expenses/1
  # DELETE /farm_expenses/1.json
  def destroy
    @farm_expense.destroy
    respond_to do |format|
      format.html { redirect_to farm_expenses_url, notice: 'Farm expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farm_expense
      @farm_expense = FarmExpense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def farm_expense_params
      params.require(:farm_expense).permit(:user_id, :date, :category, :note, :amount, :business_id)
    end
end
