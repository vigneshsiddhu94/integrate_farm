class MonthOnWeightsController < ApplicationController
  before_action :set_month_on_weight, only: %i[ show edit update destroy ]

  # GET /month_on_weights or /month_on_weights.json
  def index
    @month_on_weights = []
    total_weight = 0
    sales_count = 0
    death_count = 0
    total_kid = 0
    avg_weight_gain = 0
    last_month_weight = 0
    MonthOnWeight.order(date: :desc).each do |month_weight|
      date = month_weight.date
      start_date = date.beginning_of_month.strftime("%Y-%m-%d")
      end_date = date.end_of_month.strftime("%Y-%m-%d")
      sales_kid_data = SalesAndDeath.where(date: start_date..end_date, action_type: 'Sales', goat_type: 'Kid')
      death_kid_data = SalesAndDeath.where(date: start_date..end_date, action_type: 'Death', goat_type: 'Kid')

      # Calculate sales and death counts and weights
      sales_male_count = sales_kid_data.present? ? sales_kid_data.sum(:male_count) : 0 
      sales_female_count =  sales_kid_data.present? ? sales_kid_data.sum(:female_count) : 0 
      sales_weight = sales_kid_data.present? ? sales_kid_data.sum(:total_weight) : 0 
      
      death_male_count = death_kid_data.present? ? death_kid_data.sum(:male_count) : 0
      death_female_count = death_kid_data.present? ? death_kid_data.sum(:female_count)  : 0
      death_weight =  death_kid_data.present? ? death_kid_data.sum(:total_weight)  : 0

      # Calculate total weight, sales count, death count, and total kid count for the month
      total_weight = month_weight.total_weight + sales_weight + death_weight
      sales_count = sales_male_count + sales_female_count
      death_count = death_male_count + death_female_count
      total_kid = month_weight.total_male_count + month_weight.total_female_count
      last_month_total_kid = month_weight.total_male_count + month_weight.total_female_count + sales_count + death_count
      last_month_weight = MonthOnWeight.where("date < ?", date).order(date: :desc)
      if last_month_weight.present?
        last_month_total_weight = last_month_weight.first.total_weight
        print('last_month_weightlast_month_weightlast_month_weightlast_month_weight-------->',  last_month_total_weight )
        avg_weight_gain = last_month_weight.present? ? ((total_weight - last_month_total_weight) / last_month_total_kid).round(2) : 0
      else
        avg_weight_gain = 0
      end
      print('avg_weight_gain avg_weight_gain avg_weight_gain avg_weight_gain avg_weight_gain-------->', avg_weight_gain )
     
     
      # Append data for the current month to the array
      @month_on_weights << {
        id: month_weight.id,
        date: date,
        male: month_weight.total_male_count,
        female: month_weight.total_female_count,
        sales: sales_count,
        death: death_count,
        total_kid: total_kid,
        total_weight: total_weight,
        avg_weight_gain: avg_weight_gain
      }
    end
  end
  
  # GET /month_on_weights/1 or /month_on_weights/1.json
  def show
  end

  # GET /month_on_weights/new
  def new
    @month_on_weight = MonthOnWeight.new
    @month_on_weight.batch_weights.build
  end

  # GET /month_on_weights/1/edit
  def edit
  end

  # POST /month_on_weights or /month_on_weights.json
  def create
    # byebug
    @month_on_weight = MonthOnWeight.new(month_on_weight_params)

    respond_to do |format|
      if @month_on_weight.save
        format.html { redirect_to month_on_weight_url(@month_on_weight), notice: "Month on weight was successfully created." }
        format.json { render :show, status: :created, location: @month_on_weight }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @month_on_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /month_on_weights/1 or /month_on_weights/1.json
  def update
    respond_to do |format|
      if @month_on_weight.update(month_on_weight_params)
        format.html { redirect_to month_on_weight_url(@month_on_weight), notice: "Month on weight was successfully updated." }
        format.json { render :show, status: :ok, location: @month_on_weight }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @month_on_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /month_on_weights/1 or /month_on_weights/1.json
  def destroy
    @month_on_weight.batch_weights.destroy_all
    @month_on_weight.destroy!

    respond_to do |format|
      format.html { redirect_to month_on_weights_url, notice: "Month on weight was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_month_on_weight
      @month_on_weight = MonthOnWeight.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def month_on_weight_params
      params.require(:month_on_weight).permit(:total_male_count,:total_female_count,:total_weight,:date,:business_id,:user_id,batch_weights_attributes: [:id, :_destroy, :rack_detail_id, :kid_stage, :male_count, :female_count, :weight,:business_id,:user_id])
    end

end
