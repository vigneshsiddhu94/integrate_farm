class CowsController < BaseController
	def index
    	@cows = Cow.all.order('created_at DESC') 
    	@cows.each do |cow|
    		cow.milk_productions
    	end
  	end 

  	def health
  		@cow_health = Cow.find(params[:cow_id]).calving_periods.order('calve_date ASC')
  	end

	def new
		@cow = Cow.new
		@cow.calving_periods.build
	end

	def create
		@cow = Cow.new(cow_params)
		if @cow.save
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to home_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to home_url
	    end
	end

	def edit
		@cow = Cow.find(params[:id])
	end

	def update
		@cow = Cow.find(params[:id])
	   if @cow.update_attributes(cow_params)
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to home_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to home_url
	    end
  	end

  	def cow_health
  		@cow_health = CalvingPeriod.new
  		@cattle_health = CalvingPeriod.all.order('calve_date ASC')
  	end

  	def create_health
  		@cow_health = CalvingPeriod.new(calving_period_params)
		  if @cow_health.save
	      flash[:success] = 'Your request has been placed. Our agent will contact you shortly!'
	      redirect_to home_url
	    else
	      flash[:danger] = 'Please check the errors.'
	      redirect_to home_url
	    end

  	end

	private

	def cow_params
		params.require(:cow).permit(:token_no,:milk_assurance,:cow_rate,:date_of_purchase,:breed_variety,:cow_location,:user_id, :business_id,:age, { calving_periods_attributes: [:cow_status,:calve_date] })
	end

	def calving_period_params
		params.require(:calving_period).permit(:cow_id, :cow_status, :calve_date, :message)
	end 
end
