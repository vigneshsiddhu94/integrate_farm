class MateGoatsController < BaseController

	def index
    	@mate_goats = MateGoat.mate_goat_all(current_user).order('created_at DESC').limit(25)
    	@mate_goat = MateGoat.new
		goat = Goat.where(user_id: current_user.id)
		@male = Goat.male_boyer(current_user)
		@female = Goat.female_thalachery(current_user)
  	end 

	def new
		@mate_goat = MateGoat.new
		goat = Goat.where(user_id: current_user.id)
		@male = Goat.male_boyer(current_user)
		@female = Goat.female_thalachery(current_user)
	end

	def create
  		mate = MateGoat.new(mate_goat_params)
		respond_to do |format|
			if mate.save
			format.html {redirect_to mate_goats_path, flash: { success: 'Mate With Details Stored  ' } }
			format.json { render json: {message: 'Mate With Details Stored'}, status: :created }
			else
			format.html { redirect_to edit_mate_goat_path , flash: { errors: mat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		goat = Goat.where(user_id: current_user.id)
		@male = Goat.male_boyer(current_user)
		@female = Goat.female_thalachery(current_user)
  		@mate_goat = MateGoat.find(params[:id])
  	end

  	def update
  		mate_goat = MateGoat.find(params[:id])
	 	respond_to do |format|
			if mate_goat.update(mate_goat_params)
			format.html {redirect_to mate_goats_path, flash: { success: 'Mate Updated Successfully' } }
			format.json { render json: {message: 'Mate Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_mate_goat_path(id: params[:id]) , flash: { errors: mate_goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: mate_goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @mate_goat = MateGoat.find(params[:id])
	  respond_to do |format|
			if @mate_goat.destroy
			format.html {redirect_to mate_goats_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_mate_goat_path(id: params[:id]) , flash: { errors: mate_goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: mate_goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

  	
  	private

  	def mate_goat_params
		params.require(:mate_goat).permit(:male_id, :female_id, :comment, :date_of_mat)
	end

end
