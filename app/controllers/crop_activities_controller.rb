class CropActivitiesController < BaseController
	def index
		@crop_activities = CropActivity.crop_activity_not_live
		@crop_activity = CropActivity.crop_activity_live
	end

	def new
		@crop_activity = CropActivity.new
	end

	def create
		crop_activity = CropActivity.create(crop_activity_params)
		respond_to do |format|
		  	if crop_activity.save 
		  	 format.html {redirect_to crop_activities_url, flash: { success: 'Crop Activity Created' }}
		  	 format.json { render json: {message: 'Crop Activity Created'}, status: :created }
		  	else
		  	 format.html { redirect_to crop_activities_url, flash: { errors: crop_activity.errors.full_messages.to_sentence} }
			format.json { render json: {errors: crop_activity.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@crop_activity = CropActivity.find(params[:id])
	end
	def show
		@crop_activity = CropActivity.find(params[:id])
	end

	def update
		crop_activity = CropActivity.find(params[:id])
	 	respond_to do |format|
			if crop_activity.update(crop_activity_params)
			# crop_activity.update(:goat_sales_attributes => [:goat_id, :weight, :amount])
			format.html {redirect_to crop_activities_url, flash: { success: 'Crop Activity Updated Successfully' } }
			format.json { render json: {message: 'Crop Activity Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_crop_activity_path(id: params[:id]) , flash: { errors: crop_activity.errors.full_messages.to_sentence} }
			format.json { render json: {errors: crop_activity.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end


	private
	def crop_activity_params
		params.require(:crop_activity).permit(:land_crop_id, :activity, :date, :comment, :status, :end_date)
	end
end