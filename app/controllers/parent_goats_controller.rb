class ParentGoatsController < BaseController
	def index
		@parent_goat = ParentGoat.new
		@parent_goats = ParentGoat.parent_goat_all(current_user).order('created_at DESC').limit(25)
	end

	def new
		@parent_goat = ParentGoat.new
	end

	def create
		parent_goat = ParentGoat.create(parent_goat_params)
		respond_to do |format|
		  	if parent_goat.save 
		  	 format.html {redirect_to parent_goats_url, flash: { success: 'Parent For Goat Added' }}
		  	 format.json { render json: {message: 'Parent For Goat Added'}, status: :created }
		  	else
		  	 format.html { redirect_to parent_goats_url, flash: { errors: parent_goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent_goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def edit
		@parent_goat = ParentGoat.find(params[:id])
	end
	
	def show
		@parent_goat = ParentGoat.find(params[:id])
	end

	def update
		parent_goat = ParentGoat.find(params[:id])
	 	respond_to do |format|
			if parent_goat.update(parent_goat_params)
 			format.html {redirect_to parent_goats_url, flash: { success: 'Parent For Goat Updated Successfully' } }
			format.json { render json: {message: 'Parent For Goat Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_parent_goat_path(id: params[:id]) , flash: { errors: parent_goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent_goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	end

	def destroy
	  @parent_goat = ParentGoat.find(params[:id])
	  respond_to do |format|
			if @parent_goat.destroy
			format.html {redirect_to parent_goats_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_parent_goat_path(id: params[:id]) , flash: { errors: parent_goat.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent_goat.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end


	private
	
	def parent_goat_params
		params.require(:parent_goat).permit(:children_id, :parent_id)
	end
	
end