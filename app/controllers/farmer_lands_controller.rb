class FarmerLandsController < ApplicationController
  before_action :set_farmer_land, only: %i[ show edit update destroy ]

  # GET /farmer_lands or /farmer_lands.json
  def index
    @farmer_lands = FarmerLand.all
  end

  # GET /farmer_lands/1 or /farmer_lands/1.json
  def show
  end

  # GET /farmer_lands/new
  def new
    @farmer_land = FarmerLand.new
  end

  # GET /farmer_lands/1/edit
  def edit
  end

  # POST /farmer_lands or /farmer_lands.json
  def create
    @farmer_land = FarmerLand.new(farmer_land_params)

    respond_to do |format|
      if @farmer_land.save
        format.html { redirect_to farmer_land_url(@farmer_land), notice: "Farmer land was successfully created." }
        format.json { render :show, status: :created, location: @farmer_land }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @farmer_land.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmer_lands/1 or /farmer_lands/1.json
  def update
    respond_to do |format|
      if @farmer_land.update(farmer_land_params)
        format.html { redirect_to farmer_land_url(@farmer_land), notice: "Farmer land was successfully updated." }
        format.json { render :show, status: :ok, location: @farmer_land }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @farmer_land.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmer_lands/1 or /farmer_lands/1.json
  def destroy
    @farmer_land.destroy

    respond_to do |format|
      format.html { redirect_to farmer_lands_url, notice: "Farmer land was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmer_land
      @farmer_land = FarmerLand.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def farmer_land_params
      params.require(:farmer_land).permit(:acre, :tree_count, :farmer_id)
    end
end
