class DiseasesController < BaseController

	def index
    	@diseases = Disease.disease_all(current_user).order('created_at DESC')
  	end 

  	def goat_diseases
  		@diseases = DiseaseGoat.disease_all(current_user).order('created_at DESC')
  	end

	def new
		@disease = Disease.new
	end

	def create
  		disease = Disease.new(disease_params)
		respond_to do |format|
			if disease.save
			format.html  {redirect_to diseases_path, flash: { success: 'Disease Stored  ' } }
			format.json { render json: {message: 'Disease Stored'}, status: :created }
			else
			format.html { redirect_to new_disease_path , flash: { errors: disease.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit
  		@disease = Disease.find(params[:id])
  	end

  	def update
  		disease = Disease.find(params[:id])
	 	respond_to do |format|
			if disease.update(disease_params)
			format.html {redirect_to diseases_path, flash: { success: 'Disease Updated Successfully' } }
			format.json { render json: {message: 'Goat Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_disease_path(id: params[:id]) , flash: { errors: disease.errors.full_messages.to_sentence} }
			format.json { render json: {errors: disease.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def destroy
	  @disease = Disease.find(params[:id])
	  respond_to do |format|
			if @disease.destroy
			format.html {redirect_to diseases_path, flash: { success: 'Deleted Successfully' } }
			format.json { render json: {message: 'Deleted Successfully'}, status: :created }
			else
			format.html { redirect_to edit_disease_path(id: params[:id]) , flash: { errors: disease.errors.full_messages.to_sentence} }
			format.json { render json: {errors: disease.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
	 end

  	def goat_disease
  		@disease_goat = DiseaseGoat.new
  		@goat_diseases = DiseaseGoat.disease_goat_all(current_user).order('created_at DESC')
  	end

  	def create_goat_disease
  		disease = DiseaseGoat.new(disease_goat_params)
		respond_to do |format|
			if disease.save
			format.html {redirect_to goat_disease_path, flash: { success: 'Disease Stored  ' } }
			format.json { render json: {message: 'Disease Stored'}, status: :created }
			else
			format.html { redirect_to goat_disease_path , flash: { errors: disease.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	def edit_goat_disease
  		@disease_goat = DiseaseGoat.find(params[:id])
  	end

  	def update_goat_disease
  		disease = DiseaseGoat.find(params[:id])
	 	respond_to do |format|
			if disease.update(disease_goat_params)
			format.html {redirect_to goat_diseases_path, flash: { success: 'Disease Updated Successfully' } }
			format.json { render json: {message: 'Disease Updated Successfully'}, status: :created }
			else
			format.html { redirect_to edit_goat_disease_path(id: params[:id]) , flash: { errors: disease.errors.full_messages.to_sentence} }
			format.json { render json: {errors: disease.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
		end
  	end

  	def goat_death
  		@death_goat = DeathGoat.new
  		@death_goats = DeathGoat.death_all(current_user).order('created_at DESC').limit(25)
  	end

  	def create_goat_death
  		death = DeathGoat.new(goat_death_params)
		respond_to do |format|
			if death.save
			death.goat.update(live: 'No')
			format.html {redirect_to goat_death_path, flash: { success: 'Goat Death Data Stored  ' } }
			format.json { render json: {message: 'Goat Death Data Stored '}, status: :created }
			else
			format.html { redirect_to goat_death_path , flash: { errors: death.errors.full_messages.to_sentence} }
			format.json { render json: {errors: parent.errors.full_messages.to_sentence}, status: :unprocessable_entity }
			end
	  	end
  	end

  	private

  	def disease_params
		params.require(:disease).permit(:user_id, :name, :symptoms, :impact_goat, :treatment)
	end 

	def disease_goat_params
		params.require(:disease_goat).permit(:goat_id, :disease_id, :comment, :status, :disease_date)
	end

	def goat_death_params
		params.require(:death_goat).permit(:goat_id, :disease_id, :died_date, :weight, :comment)
	end

end
