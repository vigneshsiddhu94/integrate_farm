class PurchaserBillsController < ApplicationController
  before_action :set_purchaser_bill, only: %i[ show edit update destroy ]

  # GET /purchaser_bills or /purchaser_bills.json
  def index
    @purchaser_bills = PurchaserBill.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /purchaser_bills/1 or /purchaser_bills/1.json
  def show
  end

  # GET /purchaser_bills/new
  def new
    @purchaser_bill = PurchaserBill.new
    @place = Place.where(user_id: current_user.id)
    @farmer = Farmer.where(user_id: current_user.id)
    @vechicle = Vechicle.where(user_id: current_user.id)
    @purchaser = Purchaser.where(user_id: current_user.id)
  end

  # GET /purchaser_bills/1/edit
  def edit
    @place = Place.where(user_id: current_user.id)
    @farmer = Farmer.where(user_id: current_user.id)
    @vechicle = Vechicle.where(user_id: current_user.id)
    @purchaser = Purchaser.where(user_id: current_user.id)
    @place = Place.where(user_id: current_user.id)
  end

  # POST /purchaser_bills or /purchaser_bills.json
  def create
    @purchaser_bill = PurchaserBill.new(purchaser_bill_params)
    
    respond_to do |format|
      if @purchaser_bill.save
        format.html { redirect_to purchaser_bill_url(@purchaser_bill), notice: "Purchaser bill was successfully created." }
        format.json { render :show, status: :created, location: @purchaser_bill }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @purchaser_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchaser_bills/1 or /purchaser_bills/1.json
  def update
    respond_to do |format|
      if @purchaser_bill.update(purchaser_bill_params)
        format.html { redirect_to purchaser_bill_url(@purchaser_bill), notice: "Purchaser bill was successfully updated." }
        format.json { render :show, status: :ok, location: @purchaser_bill }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @purchaser_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchaser_bills/1 or /purchaser_bills/1.json
  def destroy
    @purchaser_bill.destroy

    respond_to do |format|
      format.html { redirect_to purchaser_bills_url, notice: "Purchaser bill was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchaser_bill
      @purchaser_bill = PurchaserBill.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def purchaser_bill_params
      params.require(:purchaser_bill).permit(:asal_vhurai, :asal_kai, :neer_vattral_vhurai, :neer_vattral_kai, :number_of_person, :asal, :kai, :total_coconut, :procurement_date, :user_id, :purchaser_id, :vechicle_id, :farmer_id, :total_amount, :paid_date, :paid)
    end
end
