class FarmerBillsController < ApplicationController
  before_action :set_farmer_bill, only: %i[ show edit update destroy ]

  # GET /farmer_bills or /farmer_bills.json
  def index
    @farmer_bills = FarmerBill.where(user_id: current_user.id).order(created_at: :desc, updated_at: :desc)
  end

  # GET /farmer_bills/1 or /farmer_bills/1.json
  def show
  end

  # GET /farmer_bills/new
  def new
    @farmer_bill = FarmerBill.new
    @farmer = Farmer.where(user_id: current_user.id)
  end

  # GET /farmer_bills/1/edit
  def edit
    @farmer = Farmer.where(user_id: current_user.id)
  end


  # POST /farmer_bills or /farmer_bills.json
  def create
    @farmer_bill = FarmerBill.new(farmer_bill_params)

    respond_to do |format|
      if @farmer_bill.save
        format.html { redirect_to farmer_bill_url(@farmer_bill), notice: "Farmer bill was successfully created." }
        format.json { render :show, status: :created, location: @farmer_bill }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @farmer_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmer_bills/1 or /farmer_bills/1.json
  def update
    respond_to do |format|
      if @farmer_bill.update(farmer_bill_params)
        format.html { redirect_to farmer_bill_url(@farmer_bill), notice: "Farmer bill was successfully updated." }
        format.json { render :show, status: :ok, location: @farmer_bill }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @farmer_bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmer_bills/1 or /farmer_bills/1.json
  def destroy
    @farmer_bill.destroy

    respond_to do |format|
      format.html { redirect_to farmer_bills_url, notice: "Farmer bill was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmer_bill
      @farmer_bill = FarmerBill.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def farmer_bill_params
      params.require(:farmer_bill).permit(:asal_kai_count,:asal_kai_vasi, :asal_kai_rate, :neer_vattral__kai_count,:neer_vatral_kai_vasi, :neer_vattral_kai_rate, :total_amount, :procurement_date, :user_id, :farmer_id, :paid_date, :paid)
    end
end
