class FarmerAccountsController < ApplicationController
  before_action :set_farmer_account, only: %i[ show edit update destroy ]

  # GET /farmer_accounts or /farmer_accounts.json
  def index
    @farmer = Farmer.find(params['farmer_id'])
    @farmer_accounts = FarmerAccount.where(farmer_id: @farmer.id)
  end

  # GET /farmer_accounts/1 or /farmer_accounts/1.json
  def show
  end

 
  def create
    @kopparai = Kopparai.new(kopparai_params)
    respond_to do |format|
      if @kopparai.save
        format.html { redirect_to kopparai_url(@kopparai), notice: "Kopparai was successfully created." }
        format.json { render :show, status: :created, location: @kopparai }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @kopparai.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /farmer_accounts/new
  def new
    @farmer_account = FarmerAccount.new
  end

  # GET /farmer_accounts/1/edit
  def edit
  end

  # POST /farmer_accounts or /farmer_accounts.json
  def create
    @farmer_account = FarmerAccount.new(farmer_account_params)

    respond_to do |format|
      if @farmer_account.save
        format.html { redirect_to farmer_account_url(@farmer_account), notice: "Farmer account was successfully created." }
        format.json { render :show, status: :created, location: @farmer_account }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @farmer_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /farmer_accounts/1 or /farmer_accounts/1.json
  def update
    respond_to do |format|
      if @farmer_account.update(farmer_account_params)
        format.html { redirect_to farmer_account_url(@farmer_account), notice: "Farmer account was successfully updated." }
        format.json { render :show, status: :ok, location: @farmer_account }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @farmer_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmer_accounts/1 or /farmer_accounts/1.json
  def destroy
    @farmer_account.destroy!

    respond_to do |format|
      format.html { redirect_to find_farmer_path, notice: "Farmer account was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_farmer_account
      @farmer_account = FarmerAccount.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def farmer_account_params
      params.require(:farmer_account).permit(:amount, :reason, :date, :farmer_id, :balance_amount)
    end
end
