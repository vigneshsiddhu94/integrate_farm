// $( document ).on('turbolinks:load', function() {
//   // console.log("It works on each visit!")
// })

document.addEventListener("turbolinks:load", function() {
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });
});

// Close any open menu accordions when window is resized below 768px

